-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2019 at 12:31 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `byton_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'anonymous',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `deleted`) VALUES
(1, 'PAPERS', 0),
(2, 'MACHINES', 0),
(3, 'PENS & PENCILS', 0),
(4, 'STATIONERIES', 0),
(5, 'EXERCISE BOOK', 0),
(6, 'ENVELOPES', 0),
(7, 'CELLOTAPES', 0),
(8, 'DOCUMENT FILES', 0),
(9, 'CATRIDGE INK AND TONERS', 0),
(10, 'SPARE PARTS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `person_id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`person_id`, `company_name`, `deleted`) VALUES
(12, NULL, 0),
(17, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(11) NOT NULL,
  `level` varchar(25) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`username`, `password`, `person_id`, `level`, `status`, `deleted`) VALUES
('admin', 'c93ccd78b2076528346216b3b2f701e6', 1, 'admin', 1, 0),
('glory', 'f68728c832c655ff625dfffc7636b051', 2, 'admin', 1, 0),
('sammongi', 'ede9754c644e3dd91602cfabb9636c7c', 3, 'sales', 1, 0),
('grace', 'ae1456ece01243203b5159435fba038d', 4, 'admin', 1, 0),
('annjohn', 'deff7e0ce47950b461d0a462402d3194', 5, 'chief', 1, 0),
('sales', '751cb3f4aa17c36186f4856c8982bf27', 16, 'sales', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `supplier_id` int(11) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `unit` varchar(25) NOT NULL,
  `buying_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `unit_selling_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `ctn_selling_price` decimal(15,2) DEFAULT NULL,
  `dzn_selling_price` decimal(10,0) DEFAULT '0',
  `outer_selling_price` decimal(15,2) DEFAULT NULL,
  `quantity_in` int(100) NOT NULL DEFAULT '0',
  `quantity_out` int(100) NOT NULL DEFAULT '0',
  `min_quantity` int(11) DEFAULT '0',
  `delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `brand`, `category_id`, `supplier_id`, `barcode`, `description`, `unit`, `buying_price`, `unit_selling_price`, `ctn_selling_price`, `dzn_selling_price`, `outer_selling_price`, `quantity_in`, `quantity_out`, `min_quantity`, `delete`) VALUES
(1, 'COUNTER BOOK', 'BLUE BIRD', 5, 9, '', 'QUIRE 2\r\n48 PIECES IN CARTON', '1', '75000.00', '2000.00', '79000.00', '0', '12000.00', 42, 0, 20, 0),
(2, 'COUNTER BOOK', 'BLUE BIRD', 5, 9, '', 'QUIRE 3\r\n36 UNITS PER CARTON', '1', '75000.00', '2500.00', '79000.00', '0', '15000.00', 46, 0, 20, 0),
(3, 'FLIPCHART', 'OSTRICH', 1, 9, '', '30 SHEETS PER REAM\r\n10 REAMS PER CTN', '1', '41500.00', '6000.00', '55000.00', '0', '1.00', 14, 0, 5, 0),
(4, 'EXERCISE BOOK', 'CROWN', 5, 9, '', '100 PCS PER CARTON', '1', '17500.00', '300.00', '21000.00', '0', '1.00', 0, 0, 10, 0),
(5, 'STIFFCOVER', 'NGALAWA', 5, 9, '', '96 PCS PER CARTON', '1', '77000.00', '1500.00', '92000.00', '0', '1.00', 0, 0, 5, 0),
(6, 'ENVELOPE', 'NGALAWA', 6, 9, '', 'SIZE: A4\r\n1000 PCS PER CARTON', '1', '58000.00', '200.00', '70000.00', '0', '1.00', 32, 0, 10, 0),
(7, 'ENVELOPE', 'NGALAWA', 6, 9, '', 'SIZE: A5\r\n1800 PCS PER CARTON\r\n', '1', '64100.00', '200.00', '89000.00', '0', '1.00', 13, 0, 5, 0),
(8, 'ENVELOPE', 'NGALAWA', 6, 9, '', 'SIZE: A3\r\n500 PCS PER CARTON', '1', '48500.00', '500.00', '60000.00', '0', '1.00', 0, 0, 10, 0),
(9, 'MARKERPEN ', 'SNOWMAN', 3, 6, '', '8 OUTER PER CARTON\r\n10 BOXES PER OUTER\r\n12 PCS PER BOX', '1', '135000.00', '500.00', '160000.00', '2500', '20000.00', 7, 0, 2, 0),
(10, 'CELLOTAPE', 'NIL', 7, 6, '', '100 YARDS, 72 PCS PER CARTON', '1', '112000.00', '3000.00', '142000.00', '23000', '0.00', 0, 0, 2, 0),
(11, 'SPRINGFILE', 'RAPID', 8, 6, '', 'PVC\r\n288 PCS PER CARTON', '1', '280000.00', '2000.00', '315000.00', '14000', '0.00', 3, 0, 2, 0),
(12, 'BOXFILE', 'ALBARADO', 8, 6, '', 'BLACK IN COLOUR\r\n50 PCS PER CARTON', '1', '105000.00', '5000.00', '125000.00', '30000', '0.00', 3, 0, 3, 0),
(13, 'RINGBINDER', 'STATIONERIES', 8, 6, '', 'MULTI COLOUR\r\n96 PCS PER CARTON', '1', '168000.00', '3500.00', '192000.00', '24000', '0.00', 2, 0, 3, 0),
(14, 'MATHEMATICAL SET', 'ACADEMY', 4, 6, '', '144 PCS PER CARTON\r\n12 OUTERS PER CARTON\r\nEACH OUTER WITH 12 PCS\r\n', '1', '155000.00', '2000.00', '176400.00', '15000', '15000.00', 3, 0, 3, 0),
(15, 'SCHOOL STUDENT RULER', 'STATIONERIES', 4, 6, '', '600 PCS PER CARTON', '1', '132000.00', '500.00', '150000.00', '3000', '15000.00', 0, 0, 2, 0),
(16, 'LAMINATION POUCH', 'STATIONERIES', 4, 6, '', 'SIZE: A4\r\n10 PCS PER CARTON', '1', '140000.00', '170000.00', '165000.00', '0', '0.00', 2, 0, 2, 0),
(17, 'LAMINATION POUCH', 'STATIONERIES', 4, 6, '', 'SIZE: A3\r\n5 PCS PER CARTON', '1', '140000.00', '36000.00', '170000.00', '0', '0.00', 0, 0, 0, 0),
(18, 'OFFICE TRAY', 'CHINESE', 4, 6, '', '3 LAYERS IRON (CHUMA)\r\n12 PCS PER CARTON', '1', '132000.00', '18000.00', '168000.00', '168000', '1.00', 0, 0, 0, 0),
(19, 'WHITEBOARDS', 'CHINESE', 4, 6, '', '90X120', '2', '55000.00', '85000.00', '0.00', '0', '0.00', 1, 0, 0, 0),
(20, 'WHITEBOARD', 'CHINESE', 4, 6, '', '70X100', '2', '50000.00', '80000.00', '0.00', '0', '0.00', 0, 0, 0, 0),
(21, 'STAPLE PINS', 'CHINESE', 4, 6, '', '50 BOXES PER CARTON', '1', '85000.00', '500.00', '110000.00', '30000', '2500.00', 2, 0, 1, 0),
(22, 'SCIENTIFIC CALCALCULATOR', 'CASIO', 2, 7, '', 'FX 991 MS\r\n40 PCS PER CARTON', '1', '1100000.00', '40000.00', '1160000.00', '360000', '0.00', 0, 0, 0, 0),
(23, 'SCIENTIFIC CALCULATOR', 'CASIO', 2, 6, '', 'FX 991 ES', '2', '37500.00', '50000.00', '1600000.00', '0', '0.00', 0, 0, 1, 0),
(24, 'PENCIL', 'NATARAJ', 3, 6, '', '2HB\r\n20 OUTER PER CARTON\r\n12 BOXES PER OUTER', '1', '420000.00', '400.00', '460000.00', '25000', '24000.00', 3, 0, 1, 0),
(25, 'RIBBON', 'STATIONERIES', 4, 6, '', 'BLUE\r\n50 PCS PER CARTON', '1', '120000.00', '3500.00', '150000.00', '0', '0.00', 0, 0, 0, 0),
(26, 'LAMINATION MACHINE', 'STATIONERIES SBT SBT', 2, 6, '', 'SIZE: A4', '2', '90000.00', '150000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(27, 'PAPERCUTTER ', 'STATIONERIES', 2, 6, '', 'SIZE:A4\r\n10 PCS PER CARTON', '2', '200000.00', '40000.00', '300000.00', '0', '1.00', 0, 0, 1, 0),
(28, 'PAPERCUTTER', 'STATIONERIES', 2, 6, '', 'SIZE: A3\r\n5 PCS IN CTS', '2', '150000.00', '50000.00', '0.00', '0', '0.00', 0, 0, 0, 0),
(29, 'RUBBER BAND', 'FALCON', 4, 6, '', '40 PCT PER CARTON', '1', '40000.00', '2000.00', '53000.00', '18000', '0.00', 46, 0, 5, 0),
(30, 'GLOSS PAPER STICKER', 'CHINESE', 1, 8, '', '28 PCS PER CARTON', '1', '196500.00', '9000.00', '229600.00', '0', '1.00', 0, 0, 1, 0),
(31, 'STAPLER', 'CHINESE', 2, 6, '', 'DS-E 335', '1', '300000.00', '4000.00', '340000.00', '0', '1.00', 3, 0, 0, 0),
(32, 'STAPLER', 'KANGAROO', 2, 6, '', 'DS 23S13QL\r\n20 PCS PER CARTON', '1', '200000.00', '40000.00', '650000.00', '0', '1.00', 0, 0, 0, 0),
(33, 'STAPLER', 'KANGAROO', 2, 6, '', 'DS 23S15PL\r\n10 PCS PER CARTON', '1', '380000.00', '700000.00', '600000.00', '0', '1.00', 0, 0, 0, 0),
(34, 'STAPLER ', 'KANGAROO', 2, 6, '', 'DS 23S20PL\r\n10 PCS PER CARTON', '1', '610000.00', '90000.00', '800000.00', '0', '1.00', 0, 0, 0, 0),
(35, 'THERMAL ROLLS', 'STATIONERIES', 4, 6, '', '80X80 ', '1', '1500000.00', '5000.00', '240000.00', '0', '0.00', 0, 0, 4, 0),
(36, 'OFFICE FILE', 'STATIONERIES', 8, 6, '', '600 PCS PER CARTON\r\n50 DOZENS IN CARTON', '1', '165000.00', '1000.00', '200000.00', '5500', '1.00', 0, 0, 0, 0),
(37, 'CLEAR BAG', 'STATIONERIES', 8, 6, '', '600 PCS PER CARTON\r\n50 DOZENS IN CARTON', '1', '150000.00', '1000.00', '175000.00', '4000', '0.00', 2, 0, 0, 0),
(38, 'NOTEBOOK', 'CHINESE', 4, 6, '', '100K\r\n400 PCS PER CARTON', '1', '240000.00', '1000.00', '265000.00', '8000', '1.00', 0, 0, 1, 0),
(39, 'NOTEBOOK', 'CHINESE', 4, 6, '', '60K\r\n300 PCS PER CARTON', '1', '240000.00', '1500.00', '270000.00', '11500', '1.00', 0, 0, 0, 0),
(40, 'NOTEBOOK', 'CHINESE', 4, 6, '', '36K\r\n200 PCS PER CARTON', '1', '260000.00', '2500.00', '300000.00', '19000', '1.00', 0, 0, 0, 0),
(41, 'NOTEBOOK', 'CHINESE', 4, 6, '', '18K\r\n100 PCS PER CARTON', '1', '220000.00', '4000.00', '260000.00', '32000', '1.00', 0, 0, 0, 0),
(42, 'ERASOR', 'STATIONERIES', 4, 6, '', 'ER-40\r\n40 OUTER PER CARTON', '1', '84000.00', '200.00', '135000.00', '0', '3500.00', 0, 0, 0, 0),
(43, 'PUNCH', 'KANGAROO', 2, 6, '', 'DP-600\r\n60 PCS PER CARTON', '1', '234000.00', '8000.00', '280000.00', '69500', '1.00', 0, 0, 0, 0),
(44, 'LEGAL PAPER', 'JAMANA', 1, 6, '', '20 PCS PER CARTON', '1', '120000.00', '8000.00', '150000.00', '0', '1.00', 3, 0, 0, 0),
(45, 'NOTEBOOK', 'CHINESE', 4, 0, '', '32K\r\n150 PCS IN CARTON', '1', '225000.00', '3000.00', '270000.00', '0', '1.00', 0, 0, 0, 0),
(46, 'NOTEBOOK', 'CHINESE', 4, 6, '280000', '25 K\r\n150 PC PER CARTON', '1', '225000.00', '3500.00', '270000.00', '23500', '1.00', 0, 0, 0, 0),
(47, 'NOTEBOOK', 'CHINESE', 4, 6, '', '48K\r\n250 PCS PER CARTON', '1', '250000.00', '2000.00', '300000.00', '14600', '1.00', 0, 0, 0, 0),
(48, 'VISITORS BOOK', 'ROYAL', 4, 6, '', '60 PCS PER CARTON', '1', '115000.00', '3000.00', '140000.00', '27000', '1.00', 1, 0, 1, 0),
(49, 'SHARPENER', 'NATARAJ', 4, 6, '', '24 JARS IN CARTON\r\nEACH JAR WITH 50 UNITS', '1', '145000.00', '400.00', '170000.00', '0', '8500.00', 0, 0, -1, 0),
(50, 'ERASOR', 'NATARAJ', 4, 6, '', '24 JARS IN CARTON\r\nEACH JAR WITH 100 UNITS', '1', '195000.00', '200.00', '210000.00', '0', '9000.00', 1, 0, 1, 0),
(51, 'RIBBON', 'STATIONERIES', 4, 6, '', 'YELLOW\r\n50 PCS PER CARTON', '1', '120000.00', '3500.00', '150000.00', '0', '1.00', 0, 0, 0, 0),
(52, 'RIBBON', 'STATIONERIES', 4, 6, '', 'GOLD\r\n50 PCS PER CARTON', '1', '120000.00', '3500.00', '150000.00', '0', '1.00', 1, 0, 0, 0),
(53, 'DISPATCH BOOK', 'ROYAL', 4, 6, '', '96 PCS PER CARTON', '1', '84000.00', '2000.00', '104000.00', '14000', '1.00', 1, 0, 1, 0),
(54, 'PRICE STICKER', 'CHINESE', 4, 6, '', 'CODE: 203', '1', '200000.00', '5000.00', '300000.00', '0', '1.00', 0, 0, 0, 0),
(55, 'COUNTER BOOK ', 'BLUE BIRD', 5, 9, '', '24 PCS PER CARTON', '1', '70800.00', '4000.00', '80000.00', '0', '0.00', 6, 0, 2, 0),
(56, 'STAPLE PINS', 'KANGAROO', 4, 6, '', '15 BOXES IN CARTON', '1', '140000.00', '1000.00', '180000.00', '0', '13000.00', 0, 0, 1, 0),
(57, 'GLUE STICK', 'DR.FAN', 4, 0, '', '32 BOXES IN CARTON\r\nEACH BOX HAS 30 PCS', '1', '360000.00', '1000.00', '460000.00', '0', '1.00', 0, 0, 0, 0),
(58, 'GLUE STICK', 'DR.FAN', 4, 0, '', '25 GRAMS\r\n30 BOXES IN CARTON\r\nEACH BOX HAVE ', '1', '435000.00', '2000.00', '510000.00', '0', '18000.00', 0, 0, 0, 0),
(59, 'GLUE STICK', 'DR.FAE', 4, 6, '', '35 GRAMS\r\n48 BOXES PER CARTON\r\n12 PCS PER BOX', '1', '720000.00', '2500.00', '850000.00', '19500', '19500.00', 0, 0, 0, 0),
(61, 'WHITEBOARD', 'CHINESE', 4, 6, '', '100X150', '2', '80000.00', '150000.00', '0.00', '0', '0.00', 3, 0, 0, 0),
(62, 'SHORTHAND NOTEBOOK', 'DELTA', 4, 6, '', '5X7 \r\nTOP SPIRAL\r\n192 PCS PER CARTON\r\n', '1', '104000.00', '1000.00', '117000.00', '7500', '0.00', 0, 0, 2, 0),
(63, 'SHORTHAND NOTEBOOK', 'ROYAL', 4, 0, '', 'A4 TOP SPIRAL\r\n70 SHEETS\r\n', '1', '99000.00', '2500.00', '119000.00', '0', '1.00', 4, 0, 1, 0),
(64, 'PEN ', 'CELLO', 3, 6, '', 'GRIPPER\r\n12 OUTER PER CARTON\r\n250 PCS PER OUTER', '5', '690000.00', '500.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(65, 'TRANSPARENT ', 'STATIONERIES', 4, 6, '', 'CLEAR\r\n20 PCS PER CARTON', '1', '140000.00', '9000.00', '170000.00', '0', '1.00', 1, 0, 1, 0),
(66, 'TRANSPARENT', 'CHINESE', 4, 6, '', 'BLUE\r\n20 PCS PER CARTON', '1', '160000.00', '10000.00', '190000.00', '0', '1.00', 0, 0, 1, 0),
(67, 'NOTEBOOK', 'ROYAL', 4, 6, '', 'HARDCOVER\r\n4 QUIRE\r\n36 PCS PER CARTON', '1', '84000.00', '4000.00', '99000.00', '34600', '0.00', 3, 0, 1, 0),
(68, 'CORRECTION BOTTLES', 'ANYWAY', 4, 6, '', '40 BOXES IN A CARTON\r\n12 PCS PER BOX', '1', '216000.00', '1000.00', '216000.00', '7000', '7000.00', 0, 0, 1, 0),
(69, 'COLOUR PENCIL', 'NATARAJ', 3, 6, '', '90 MM\r\n10 OUTERS PER CARTON\r\n120 PCS PER CARTON', '1', '250000.00', '3500.00', '272000.00', '30600', '25500.00', 0, 0, 0, 0),
(70, 'PEN', 'NATARAJ', 3, 6, '', 'SULFER BLUE\r\n24 BOXES PER CARTON\r\n50 PENS PER BOX', '1', '192000.00', '500.00', '240000.00', '0', '12000.00', 1, 0, 1, 0),
(71, 'CELLOTAPE', 'STATIONERIES', 4, 6, '', 'CT-12\r\n60 DOZENS PER CARTON', '1', '72500.00', '500.00', '110000.00', '2000', '2000.00', 0, 0, 1, 0),
(72, 'PACKING TAPE', 'CHINESE', 7, 6, '', 'CLEAR\r\nPT-400\r\n36 PCS IN CARTON', '1', '198000.00', '7000.00', '219000.00', '36000', '0.00', 0, 0, 0, 0),
(73, 'SUBJECT BOOK', 'HAI BIN', 4, 6, '', 'NO 3\r\n96 PCS PER CARTON', '1', '144000.00', '3000.00', '170000.00', '0', '1.00', 0, 0, 0, 0),
(74, 'SUBJECT BOOK', 'HAI BIN', 4, 6, '', 'NO 5 SMALL\r\n72 PCS PER CARTON', '1', '180000.00', '4000.00', '210000.00', '0', '1.00', 0, 0, 0, 0),
(75, 'SUBJECT BOOK', 'HAI BIN', 4, 6, '', 'NO 5 BIG\r\n', '1', '168000.00', '5000.00', '193000.00', '0', '1.00', 0, 0, 0, 0),
(76, 'ID HOLDER (KAMBA)', 'CHINESE', 4, 6, '', '2.0 LY-20P-BL', '1', '580000.00', '1000.00', '70000.00', '0', '45000.00', 0, 0, 0, 0),
(77, 'THERMAL ROLLS', 'CHINESE', 4, 6, '', '57X40 PLAIN\r\n100 PCS PER CARTON', '1', '65000.00', '1500.00', '120000.00', '0', '14000.00', 0, 0, 5, 0),
(78, 'SCISSORS', 'DL', 4, 6, '', 'DL-55\r\n30 BOXES PER CARTON\r\n12 PCS PER BOX', '1', '324000.00', '1500.00', '430000.00', '13000', '13000.00', 0, 0, 0, 0),
(79, 'SCISSORS', 'DL', 4, 6, '', 'DL-55', '1', '324000.00', '2500.00', '340000.00', '19500', '19500.00', 0, 0, 0, 0),
(80, 'SCISSORS', 'DL', 4, 6, '', 'DL-95\r\n15 BOXS PER CARTON', '1', '324000.00', '2500.00', '280000.00', '26000', '26000.00', 0, 0, 0, 0),
(81, 'BINDING TAPE', 'STATIONERIES', 4, 6, '', 'BLACK \r\n60 PCS PER CARTON', '1', '180000.00', '4000.00', '205000.00', '0', '205000.00', 0, 0, 1, 0),
(82, 'OFFICE PINS', 'STATIONERIES', 4, 6, '', '10O BOXES PER CARTON\r\n10 BOXES IN EACH BOX', '1', '210000.00', '500.00', '240000.00', '2500', '2500.00', 1, 0, 0, 0),
(83, 'ID CARD HOLDER', 'XINHUASHENG', 4, 6, '', 'H061', '1', '400000.00', '300.00', '550000.00', '0', '72500.00', 0, 0, 0, 0),
(84, 'STAMP PAD', 'HUHUA', 4, 6, '', 'NO 3\r\n20 DOZENS PER CARTON', '1', '216000.00', '1500.00', '195000.00', '12000', '0.00', 0, 0, 0, 0),
(85, 'STAMPAD INK', 'HUHUA', 4, 6, '', '30 DOZENS PER CARTON\r\n', '1', '120000.00', '1000.00', '150000.00', '13500', '0.00', 0, 0, 0, 0),
(86, 'THUMB TACKS', 'STATIONERIES', 4, 6, '', '100 BOXES PER CARTON\r\n10 BOXES PER BOX', '1', '120000.00', '2500.00', '160000.00', '2500', '2500.00', 2, 0, 1, 0),
(87, 'COLOUR PENCIL', 'CHINESE', 4, 6, '', 'SMALL\r\n40 DOZENS IN CARTON', '1', '156000.00', '1000.00', '190200.00', '0', '5000.00', 0, 0, 0, 0),
(88, 'STORE LEDGER', 'ROYAL', 4, 6, '', '30 PCS IN A CARTON', '1', '115000.00', '6000.00', '135000.00', '54000', '0.00', 0, 0, 0, 0),
(89, 'CASH BOOK', 'ROYAL', 4, 6, '', '30 PCS IN CARTON', '1', '115000.00', '6000.00', '135000.00', '54000', '0.00', 3, 0, 0, 0),
(90, 'CORRECTION PEN', 'ANYWAY', 4, 6, '', '40 BOXES PER CARTON', '1', '216000.00', '1500.00', '262000.00', '8500', '8500.00', 0, 0, 0, 0),
(91, 'SPRINGFILE', 'ACADEMY', 8, 6, '', 'MANILA ', '1', '110000.00', '1000.00', '125000.00', '0', '0.00', 6, 0, 0, 0),
(92, 'TRAFFIC FILE', 'CHINESE', 8, 6, '', '60 PCS PER CARTON', '1', '72000.00', '3000.00', '102000.00', '20400', '0.00', 2, 0, 1, 0),
(93, 'MANILLA', 'STATIONERIES', 1, 6, '', 'EMBOSSED ', '1', '110000.00', '8000.00', '150000.00', '0', '8000.00', 0, 0, 0, 0),
(94, 'GUEST HOUSE REGISTER', 'ROYAL', 4, 6, '', '60 PCS IN CARTON', '1', '110000.00', '3000.00', '130000.00', '28000', '0.00', 2, 0, 1, 0),
(95, 'THERMAL ROLLS ', 'CHINESE', 4, 8, '', '57X50 PRINTED', '1', '520000.00', '2500.00', '720000.00', '0', '23000.00', 0, 0, 0, 0),
(96, 'MASKIN TAPE', 'STATIONERIES', 4, 6, '', '12 DOZENS IN CARTON', '1', '54000.00', '1000.00', '68000.00', '7500', '7500.00', 3, 0, 1, 0),
(97, 'STAPLER', 'KANGAROO', 2, 6, '', 'DS-45NR\r\n120 PCS PER CARTON\r\n12 OUTER WITH 10 PCS', '1', '492000.00', '6000.00', '540000.00', '54000', '45000.00', 0, 0, 0, 0),
(98, 'PENCIL', 'CHINESE', 3, 6, '', 'YELLOW\r\n20 GROSS IN CARTON\r\n144 PCS PER GROSS', '1', '136000.00', '100.00', '180000.00', '800', '9000.00', 1, 0, 0, 0),
(99, 'STICKY NOTE', 'STATIONERIES', 4, 6, '', 'MEMO \r\n3X3\r\n24 DOZENS PER CARTON', '1', '115000.00', '1000.00', '141000.00', '0', '141000.00', 0, 0, 0, 0),
(100, 'EXERCISE BOOK ', 'BALAJI', 5, 6, '', '192 PAGES\r\n60 PCS IN CARTON', '1', '69000.00', '2000.00', '75000.00', '0', '75000.00', 4, 0, 2, 0),
(101, 'LAMINATION POUCH', 'STATIONERIES', 4, 6, '', 'A5\r\n20 PCS IN CARTON', '1', '200000.00', '15000.00', '170000.00', '0', '15000.00', 0, 0, 0, 0),
(102, 'BLACKBOARD RULER ', 'CHINESE', 4, 6, '', '1 METER', '2', '150000.00', '3000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(103, 'PEN', 'CELLO', 3, 6, '', 'FINEGRIP (OUTER) \r\n12 OUTER PER CARTON\r\n250 PCS PER OUTER', '1', '690000.00', '500.00', '840000.00', '0', '70000.00', 0, 0, 0, 0),
(104, 'MATHEMATICAL SET', 'MARSHAL', 4, 6, '', '144 PCS PER CARTON', '1', '216000.00', '3000.00', '250000.00', '0', '23000.00', 0, 0, 0, 0),
(105, 'MATHEMATICAL SET', 'NATARAJ', 4, 6, '', '96 PCS PER CARTON', '1', '290000.00', '5000.00', '335000.00', '0', '44000.00', 0, 0, 0, 0),
(106, 'SHARPENER', 'CHINESE', 4, 6, '', '48 JARS IN CARTON\r\n96 PCS PER JAR', '1', '240000.00', '6000.00', '280000.00', '0', '6000.00', 0, 0, 0, 0),
(107, 'SHORTHAND NOTEBOOK', 'ROYAL', 4, 6, '', '3X5\r\nTOP SPIRAL\r\n', '1', '108000.00', '1000.00', '135000.00', '0', '4500.00', 0, 0, 0, 0),
(108, 'TONER', 'CHINESE', 9, 10, '', 'CEXV 14', '2', '16500.00', '35000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(109, 'TONER', 'KIYO', 9, 10, '', 'KIYO FOR CANON', '2', '23500.00', '35000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(110, 'TONER', 'CHINESE', 9, 10, '', 'C-EXV 18', '2', '30000.00', '50000.00', '0.00', '0', '0.00', 0, 0, 0, 0),
(111, 'TONER', 'CHINESE', 9, 10, '', 'C-EXV 42', '2', '20000.00', '35000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(112, 'TONER', 'CHINESE', 9, 10, '', 'C-EXV 33', '2', '28500.00', '45000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(113, 'TONER', 'NRG', 9, 10, '', 'CPi 10', '2', '20000.00', '35000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(114, 'TONER ', 'CHINESE', 9, 10, '', 'SILVER POWDER 1 KG', '2', '25000.00', '38000.00', '0.00', '0', '0.00', 0, 0, 15, 0),
(115, 'TONER', 'CHINESE', 9, 10, '', 'SILVER POWDER 1/2 KG', '2', '12500.00', '20000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(116, 'PICK UP ROLLER', 'CHINESE', 10, 10, '', '2016/2318/2018', '2', '14000.00', '25000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(117, 'LAMINATION POUCH', 'STATIONERIES', 4, 6, '', 'A5 20 PCS PER CARTON', '1', '200000.00', '15000.00', '230000.00', '0', '15000.00', 0, 0, 0, 0),
(118, 'SHORTHAND NOTEBOOK', 'ROYAL', 4, 6, '', '4X6 70 SHEETS', '1', '108000.00', '1000.00', '117000.00', '0', '6000.00', 0, 0, 0, 0),
(119, 'ABUCUS', 'CHINESE', 4, 8, '', 'SMALL \r\n216 PCS PER CTN', '1', '345600.00', '4000.00', '460000.00', '0', '216.00', 1, 0, 0, 0),
(120, 'MATHEMATICAL SET', 'MARSHAL', 4, 6, '', 'MARSHAL\r\n144 PCS PER CARTON', '1', '216000.00', '3000.00', '250000.00', '23000', '23000.00', 0, 0, 0, 0),
(121, 'MATHEMATICAL SET', 'NATARAJ', 4, 6, '', 'NATARAJ ', '1', '290000.00', '50000.00', '340000.00', '44000', '37000.00', 0, 0, 0, 0),
(122, 'SHORTHAND NOTEBOOK', 'ROYAL', 4, 6, '', 'MEMO 3X5\r\n20 DOZENS PER CARTON', '1', '108000.00', '500.00', '120000.00', '4500', '4500.00', 0, 0, 0, 0),
(123, 'NUMBERING MACHINE', 'CHINESE', 2, 6, '', '6 DIGITS', '2', '12000.00', '20000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(124, 'NUMBERING MACHINE', 'CHINESE', 2, 6, '', '8 DIGITS', '2', '17000.00', '27000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(125, 'CATRIDGE', 'UNICORN', 9, 10, '', 'UNICORN 05A', '2', '35000.00', '80000.00', '0.00', '0', '0.00', 0, 0, 15, 0),
(126, 'CATRIDGE', 'UNICORN', 9, 10, '', 'UNICORN 80A', '2', '35000.00', '80000.00', '0.00', '0', '0.00', 0, 0, 15, 0),
(127, 'CATRIDGE', 'UNICORN', 9, 10, '', 'UNICORN 85 A', '2', '35000.00', '80000.00', '0.00', '0', '0.00', 0, 0, 15, 0),
(128, 'CATRIDGE', 'UNICORN', 9, 10, '', 'UNICORN 83A', '2', '35000.00', '80000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(129, 'CATRIDGE', 'UNICORN', 9, 10, '', 'UNICORN 78A', '2', '35000.00', '80000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(130, 'CATRIDGE', 'UNICORN', 9, 10, '', 'UNICORN 17A', '2', '120000.00', '180000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(131, 'CATRIDGE', 'UNICORN', 9, 10, '', 'UNICORN 26A', '2', '120000.00', '180000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(132, 'CATRIDGE', 'PREMIUM', 9, 10, '', 'PREMIUM 26A', '2', '100000.00', '180000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(133, 'CALCULATOR', 'CITIZEN', 2, 6, '', 'CT-912', '2', '6000.00', '12000.00', '0.00', '0', '0.00', 0, 0, 10, 0),
(134, 'CALCULATOR', 'CASIO ', 2, 6, '', 'DJ-240', '2', '32000.00', '45000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(135, 'CALCULATOR', 'CASIO', 2, 6, '', 'DJ-120', '2', '24000.00', '35000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(136, 'CALCULATOR', 'CASIO', 2, 6, '', 'MJ-120', '2', '20000.00', '25000.00', '0.00', '0', '0.00', 0, 0, 3, 0),
(137, 'ANALYSIS BOOK', 'MAXON', 4, 11, '', '1 QUIRE, 36 COLUMN', '2', '9000.00', '18000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(138, 'ANALYSIS BOOK', 'MAXON', 4, 11, '', '2 QUIRE, 36 COLUMN', '2', '14000.00', '22000.00', '0.00', '0', '0.00', 0, 0, 1, 0),
(139, 'ANALYSIS BOOK', 'MAXON', 4, 11, '', '1 QUIRE, 14 COLUMN', '2', '5500.00', '13000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(140, 'ANALYSIS BOOK', 'MAXON', 4, 11, '', '2 QUIRE, 14 COLUMN', '2', '9000.00', '18000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(141, 'ANALYSIS BOOK', 'MAXON', 4, 11, '', '1 QUIRE, 7 COLUMN', '2', '5500.00', '12000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(142, 'ANALYSIS BOOK', 'MAXON', 4, 11, '', '2 QUIRE, 7 COLUMN', '2', '9000.00', '18000.00', '0.00', '0', '0.00', 0, 0, 2, 0),
(143, 'ENVELOPE', 'NGALAWA', 6, 9, '', 'SIZE: A6 \r\n500 PCS PER OUTER \r\n10 OUTER PER CARTON', '1', '115000.00', '100.00', '150000.00', '0', '15000.00', 6, 0, 5, 0),
(144, 'DVD-R', 'CHINESE', 4, 8, '', '12 PCS PER CARTON', '1', '168000.00', '17000.00', '192000.00', '0', '17000.00', 4, 0, 3, 0),
(145, 'CHALK', 'CONTINENTAL', 4, 13, '', '30 BOXES IN CARTON', '1', '23500.00', '2000.00', '28000.00', '0', '2000.00', 70, 0, 20, 0),
(146, 'MANILLA', 'TYPECK', 1, 9, '', 'YELLOW, 10 PCS IN CARTON', '1', '44000.00', '5000.00', '50000.00', '0', '5000.00', 8, 0, 4, 0),
(147, 'MANILLA', 'TYPECK', 1, 9, '', 'BLUE, 10 PCS IN CARTON', '1', '44000.00', '5000.00', '50000.00', '0', '5000.00', 26, 0, 5, 0),
(148, 'PEN', 'BALAJI', 3, 14, '', 'BLUE \r\n4 OUTER IN CARTON\r\n20 BOXES IN OUTER\r\n50 PCS IN EACH BOX', '1', '300000.00', '200.00', '325000.00', '0', '85000.00', 0, 0, 5, 0),
(149, 'PEN', 'BALAJI', 3, 14, '', 'RED\r\n4 OUTER PER CARTON\r\n20 BOXES PER OUTER', '1', '300000.00', '200.00', '325000.00', '0', '85000.00', 2, 0, 3, 0),
(150, 'PEN', 'BALAJI', 3, 14, '', 'BLACK\r\n4 OUTER PER CARTON\r\n20 BOXES PER OUTER', '1', '300000.00', '200.00', '325000.00', '0', '85000.00', 0, 0, 5, 0),
(151, 'PHOTOCOPY PAPER ', 'MULTIBRAND', 1, 10, '', 'SIZE: A4', '1', '41000.00', '9000.00', '43000.00', '0', '9000.00', 0, 0, 40, 0),
(152, 'EXERCISE BOOK', 'MSOMI', 5, 7, '', 'MSOMI 100 PCS IN CARTON', '1', '35000.00', '600.00', '42000.00', '0', '42000.00', 0, 0, 10, 0),
(153, 'PHOTOCOPY PAPER', 'MULTI BRAND', 1, 7, '', 'A3\r\n5 PCS PER CARTON', '1', '75000.00', '18000.00', '90000.00', '0', '18000.00', 11, 0, 5, 0),
(154, 'EXERCISE BOOK', 'NGALAWA', 5, 9, '', 'NGALAWA 100 PAGES SINGLE LINE\r\n200 PCS PER CARTON', '1', '83000.00', '5000.00', '88000.00', '0', '1.00', 21, 0, 10, 0),
(155, 'OFFICE GLUE', 'VISTA BOND', 4, 15, '', '1 KILOGRAM\r\n36 PCS IN CARTON', '1', '24000.00', '5000.00', '36000.00', '0', '0.00', 17, 0, 5, 0),
(156, 'OFFICE GLUE', 'VISTA BOND', 4, 7, '', '90 GRAMS\r\n36 PCS PER CARTON', '1', '15000.00', '1000.00', '21000.00', '7500', '0.00', 8, 0, 5, 0),
(157, 'THERMAL ROLL', 'CHINESE', 1, 8, '', '57X40 PRINTED\r\n400 PCS IN CARTON', '1', '320000.00', '2000.00', '500000.00', '0', '14000.00', 5, 0, 2, 0),
(158, 'MARKER PEN', 'TYNO', 3, 6, '', 'PERMANENT \r\n10 OUTER PER CARTON\r\n10 BOXES PER OUTER\r\n12 PCS PER BOX', '1', '420000.00', '1000.00', '490000.00', '0', '55000.00', 1, 0, 0, 0),
(159, 'PENCIL', 'CHINESE', 3, 6, '', 'POUCH\r\nDSN-48\r\n60 BAGS IN CARTON\r\n', '1', '180000.00', '200.00', '210000.00', '0', '4000.00', 0, 0, 1, 0),
(160, 'RIBBON', 'STATINERIES', 4, 6, '', 'BLACK\r\n50 PCS IN CARTON\r\n', '1', '120000.00', '3500.00', '150000.00', '0', '1.00', 0, 0, 0, 0),
(161, 'RIBBON', 'STATIONERIES', 4, 6, '', 'PURPLE\r\n50 PCS IN CARTON', '1', '120000.00', '3500.00', '150000.00', '0', '1.00', 0, 0, 1, 0),
(162, 'EXERCISE BOOK', 'NGALAWA', 5, 9, '', 'NGALAWA 200 PGS SQUARE \r\n100 PCS PER CARTON\r\n', '1', '71000.00', '1000.00', '75000.00', '0', '1.00', 0, 0, 10, 0),
(163, 'EXERCISE BOOK', 'NGALAWA', 5, 9, '', 'NGALAWA 200 PGS PLAIN\r\n100 PCS PER CARTON', '1', '71000.00', '1000.00', '75000.00', '0', '1.00', 0, 0, 5, 0),
(164, 'EXERCISE BOOK', 'NGALAWA', 5, 6, '', 'NGALAWA 200 PGS DOUBLE LINE\r\n100 PCS PER CARTON', '1', '71000.00', '1000.00', '75000.00', '0', '1.00', 0, 0, 5, 0),
(165, 'WRITING PAD', 'ROYAL', 5, 6, '', 'ROYAL IMPORTED\r\n72 PCS IN CARTON', '1', '68000.00', '2000.00', '72000.00', '0', '0.00', 0, 0, 2, 0),
(166, 'VEHECLE LOG BOOK', 'ROYAL', 4, 6, '', '60 PCS IN CARTON', '1', '84000.00', '2000.00', '104000.00', '0', '1.00', 0, 0, 1, 0),
(167, 'OFFICE GLUE', 'VISTA BOND', 4, 15, '', '160 GRAMS\r\n24 PCS IN CARTON', '1', '16000.00', '2000.00', '22000.00', '11500', '0.00', 0, 0, 2, 0),
(168, 'KAMBA ', 'CHINESE', 4, 6, '', '1.5 BL (KATI)', '1', '400000.00', '500.00', '450000.00', '36000', '30000.00', 0, 0, 0, 0),
(169, 'STAMPAD INK', 'CHINESE', 4, 6, '', '30 DOZENS IN CARTON', '1', '120000.00', '1000.00', '150000.00', '5500', '0.00', 0, 0, 0, 0),
(170, 'SHARPENER', 'CHINESE', 4, 6, '', '48 JARS IN CARTON\r\nEACH JAR WITH 96 PCS', '1', '300000.00', '200.00', '330000.00', '0', '7500.00', 0, 0, 0, 0),
(171, 'TONER C-EXV 14/ GPR-18', 'CHINESE', 9, 10, '', '24 PCS IN CARTON', '1', '396000.00', '0.00', '528000.00', '0', '0.00', 0, 0, 1, 0),
(172, 'TONER C EXV 42', 'CHINESE', 9, 10, '', '24 PCS IN CARTON', '1', '480000.00', '35000.00', '576000.00', '0', '0.00', 0, 0, 0, 0),
(173, 'TONER C EXV 33', 'CHINESE', 9, 10, '', '24 PCS IN CARTON', '1', '686000.00', '45000.00', '840000.00', '0', '0.00', 0, 0, 0, 0),
(174, 'TONER CPi 10/ CPi 7', 'NRG', 9, 10, '', '20 PCS IN CARTON', '1', '400000.00', '35000.00', '600000.00', '360000', '150000.00', 0, 0, 1, 0),
(175, 'TONER 1 KG', 'CHINESE', 9, 10, '', '1 KG\r\n24 PCS IN CARTON', '1', '432000.00', '35000.00', '600000.00', '0', '0.00', 0, 0, 0, 0),
(176, 'TONER POWDER 1/2 KG', 'CHINESE', 9, 10, '', 'POWDER\r\n40 PCS IN CARTON', '1', '500000.00', '20000.00', '560000.00', '0', '0.00', 0, 0, 0, 0),
(177, 'MASTER ROLL', 'NRG', 4, 10, '', 'CPMT- 21', '2', '25000.00', '50000.00', '0.00', '0', '0.00', 0, 0, 0, 0),
(178, 'STAPLER', 'KANGAROO', 2, 6, '', 'DS-425', '1', '375000.00', '9000.00', '410000.00', '83400', '69500.00', 0, 0, 0, 0),
(179, 'SPIRAL', 'CHINESE', 4, 6, '', '8 MM\r\n20 OUTERS IN CARTON', '1', '100000.00', '9000.00', '180000.00', '0', '9000.00', 0, 0, 0, 0),
(180, 'MAGAZINE RACK', 'CHINESE', 4, 6, '', '3 TRAYS\r\n40 PCS IN CARTON', '1', '140000.00', '12000.00', '320000.00', '84000', '0.00', 0, 0, 0, 0),
(181, 'SPIRAL', 'CHINESE', 4, 6, '', '10 MM\r\n20 OUTERS PER CARTON', '1', '120000.00', '10000.00', '0.00', '0', '10000.00', 0, 0, 0, 0),
(182, 'SPIRAL', 'CHINESE', 4, 6, '', '14 MM\r\n10 OUTERS PER CARTON', '1', '80000.00', '14000.00', '0.00', '0', '14000.00', 0, 0, 0, 0),
(183, 'OFFICE TRAY (CHUMA)', 'CHINESE', 4, 6, '', '4 LAYERS\r\n12 PCS IN CARTON\r\n12 OUTERS PER CARTON', '1', '168000.00', '20000.00', '204000.00', '204000', '20000.00', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_units`
--

CREATE TABLE `item_units` (
  `item_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `outers_per_ctn` int(11) DEFAULT NULL,
  `pcs_per_outer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_units`
--

INSERT INTO `item_units` (`item_id`, `unit_id`, `outers_per_ctn`, `pcs_per_outer`) VALUES
(1, 1, 8, 6),
(2, 1, 6, 6),
(3, 1, 1, 1),
(4, 1, 1, 1),
(5, 1, 1, 1),
(6, 1, 1, 1),
(7, 1, 11, 1),
(8, 1, 1, 1),
(9, 1, NULL, NULL),
(10, 1, NULL, NULL),
(11, 1, NULL, NULL),
(12, 1, NULL, NULL),
(13, 1, NULL, NULL),
(14, 1, NULL, NULL),
(15, 1, NULL, NULL),
(16, 1, NULL, NULL),
(17, 1, NULL, NULL),
(18, 1, NULL, NULL),
(19, 2, NULL, NULL),
(20, 2, NULL, NULL),
(21, 1, NULL, NULL),
(22, 1, NULL, NULL),
(23, 2, NULL, NULL),
(24, 1, NULL, NULL),
(25, 1, NULL, NULL),
(26, 2, NULL, NULL),
(27, 2, NULL, NULL),
(28, 2, 0, 0),
(29, 1, NULL, NULL),
(30, 1, NULL, NULL),
(31, 1, 1, 1),
(32, 1, 1, 1),
(33, 1, 1, 1),
(34, 1, 1, 1),
(35, 1, NULL, NULL),
(36, 1, NULL, NULL),
(37, 1, NULL, NULL),
(38, 1, NULL, NULL),
(39, 1, NULL, NULL),
(40, 1, NULL, NULL),
(41, 1, NULL, NULL),
(42, 1, NULL, NULL),
(43, 1, NULL, NULL),
(44, 1, NULL, NULL),
(45, 1, 1, 1),
(46, 1, NULL, NULL),
(47, 1, NULL, NULL),
(48, 1, NULL, NULL),
(49, 1, NULL, NULL),
(50, 1, NULL, NULL),
(51, 1, 1, 1),
(52, 1, 1, 1),
(53, 1, NULL, NULL),
(54, 1, NULL, NULL),
(55, 1, NULL, NULL),
(56, 1, NULL, NULL),
(57, 1, 1, 1),
(58, 1, 30, 20),
(59, 1, NULL, NULL),
(61, 2, 0, 0),
(62, 1, NULL, NULL),
(63, 1, 1, 1),
(64, 5, 0, 0),
(65, 1, 1, 1),
(66, 1, 1, 1),
(67, 1, NULL, NULL),
(68, 1, NULL, NULL),
(69, 1, NULL, NULL),
(70, 1, NULL, NULL),
(71, 1, NULL, NULL),
(72, 1, NULL, NULL),
(73, 1, 1, 1),
(74, 1, 1, 1),
(75, 1, 1, 1),
(76, 1, 20, 100),
(77, 1, 10, 10),
(78, 1, NULL, NULL),
(79, 1, NULL, NULL),
(80, 1, NULL, NULL),
(81, 1, NULL, NULL),
(82, 1, NULL, NULL),
(83, 1, NULL, NULL),
(84, 1, NULL, NULL),
(85, 1, NULL, NULL),
(86, 1, NULL, NULL),
(87, 1, 40, 12),
(88, 1, NULL, NULL),
(89, 1, NULL, NULL),
(90, 1, NULL, NULL),
(91, 1, NULL, NULL),
(92, 1, NULL, NULL),
(93, 1, 20, 100),
(94, 1, NULL, NULL),
(95, 1, 40, 10),
(96, 1, NULL, NULL),
(97, 1, NULL, NULL),
(98, 1, NULL, NULL),
(99, 1, 1, 1),
(100, 1, 1, 1),
(101, 1, 20, 100),
(102, 2, 0, 0),
(103, 1, NULL, NULL),
(104, 1, 12, 12),
(105, 1, 8, 12),
(106, 1, 48, 96),
(107, 1, 30, 12),
(108, 2, 0, 0),
(109, 2, 0, 0),
(110, 2, 0, 0),
(111, 2, 0, 0),
(112, 2, 0, 0),
(113, 2, 0, 0),
(114, 2, 0, 0),
(115, 2, 0, 0),
(116, 2, 0, 0),
(117, 1, 20, 20),
(118, 1, 20, 12),
(119, 1, 1, 1),
(120, 1, NULL, NULL),
(121, 1, NULL, NULL),
(122, 1, NULL, NULL),
(123, 2, 0, 0),
(124, 2, 0, 0),
(125, 2, 0, 0),
(126, 2, 0, 0),
(127, 2, 0, 0),
(128, 2, 0, 0),
(129, 2, 0, 0),
(130, 2, 0, 0),
(131, 2, 0, 0),
(132, 2, 0, 0),
(133, 2, 0, 0),
(134, 2, NULL, NULL),
(135, 2, NULL, NULL),
(136, 2, 0, 0),
(137, 2, 0, 0),
(138, 2, 0, 0),
(139, 2, 0, 0),
(140, 2, 0, 0),
(141, 2, 0, 0),
(142, 2, 0, 0),
(143, 1, 10, 500),
(144, 1, 12, 50),
(145, 1, 30, 100),
(146, 1, 10, 100),
(147, 1, 10, 100),
(148, 1, 4, 20),
(149, 1, 4, 20),
(150, 1, 4, 20),
(151, 1, 5, 500),
(152, 1, 1, 1),
(153, 1, 5, 500),
(154, 1, 1, 1),
(155, 1, NULL, NULL),
(156, 1, NULL, NULL),
(157, 1, 40, 10),
(158, 1, 10, 10),
(159, 1, NULL, NULL),
(160, 1, 1, 1),
(161, 1, 1, 1),
(162, 1, 1, 1),
(163, 1, 1, 1),
(164, 1, 1, 1),
(165, 1, NULL, NULL),
(166, 1, 1, 1),
(167, 1, NULL, NULL),
(168, 1, NULL, NULL),
(169, 1, NULL, NULL),
(170, 1, NULL, NULL),
(171, 1, NULL, NULL),
(172, 1, NULL, NULL),
(173, 1, NULL, NULL),
(174, 1, NULL, NULL),
(175, 1, NULL, NULL),
(176, 1, NULL, NULL),
(177, 2, NULL, NULL),
(178, 1, NULL, NULL),
(179, 1, NULL, NULL),
(180, 1, NULL, NULL),
(181, 1, NULL, NULL),
(182, 1, NULL, NULL),
(183, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message`) VALUES
(1, 'Dear  $name . Thank you for shopping with us. Your invoice number is $invoice ');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(20171101030723);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `ref_number` varchar(255) DEFAULT NULL,
  `ordered_by` int(11) DEFAULT NULL,
  `order_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `processed_by` int(11) DEFAULT NULL,
  `processed_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `ref_number`, `ordered_by`, `order_date`, `processed_by`, `processed_date`, `status`) VALUES
(5, '1536540887', 16, '2018-09-10 03:54:54', NULL, NULL, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `store` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item`, `quantity`, `store`) VALUES
(5, 1536540770, 119, 50, 1),
(6, 1536540887, 138, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number_1` varchar(255) DEFAULT NULL,
  `phone_number_2` varchar(255) DEFAULT NULL,
  `phone_number_3` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `comments` text,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `first_name`, `last_name`, `gender`, `phone_number_1`, `phone_number_2`, `phone_number_3`, `email`, `address`, `city`, `region`, `district`, `comments`, `deleted`) VALUES
(1, 'Samwel', 'Mongi', 1, '255685848634', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 'GLORY', 'MONGI', 0, '255757904833', '255', '', NULL, 'KIBAIGWA', NULL, 'DODOMA', 'KONGWA', NULL, 0),
(3, 'SAMWEL', 'MONGI', 1, '255685848634', '255', '', 'sammongi@gmail.com', 'MBEYA', 'MBEYA', 'MBEYA', 'MBEYA TOWN', NULL, 0),
(4, 'GRACE', 'MONGI', 0, '255755480057', '255658480057', '', 'gracemongi@gmail.com', 'MBEYA', 'MBEYA', 'MBEYA', 'MBEYA TOWN', NULL, 0),
(5, 'ANNA', 'JOHN', 0, '255769351116', '', '', NULL, 'MBEYA', 'MBEYA', 'MBEYA', 'MBEYA MJINI', NULL, 0),
(6, 'AZIZI', 'KINGS', 1, '255754275252', '', '', NULL, 'DAR ES SALAAM', 'DAR ES SALAAM', 'DAR ES SALAAM', 'ILALA', NULL, 0),
(7, 'ANWARY', 'MBAGALA', 1, '255715395405', '', '', NULL, 'DAR ES SALAAM', 'DAR ES SALAAM', 'DAR ES SALAAM', 'TEMEKE', NULL, 0),
(8, 'SEBASTIAN', 'KOMANYA', 1, '255756547444', '255715547444', '255715667578', NULL, 'DAR ES SALAAM', 'DAR ES SALAAM', 'DAR ES SALAAM', 'ILALA', NULL, 0),
(9, 'NAYAK', 'NAYAK', 1, '255784873734', '', '', NULL, 'DAR ES SALAAM', 'DAR ES SALAAM', 'DAR ES SALAAM', 'ILALA', NULL, 0),
(10, 'MUDY', 'TECK', 1, '255684443272', '255759443272', '', NULL, 'ILALA', 'DAR ES SALAAM', 'DAR ES SALAAM', 'ILALA', NULL, 0),
(11, 'DADA', 'FURAHA', 0, '255744777788', '', '', NULL, 'DAR ES SALAAM', 'DAR ES SALAAM', 'DAR ES SALAAAM', 'ILALA', NULL, 0),
(12, 'SAMWEL', 'MONGI', NULL, '255684848634', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, 'MIKE', 'MPIGAUZI', 1, '255753638910', '255715599004', '', NULL, 'DAR ES SALAAM', 'ILALA', 'ILALA', 'ILALA', NULL, 0),
(14, 'BALAJI', 'PERSONAL', 1, '255786602891', '255685576557', '255715602891', NULL, 'DAR ES SALAAM', 'DAR ES SALAAM', 'TEMEKE', 'TEMEKE', NULL, 0),
(15, 'ERICK', 'LISSA', 1, '255678061060', '255786928935', '255717020929', NULL, 'DAR ES SALAAM', NULL, 'DAR ES SALAAM', 'ILALA', NULL, 0),
(16, 'Moses', 'Zengo', 1, '255764713138', '', '', 'moizes92@gmail.com', 'Dangote', NULL, 'Dar', 'Dar', NULL, 0),
(17, 'Kawawa', 'Tim', NULL, '25565843138', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice`
--

CREATE TABLE `proforma_invoice` (
  `id` int(11) NOT NULL,
  `invoice_number` varchar(32) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `sub_total` decimal(15,2) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `VAT` decimal(15,2) NOT NULL,
  `total_VAT` decimal(15,2) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'pending',
  `comment` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proforma_invoice`
--

INSERT INTO `proforma_invoice` (`id`, `invoice_number`, `customer_id`, `employee_id`, `sub_total`, `discount`, `VAT`, `total_VAT`, `date_created`, `status`, `comment`) VALUES
(55, '1573628936', 17, 1, '10800.00', 0, '1944.00', '12744.00', '2019-11-13 08:08:56', 'pending', NULL),
(56, '1573629189', 17, 1, '4500.00', 0, '810.00', '5310.00', '2019-11-13 08:13:09', 'pending', NULL),
(57, '1573629230', 17, 1, '8200.00', 0, '1476.00', '9676.00', '2019-11-13 08:13:50', 'pending', NULL),
(58, '1573629753', 12, 1, '2500.00', 0, '450.00', '2950.00', '2019-11-13 08:22:33', 'pending', NULL),
(59, '1573648034', 12, 16, '4500.00', 0, '810.00', '5310.00', '2019-11-13 13:27:14', 'pending', NULL),
(60, '1573710211', 17, 16, '2000.00', 0, '360.00', '2360.00', '2019-11-14 06:43:31', 'pending', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `proforma_items`
--

CREATE TABLE `proforma_items` (
  `id` int(11) NOT NULL,
  `invoice_number` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(11) NOT NULL,
  `unit_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proforma_items`
--

INSERT INTO `proforma_items` (`id`, `invoice_number`, `item`, `quantity`, `unit`, `unit_price`) VALUES
(47, 1573628936, 1, 1, '0', 2000),
(48, 1573628936, 2, 1, '0', 2500),
(49, 1573628936, 3, 1, '0', 6000),
(50, 1573628936, 4, 1, '0', 300),
(51, 1573629189, 1, 1, '0', 2000),
(52, 1573629189, 2, 1, '0', 2500),
(53, 1573629230, 1, 1, '0', 2000),
(54, 1573629230, 3, 1, '0', 6000),
(55, 1573629230, 6, 1, '0', 200),
(56, 1573629753, 2, 1, '0', 2500),
(57, 1573648034, 1, 1, '0', 2000),
(58, 1573648034, 2, 1, '0', 2500),
(59, 1573710211, 1, 1, 'Piece', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `sale_time` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `comment` text,
  `invoice_number` varchar(32) NOT NULL,
  `amount_paid` decimal(15,2) NOT NULL,
  `amount_due` decimal(15,2) NOT NULL,
  `discount` decimal(15,2) NOT NULL,
  `sale_status` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `sale_time`, `customer_id`, `employee_id`, `comment`, `invoice_number`, `amount_paid`, `amount_due`, `discount`, `sale_status`) VALUES
(9, '2018-05-21', 12, 3, '', '2018052123213452497', '500.00', '0.00', '0.00', 0),
(10, '2018-05-21', 12, 3, '', '2018052138635386707', '27000.00', '500.00', '0.00', 1),
(15, '2019-10-22', 17, 1, NULL, '11585020283', '0.00', '0.00', '0.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales_items`
--

CREATE TABLE `sales_items` (
  `item_id` int(11) NOT NULL DEFAULT '0',
  `invoice` varchar(255) DEFAULT NULL,
  `disc` decimal(15,2) NOT NULL,
  `quantity_purchased` decimal(15,1) NOT NULL DEFAULT '0.0',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_items`
--

INSERT INTO `sales_items` (`item_id`, `invoice`, `disc`, `quantity_purchased`, `amount`) VALUES
(8, '2018052123213452497', '0.00', '1.0', '500.00'),
(8, '2018052138635386707', '0.00', '1.0', '500.00'),
(124, '2018052138635386707', '0.00', '1.0', '27000.00'),
(2, '11585020283', '0.00', '1.0', '79000.00');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `stock_id` int(10) NOT NULL,
  `stock_date` varchar(255) NOT NULL,
  `location` int(11) NOT NULL,
  `transport_carriage` decimal(15,2) DEFAULT '0.00',
  `employee_id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`stock_id`, `stock_date`, `location`, `transport_carriage`, `employee_id`, `comment`) VALUES
(1526915437, '2018-05-21', 1, '0.00', 1, ''),
(1526915571, '2018-05-21', 1, '0.00', 1, 'all in Good condition'),
(1526966765, '2018-05-22', 1, '0.00', 1, ''),
(1527420120, '2018-05-27', 1, '0.00', 1, ''),
(1527432931, '2018-05-27', 2, '0.00', 1, ''),
(1527501838, '2018-05-28', 2, '0.00', 1, ''),
(1527514961, '2018-05-28', 2, '0.00', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `stock_items`
--

CREATE TABLE `stock_items` (
  `stock_id` int(10) NOT NULL,
  `item_id` varchar(255) NOT NULL,
  `quantity_in` int(11) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock_items`
--

INSERT INTO `stock_items` (`stock_id`, `item_id`, `quantity_in`, `cost_price`, `unit_price`, `location`) VALUES
(1526915437, '119', 1, '349000.00', '4000.00', 1),
(1526915571, '82', 1, '212500.00', '500.00', 1),
(1526915571, '14', 2, '176500.00', '2000.00', 1),
(1526915571, '1', 36, '75000.00', '2000.00', 1),
(1526915571, '2', 7, '75000.00', '2500.00', 1),
(1526915571, '55', 6, '75000.00', '4000.00', 1),
(1526915571, '3', 14, '45000.00', '6000.00', 1),
(1526915571, '16', 2, '137000.00', '17000.00', 1),
(1526915571, '6', 32, '62000.00', '200.00', 1),
(1526915571, '7', 13, '68000.00', '200.00', 1),
(1526915571, '61', 3, '85000.00', '140000.00', 1),
(1526915571, '19', 1, '55000.00', '90000.00', 1),
(1526966765, '89', 1, '117500.00', '6000.00', 1),
(1526966765, '143', 6, '118000.00', '200.00', 1),
(1526966765, '144', 4, '170000.00', '17000.00', 1),
(1527420120, '146', 8, '45000.00', '5000.00', 1),
(1527420120, '147', 26, '45000.00', '5000.00', 1),
(1527420120, '145', 70, '23500.00', '2000.00', 1),
(1527432931, '65', 1, '145000.00', '9000.00', 2),
(1527432931, '86', 2, '125000.00', '500.00', 2),
(1527432931, '37', 2, '155000.00', '1000.00', 2),
(1527432931, '91', 6, '111000.00', '1000.00', 2),
(1527432931, '1', 6, '75000.00', '2000.00', 2),
(1527432931, '2', 39, '75000.00', '2000.00', 2),
(1527432931, '53', 1, '10.00', '10.00', 2),
(1527432931, '29', 46, '10.00', '10.00', 2),
(1527432931, '31', 3, '1.00', '10.00', 2),
(1527432931, '9', 7, '1.00', '0.00', 2),
(1527432931, '24', 3, '1.00', '0.00', 2),
(1527432931, '96', 3, '10.00', '0.00', 2),
(1527432931, '44', 2, '10.00', '0.00', 2),
(1527501838, '21', 2, '0.00', '0.00', 2),
(1527501838, '98', 1, '0.00', '0.00', 2),
(1527501838, '63', 4, '0.00', '0.00', 2),
(1527501838, '100', 4, '0.00', '0.00', 2),
(1527501838, '70', 1, '0.00', '0.00', 2),
(1527501838, '92', 2, '0.00', '0.00', 2),
(1527501838, '11', 3, '0.00', '0.00', 2),
(1527501838, '12', 3, '0.00', '0.00', 2),
(1527501838, '44', 1, '0.00', '0.00', 2),
(1527501838, '94', 2, '0.00', '0.00', 2),
(1527501838, '48', 1, '0.00', '0.00', 2),
(1527501838, '89', 2, '0.00', '0.00', 2),
(1527501838, '13', 2, '0.00', '0.00', 2),
(1527501838, '149', 2, '0.00', '0.00', 2),
(1527501838, '52', 1, '0.00', '0.00', 2),
(1527501838, '50', 1, '0.00', '0.00', 2),
(1527501838, '67', 3, '0.00', '0.00', 2),
(1527514961, '155', 17, '0.00', '0.00', 2),
(1527514961, '156', 8, '0.00', '0.00', 2),
(1527514961, '157', 5, '0.00', '0.00', 2),
(1527514961, '158', 1, '0.00', '0.00', 2),
(1527514961, '14', 1, '0.00', '0.00', 2),
(1527514961, '153', 11, '0.00', '0.00', 2),
(1527514961, '154', 21, '0.00', '0.00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `stock_out`
--

CREATE TABLE `stock_out` (
  `stock_id` varchar(25) NOT NULL,
  `stock_date` varchar(255) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_out_items`
--

CREATE TABLE `stock_out_items` (
  `id` int(11) NOT NULL,
  `stock_id` varchar(25) NOT NULL,
  `item_id` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_store`
--

CREATE TABLE `stock_store` (
  `item_id` int(11) NOT NULL,
  `quantity_in` int(11) NOT NULL DEFAULT '0',
  `quantity_out` int(11) NOT NULL DEFAULT '0',
  `location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_store`
--

INSERT INTO `stock_store` (`item_id`, `quantity_in`, `quantity_out`, `location`) VALUES
(1, 36, 0, 1),
(1, 6, 0, 2),
(2, 7, 0, 1),
(2, 39, 0, 2),
(3, 14, 0, 1),
(6, 32, 0, 1),
(7, 13, 0, 1),
(9, 7, 0, 2),
(11, 3, 0, 2),
(12, 3, 0, 2),
(13, 2, 0, 2),
(14, 2, 0, 1),
(14, 1, 0, 2),
(16, 2, 0, 1),
(19, 1, 0, 1),
(21, 2, 0, 2),
(24, 3, 0, 2),
(29, 46, 0, 2),
(31, 3, 0, 2),
(37, 2, 0, 2),
(44, 3, 0, 2),
(48, 1, 0, 2),
(50, 1, 0, 2),
(52, 1, 0, 2),
(53, 1, 0, 2),
(55, 6, 0, 1),
(61, 3, 0, 1),
(63, 4, 0, 2),
(65, 1, 0, 2),
(67, 3, 0, 2),
(70, 1, 0, 2),
(82, 1, 0, 1),
(86, 2, 0, 2),
(89, 1, 0, 1),
(89, 2, 0, 2),
(91, 6, 0, 2),
(92, 2, 0, 2),
(94, 2, 0, 2),
(96, 3, 0, 2),
(98, 1, 0, 2),
(100, 4, 0, 2),
(119, 1, 0, 1),
(143, 6, 0, 1),
(144, 4, 0, 1),
(145, 70, 0, 1),
(146, 8, 0, 1),
(147, 26, 0, 1),
(149, 2, 0, 2),
(153, 11, 0, 2),
(154, 21, 0, 2),
(155, 17, 0, 2),
(156, 8, 0, 2),
(157, 5, 0, 2),
(158, 1, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `deleted`) VALUES
(1, 'KABWE', 0),
(2, 'MWONGOZO SACCOS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `person_id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`person_id`, `company_name`, `delete`) VALUES
(6, 'KINGS STATIONERIES', 0),
(7, 'ANHANSA STORE', 0),
(8, 'KOMANYA SHOP', 0),
(9, 'TANZANIA PRINTING SERVICES ', 0),
(10, 'FREEDOM COMMUNICATIONS', 0),
(11, 'FURAHA BOOKSHOP', 0),
(13, 'MKONGOMA COMPANY', 0),
(14, 'BALAJI THERMALWARE', 0),
(15, 'LISSA PRODUCTS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `description`) VALUES
(1, 'ctn', 'Carton'),
(2, 'pcs', 'Piece'),
(4, 'dzn', 'TWELVE PIECES'),
(5, 'outer', 'UNITS ENCLOSED IN A PACKAGE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD KEY `username` (`username`),
  ADD KEY `password` (`password`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_name` (`item_name`,`description`),
  ADD KEY `item_number` (`barcode`),
  ADD KEY `item_name_2` (`item_name`);

--
-- Indexes for table `item_units`
--
ALTER TABLE `item_units`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_id` (`item_id`,`unit_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `proforma_invoice`
--
ALTER TABLE `proforma_invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`);

--
-- Indexes for table `proforma_items`
--
ALTER TABLE `proforma_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_time` (`sale_time`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `invoice_number` (`invoice_number`);

--
-- Indexes for table `sales_items`
--
ALTER TABLE `sales_items`
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `stock_out`
--
ALTER TABLE `stock_out`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `stock_out_items`
--
ALTER TABLE `stock_out_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_store`
--
ALTER TABLE `stock_store`
  ADD UNIQUE KEY `unique_index` (`item_id`,`location`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `store_name` (`name`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `proforma_invoice`
--
ALTER TABLE `proforma_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `proforma_items`
--
ALTER TABLE `proforma_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `stock_out_items`
--
ALTER TABLE `stock_out_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales_items`
--
ALTER TABLE `sales_items`
  ADD CONSTRAINT `sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD CONSTRAINT `suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
