    $(".add-emp-btn").click(function () {
        // get needed html
        $.get("<?= base_url() ?>/admin/employees/form", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_tip').modal(/* options object here*/);

        });
    });
    
    function addRow(tableID) {
        var table = document.getElementById(tableID);

        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);

        var item_id = row.insertCell(0);
        var element1 = document.createElement("td");
        item_id.appendChild(element1);
        item_id.setAttribute("name", "item_id");
        item_id.setAttribute("id", "item_id");
        item_id.innerHTML = response.item_info.id;

        var item = row.insertCell(1);
        var element2 = document.createElement("td");
        item.appendChild(element2);
        item.innerHTML = response.item_info.item_name + ", " + response.item_info.description;

        var cell3 = row.insertCell(2);
        var element3 = document.createElement("select");
        element3.setAttribute("id", "price");
        element3.setAttribute("class", "price form-control");
        element3.setAttribute("name", "price");
        cell3.appendChild(element3);

        var option1 = document.createElement("option");
        option1.value = response.item_info.unit_selling_price;
        option1.text = "Piece (" + response.item_info.unit_selling_price + ")";
        element3.appendChild(option1);

        var option2= document.createElement("option");
        option2.value = response.item_info.ctn_selling_price;
        option2.text = "Carton (" + response.item_info.ctn_selling_price + ")";
        element3.appendChild(option2);

        var option3 = document.createElement("option");
        option3.value = response.item_info.dzn_selling_price;
        option3.text = "Dozen (" + response.item_info.dzn_selling_price + ")";
        element3.appendChild(option3);

        var option4 = document.createElement("option");
        option4.value = response.item_info.outer_selling_price;
        option4.text = "Outer (" + response.item_info.outer_selling_price + ")";
        element3.appendChild(option4);

        var cell5 = row.insertCell(3);
        var element5 = document.createElement("input");
        element5.type = "text";
        element5.setAttribute("id", "qty");
        element5.setAttribute("name", "qty");
        element5.setAttribute("class", "qty");
        element5.value = "1";
        cell5.appendChild(element5);

        var cell6 = row.insertCell(4);
        var element6 = document.createElement("input");
        element6.type = "text";
        element6.setAttribute("id", "disc");
        element6.setAttribute("name", "disc");
        element6.setAttribute("class", "disc");
        element6.value = "0";
        cell6.appendChild(element6);

        var cell7 = row.insertCell(5);
        var element7 = document.createElement("input");
        element7.type = "text";
        element7.setAttribute("id", "amount");
        element7.setAttribute("name", "amount");
        element7.setAttribute("class", "amount");
        element7.value = response.item_info.unit_price;
        cell7.appendChild(element7);

        var cell8 = row.insertCell(6);
        var element8 = document.createElement("button");
        element8.type = "button";
        element8.setAttribute("id", "deleteRow");
        element8.setAttribute("name", "deleteRow");
        element8.setAttribute("class", "btn btn-xs btn-danger deleteRow");
        element8.innerHTML = "<i class=\"fa fa-sm fa-close\"></i> ";
        cell8.appendChild(element8);
    }

    function calculator($tblrow){
        var qty = parseInt($tblrow.find("[name=qty]").val(),10);
        var price = parseFloat($tblrow.find("[name=price]").val());
        var disc = parseFloat($tblrow.find("[name=disc]").val()/100);
        var amount = price * qty;
        var discount = 0;
       
         if (!isNaN(amount)) {
            $tblrow.find('.amount').val(amount.toFixed(2));
            var subTotal = 0;
            var total = 0;

            $(".disc").each(function () {
                var discval = parseFloat($(this).val());
                discount += isNaN(discval) ? 0 : discval;
            });
            $('.discount').val(discount.toFixed(2) + " %");

            $(".amount").each(function () {
                var stval = parseFloat($(this).val());
                subTotal += isNaN(stval) ? 0 : stval;
            });

            $('.subtotal').val(subTotal.toFixed(2));

            $(".amount").each(function () {
                var pay = parseFloat($(this).val());
                total += isNaN(pay) ? 0 : pay;
            });
            
            total_amount = parseFloat(total + (total * discount/100));

            $('.total').val(total_amount.toFixed(2));

            if($('input[type=checkbox]').prop('checked')){
                calculateVAT();
            } else {
                $('.vat').val(parseFloat(0))
            }
        }
    }


    function sales_calculations(){
        $("#register").find("tbody tr").each(function (index) {
            var $tblrow = $(this); 
        
            calculator($tblrow);

            $tblrow.find('.qty').on('keyup', function () {
                calculator($tblrow);
            });

            $tblrow.find('.disc').on('keyup', function () {
                calculator($tblrow);
            });

            $tblrow.find('.price').on('change', function () {
                calculator($tblrow);
            });
        });
    }
	 
    function deleteRow(tableID) {
        try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;

            for(var i=0; i<rowCount; i++) {
                var row = table.rows[i];
                table.deleteRow(i);
                rowCount--;
                i--;
            }
            alert(rowCount);
        }catch(e) {
            alert(e);
        }
    }

    // function generateInvoiceNum(){
    //     var d = new Date();
    //     function f(n) { return n < 10 ? '0' + n : n; }
    //     var random_num = Math.floor(Math.random() * (99999999999 -  10000000000)) + 10000000000;
    //     invoice_num = d.getFullYear() + f(d.getMonth()+1) + f(d.getDate()) + random_num;
    //     return invoice_num;
    // }

    function calculateVAT(){
        total = $('.total').val();
        VAT = parseFloat(total) * 0.18
            
        total_incl_vat = parseFloat(total) + parseFloat(VAT)
    
        $('.total_incl_vat').val(total_incl_vat.toFixed(2));

        $('.vat').val(VAT.toFixed(2));
    }
     
   