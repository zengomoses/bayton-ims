<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 10:33 AM
 */

    $this->load->view($this->config->item('byton_ims_template_dir_admin') . 'header');
    $this->load->view($this->config->item('byton_ims_template_dir_admin') . 'content');
    $this->load->view($this->config->item('byton_ims_template_dir_admin') . 'footer');
