<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BYTON | <?= $title?>  </title>

    <!-- Bootstrap -->
    <link href="<?= base_url() ?>assets/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<?= base_url() ?>assets/js/jquery/dist/jquery.min.js"></script>
    <link href="<?= base_url() ?>assets/css/bootstrapValidator.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url() ?>assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url() ?>assets/css/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?= base_url() ?>assets/css/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= base_url() ?>assets/css/build/css/custom.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet">
    <script src="<?= base_url() ?>assets/bootstrap-sweetalert/dist/sweetalert.js"></script>
    <style>
        .login{
            background: rgba(152, 184, 255, 0.61);
        }
    </style>
</head>
<body class="login">
<div class="row">
    <div class="navbar navbar-default top_nav">
    </div>
</div>
