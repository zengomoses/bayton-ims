<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 10:33 AM
 */
if (isset($page)) {
    if (isset($module)) {
        $this->load->view("$module/$page");
    } else {
        $this->load->view($page);
    }
}