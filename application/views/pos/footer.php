
</div>
</div>
<!-- Bootstrap -->
<script src="<?= base_url() ?>assets/js/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/js/dataTables.bootstrap.min.js"></script>

<script>
    $('#myData').dataTable();
</script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/js/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?= base_url() ?>assets/js/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?= base_url() ?>assets/js/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?= base_url() ?>assets/js/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?= base_url() ?>assets/js/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?= base_url() ?>assets/js/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="<?= base_url() ?>assets/js/skycons/skycons.js"></script>
<!-- Flot -->
<script src="<?= base_url() ?>assets/js/Flot/jquery.flot.js"></script>
<script src="<?= base_url() ?>assets/js/Flot/jquery.flot.pie.js"></script>
<script src="<?= base_url() ?>assets/js/Flot/jquery.flot.time.js"></script>
<script src="<?= base_url() ?>assets/js/Flot/jquery.flot.stack.js"></script>
<script src="<?= base_url() ?>assets/js/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?= base_url() ?>assets/js/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?= base_url() ?>assets/js/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="<?= base_url() ?>assets/js/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?= base_url() ?>assets/js/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="<?= base_url() ?>assets/js/jqvmap/dist/jquery.vmap.js"></script>
<script src="<?= base_url() ?>assets/js/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?= base_url() ?>assets/js/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?= base_url() ?>assets/js/moment/min/moment.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?= base_url() ?>assets/js/script.js"></script>
<script src="<?= base_url() ?>assets/js/custom.js"></script>

<script src="<?= base_url() ?>assets/js/bootstrap-select.min.js"></script>
</body>
</html>
