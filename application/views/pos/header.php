<?php
    if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
        $name = ($this->session->userdata['logged_in']['name']);
        $gender = ($this->session->userdata['logged_in']['gender']);
        $username = ($this->session->userdata['logged_in']['username']);
        $user_id = ($this->session->userdata['logged_in']['id']);
        $user_level = ($this->session->userdata['logged_in']['level']);
    }else {
        header("location:". base_url('auth')."");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BYTON | <?= $title?> </title>
    <script src="<?= base_url() ?>assets/js/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="<?= base_url() ?>assets/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/dataTables.min.css" rel="stylesheet">

    <link href="<?= base_url() ?>assets/css/bootstrapValidator.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/bootstrap-select.min.css" rel="stylesheet">

    <link href="<?= base_url() ?>assets/css/bootstrapValidator.min.css" rel="stylesheet">
    <script src="<?= base_url() ?>assets/js/bootstrapValidator.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?= base_url() ?>assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url() ?>assets/css/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= base_url() ?>assets/css/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="<?= base_url() ?>assets/css/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?= base_url() ?>assets/css/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?= base_url() ?>assets/css/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/pos.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet">
    <script src="<?= base_url() ?>assets/bootstrap-sweetalert/dist/sweetalert.js"></script>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0; background-color: white; align-items: center; ">
                    <a href="" class="" style="border: 0; background-color: white; margin-left: 50px">
                        <img src="<?= base_url() ?>assets/images/byton_logo.png" alt="..." class="" width="120" height="50">
                    </a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <?php if($gender == 0):?>
                            <img src="<?= base_url() ?>assets/images/female.png" alt="..." class="img-circle profile_img">                        <?php endif;?>
                        <?php if($gender == 1):?>
                            <img src="<?= base_url() ?>assets/images/male.png" alt="..." class="img-circle profile_img">
                        <?php endif;?>
                    </div>
                    <div class="profile_info" style="text-align: center">
                        <span style="font-size: large">Welcome,</span>
                        <h2 style="font-size: larger"><?= $name?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a href="<?= base_url('pos') ?>"><span><i class="fa fa-shopping-bag"></i>Sales Register</span></a></li>
                            <li><a href="<?= base_url('pos/customers') ?>"><span><i class="fa fa-users"></i>Customers</span></a></li>
                            <?php if ($user_level == "chief"):?>
                                <li><a><span><i class="fa fa-hand-lizard-o"></i>Order Process <span class="fa fa-chevron-right"></span></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?= base_url('pos/orders') ?>"><span>My Orders</span></a></li>
                                        <li><a href="<?= base_url('pos/orders_chief') ?>"><span>Manage Orders</span></a></li>

                                    </ul>
                                </li>
                                <li><a><span><i class="fa fa-briefcase"></i>Inventory <span class="fa fa-chevron-right"></span></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?= base_url('pos/stores/stores') ?>">Stores</a></li>
                                        <li><a href="<?= base_url('pos/stocks')  ?>">Stock</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?= base_url('pos/pos_sms') ?>"><span><i class="fa fa-envelope"></i>Messaging</span></a></li>
                            <?php else:?>
                                <li><a href="<?= base_url('pos/orders') ?>"><span><i class="fa fa-balance-scale"></i>Orders</span></a></li>
                            <?php endif;?>
                            <li><a href="<?= base_url('/pos/invoices') ?>"><span><i class="fa fa-balance-scale"></i>Proformer Invoice</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?php if($gender == 0):?>
                                    <img src="<?= base_url() ?>assets/images/female.png" alt=""><?= $name ?>
                                <?php endif;?>
                                <?php if($gender == 1):?>
                                    <img src="<?= base_url() ?>assets/images/male.png" alt=""><?= $name ?>
                                <?php endif;?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="<?= base_url('auth/logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>

                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
