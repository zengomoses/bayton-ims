<?php

class Migration_people_key_constraints extends CI_Migration {

    public function up() {
        $this->db->query('ALTER TABLE employees ADD FOREIGN KEY(person_id) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->db->query('ALTER TABLE customers ADD FOREIGN KEY(person_id) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->db->query('ALTER TABLE suppliers ADD FOREIGN KEY(person_id) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    public function down() {
        $this->dbforge->drop_table('people_key_constraints');
    }

}