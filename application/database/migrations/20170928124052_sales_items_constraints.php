<?php

class Migration_sales_items_constraints extends CI_Migration {

    public function up() {
        $this->db->query('ALTER TABLE sales_items ADD FOREIGN KEY(item_id) REFERENCES items(id) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    public function down() {
        $this->dbforge->drop_table('sales_items_constraints');
    }
}