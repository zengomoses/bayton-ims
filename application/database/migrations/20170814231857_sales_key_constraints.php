<?php

class Migration_sales_key_constraints extends CI_Migration {

    public function up() {
        $this->db->query('ALTER TABLE sales ADD FOREIGN KEY(customer_id) REFERENCES customers(person_id) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->db->query('ALTER TABLE sales ADD FOREIGN KEY(employee_id) REFERENCES employees(person_id) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    public function down() {
        $this->dbforge->drop_table('sales_key_constraints');
    }

}