<?php

class Migration_sales extends CI_Migration {

    public function up() {
        $this->dbforge->drop_table('sales', TRUE);
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'sale_time' => array(
                'type' => 'TIMESTAMP',
            ),
            'customer_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'employee_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'comment' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'invoice_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 32,
            ),
            'discount' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,2',
            ),


            'sale_status' => array(
                'type' => 'TINYINT',
                'constraint' => 2,
                'default' => 0,
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('sale_time');
        $this->dbforge->add_key('customer_id');
        $this->dbforge->add_key('employee_id');
        $this->dbforge->add_key('invoice_number');
        $this->dbforge->create_table('sales');
    }

    public function down() {
        $this->dbforge->drop_table('sales');
    }

}