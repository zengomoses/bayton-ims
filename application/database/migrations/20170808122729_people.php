<?php

class Migration_people extends CI_Migration {

    public function up() {
        $this->dbforge->drop_table('person',TRUE);
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'gender' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'phone_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' =>TRUE,
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' =>TRUE,
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'city' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'state' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'comments' => array(
                'type' => 'TEXT',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('person',TRUE);

        $this->dbforge->drop_table('employees',TRUE);
        $this->dbforge->add_field(array(
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'person_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'level' => array(
              'type' => 'VARCHAR',
              'constraint' => '25'
            ),
            'status' => array(
              'type' => 'INT',
              'constraint' => '1',
                'default' => 0
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0',
            ),
        ));
        $this->dbforge->add_key('username');
        $this->dbforge->add_key('password');
        $this->dbforge->create_table('employees',TRUE);

        $this->dbforge->drop_table('customers', TRUE);
        $this->dbforge->add_field(array(
           'person_id' => array(
             'type' => 'INT',
             'constraint' => 11,
           ),
            'company_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'discount_percent' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,2',
                'default' => '0.00'
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0',
            ),
        ));

        $this->dbforge->add_key('person_id');
        $this->dbforge->create_table('customers',TRUE);

        $this->dbforge->drop_table('suppliers', TRUE);
        $this->dbforge->add_field(array(
            'person_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'company_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'delete' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0',
            ),
        ));
        $this->dbforge->add_key('person_id');
        $this->dbforge->create_table('suppliers',TRUE);
    }

    public function down() {
        $this->dbforge->drop_table('person');
        $this->dbforge->drop_table('employees');
        $this->dbforge->drop_table('customers');
        $this->dbforge->drop_table('suppliers');
    }
}