<?php

class Migration_items extends CI_Migration {

    public function up() {
        $this->dbforge->drop_table('items',TRUE);
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'item_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
             ),
            'category_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 1,
             ),
            'supplier_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'item_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
//            'cost_price' => array(
//                'type' => 'DECIMAL',
//                'constraint' => '15,2',
//            ),
            'selling_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,2',
                'default' => '0',
            ),
            'quantity' => array(
                'type' => 'INT',
                'constraint' => 100,
                'default' => '0',
            ),
            'quantity_sold' => array(
                'type' => 'INT',
                'constraint' => 100,
                'default' => '0',
            ),
            'delete' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0',
            ),

        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('item_number');
        $this->dbforge->create_table('items');
    }

    public function down() {
        $this->dbforge->drop_table('items');
    }

}