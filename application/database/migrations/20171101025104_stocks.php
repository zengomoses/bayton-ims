<?php

class Migration_stocks extends CI_Migration {

    public function up() {
        $this->dbforge->drop_table('stocks',TRUE);
        $this->dbforge->add_field(array(
            'stock_id' => array(
                'type' => 'INT',
                'constraint' => 10,
            ),
            'stock_date' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'employee_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'comment' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
        ));
        $this->dbforge->add_key('stock_id', TRUE);
        $this->dbforge->create_table('stocks');

        $this->dbforge->drop_table('stock_items',TRUE);
        $this->dbforge->add_field(array(
            'stock_id' => array(
                'type' => 'INT',
                'constraint' => 10,
            ),
            'item_id' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'quantity_in' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'cost_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,2',
            ),
            'unit_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,2',
            ),
        ));
        $this->dbforge->create_table('stock_items');

    }

    public function down() {
        $this->dbforge->drop_table('stocks');
        $this->dbforge->drop_table('stock_items');
    }

}