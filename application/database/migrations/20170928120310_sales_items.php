<?php

class Migration_sales_items extends CI_Migration {

    public function up() {
        $this->dbforge->drop_table('sales_items',TRUE);
        $this->dbforge->add_field(array(

            'item_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => '0'
            ),
            'invoice' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => NULL
            ),
            'disc' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,2',
            ),
            'quantity_purchased' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,1',
                'default' => '0',
            ),
        ));
        $this->dbforge->create_table('sales_items');
    }

    public function down() {
        $this->dbforge->drop_table('sales_items');
    }

}