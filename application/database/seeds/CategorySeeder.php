<?php

class CategorySeeder extends Seeder {

    private $table = 'categories';

    public function run() {
        $this->db->truncate($this->table);
        $data = array(
            'name' => 'Unspecified',
            'deleted' => '0',
        );

        $this->db->insert($this->table, $data);

        echo PHP_EOL;
    }
}
