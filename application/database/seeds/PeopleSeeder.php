<?php

class PeopleSeeder extends Seeder {

    private $table = 'person';

    public function run() {
       // $this->db->truncate($this->table);
        echo "Seeding 1: Admin ....\n";
        $data = array(
            'first_name' => 'Byton',
            'last_name' => 'Admin',
            'gender' => 'Male',
            'phone_number' => '1111111111',
            'email' => 'admin@byton.com',
            'address' => 'P.O Box 000',
            'city' => 'Mbeya',
            'state' => 'Mbeya',
            'comments' => '',
        );
        $this->db->insert($this->table, $data);

        echo "Seeding 2: Sales Personnel ....\n";
        $data = array(
            'first_name' => 'Sales',
            'last_name' => 'Officer',
            'gender' => 'Male',
            'phone_number' => '2222222222',
            'email' => 'salesofficer@byton.com',
            'address' => 'P.O Box 000',
            'city' => 'Mbeya',
            'state' => 'Mbeya',
            'comments' => '',
        );
        echo "Seeding 3: Unspecified Customer ....\n";
        $data = array(
            'first_name' => 'Unspecified',
            'last_name' => 'Customer',
            'gender' => 'Male',
            'phone_number' => '3333333333',
            'email' => 'customer@byton.com',
            'address' => 'P.O Box 000',
            'city' => 'Mbeya',
            'state' => 'Mbeya',
            'comments' => '',
        );
        $this->db->insert($this->table, $data);

        echo PHP_EOL;
    }
}
