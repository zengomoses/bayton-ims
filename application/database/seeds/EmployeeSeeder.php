<?php

class EmployeeSeeder extends Seeder {

    private $table = 'employees';

    public function run() {
       // $this->db->truncate($this->table);
        echo "Seeding 1: Admin Account .......|\n";
        echo "**************************************\n";
        echo "* Username: admin                    *\n";
        echo "* Password: admin1234                *\n";
        echo "**************************************\n";

        $data = array(
            'username' => 'admin',
            'password' =>'c93ccd78b2076528346216b3b2f701e6',
            'person_id' => 1,
            'level' => 'admin',
            'status' => '1'
        );

        $this->db->insert($this->table, $data);

        echo "Seeding 2: Sales Personnel Account .......\n";
        echo "**************************************\n";
        echo "* Username: sales                    *\n";
        echo "* Password: admin1234                *\n";
        echo "**************************************\n";
        $data = array(
            'username' => 'sales',
            'password' =>'c93ccd78b2076528346216b3b2f701e6',
            'person_id' => 2,
            'level' => 'sales',
            'status' => '1'
        );

        $this->db->insert($this->table, $data);

        echo PHP_EOL;
    }
}
