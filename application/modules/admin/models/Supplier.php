<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Supplier extends Person
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
     Determines if a given person_id is a supplier
     */
    function exists($person_id)
    {
        $this->db->from('suppliers');
        $this->db->join('person', 'person.id = suppliers.person_id');
        $this->db->where('suppliers.person_id', $person_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    /*
    Returns all the suppliers
    */
    function get_all()
    {
        $this->db->from('suppliers');
        $this->db->join('person', 'suppliers.person_id=person.id');
        $this->db->order_by("last_name", "asc");
        return $this->db->get()->result();
    }

    /*
   Gets information about a particular supplier
   */
    public function get_info($supplier_id)
    {
        $this->db->from('suppliers');
        $this->db->join('person', 'person.id = suppliers.person_id');
        $this->db->where('suppliers.person_id', $supplier_id);
        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $supplier_id is NOT an supplier
            $person_obj = parent::get_info(-1);

            //Get all the fields from supplier table
            //append those fields to base parent object, we we have a complete empty object
            foreach($this->db->list_fields('suppliers') as $field)
            {
                $person_obj->$field = '';
            }

            return $person_obj;
        }
    }

    /*
	Inserts supplier
	*/
    public function save_supplier(&$person_data, &$supplier_data, $supplier_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(parent::save($person_data, $supplier_id))
        {
            if(!$supplier_id || !$this->exists($supplier_id))
            {
                $supplier_data['person_id'] = $supplier_id = $person_data['person_id'];
                $success = $this->db->insert('suppliers', $supplier_data);
            }
        }

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    public function update_supplier($person_data, $supplier_data, $supplier_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
            $this->db->where('person_id', $supplier_id);
            $success = $this->db->update('suppliers', $supplier_data);

            $this->db->where('id', $supplier_id);
            $success &= $this->db->update('person', $person_data);
        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }
}
