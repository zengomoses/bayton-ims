<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Sale extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
	Determines if a given sale_id is an sale
	*/
    public function exists($sale_id, $ignore_deleted = FALSE, $deleted = FALSE)
    {
        if (ctype_digit($sale_id))
        {
            $this->db->from('sales');
            $this->db->where('id', (int) $sale_id);
            if ($ignore_deleted == FALSE)
            {
                $this->db->where('deleted', $deleted);
            }

            return ($this->db->get()->num_rows() == 1);
        }

        return FALSE;
    }

    /*
	Determines if a given sale_number exists
	*/
    public function sale_number_exists($sale_number, $sale_id = '')
    {
        $this->db->from('sales');
        $this->db->where('sale_number', (string) $sale_number);
        if(ctype_digit($sale_id))
        {
            $this->db->where('id !=', (int) $sale_id);
        }

        return ($this->db->get()->num_rows() == 1);
    }

    /*

    /*
    Returns all the sales
    */
    public function get_all()
    {
        $this->db->from('sales');
        //$this->db->order_by("sale_id", "asc");
        return $this->db->get()->result();
    }

    /*
     Inserts or updates a category
     */
    public function save_sale(&$sale_data, $sale_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(!$sale_id || !$this->exists($sale_id))
        {
            $success = $this->db->insert('sales', $sale_data);
        }
        else
        {
            $this->db->where('sale_id', $sale_id);
            $success = $this->db->update('sales', $sale_data);
        }

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    /*
   Gets information about a particular sale

   */
    function get_sales_items($sales_invoice)
    {
        $this->db->select('c.invoice, c.amount, c.disc,c.quantity_purchased,it.item_name,it.description,it.brand, it.unit_selling_price', FALSE);
        $this->db->from('sales_items c');
        $this->db->join('items it', 'it.id = c.item_id', 'left');
        $this->db->where('c.invoice', $sales_invoice);

        return $this->db->get()->result();
    }
    public function get_info($sale_invoice)
    {

        $this->db->from('sales');
        $this->db->where('invoice_number', $sale_invoice);
        return $this->db->get()->row();
    }
    /*
        Gets today's total of sales
    */
    public function get_total_rows()
    {
        $this->db->from('sales');
        $this->db->like('sale_time', date('Y-m-d'));

        return $this->db->count_all_results();
    }

    public function total_income_today()
    {

        $this->db->select('sum(amount_paid) as income', FALSE);
        $this->db->from('sales');
        $this->db->like('sale_time', date('Y-m-d'));

        return $this->db->get()->result();

    }

    public function mostly_sold_item_today()
    {
        $this->db->select('c.item_id,g.name, sum(c.quantity_purchased) as total', FALSE);
        $this->db->from('sales_items c');
        $this->db->join('sales s', 'c.invoice = s.invoice_number');
        $this->db->join('items t', 'c.item_id = t.id');
        $this->db->join('categories g', 't.category_id = g.id', 'left');
        $this->db->like('sale_time', date('Y-m-d'));
        $this->db->limit(1);
        $this->db->group_by('c.item_id');
        $this->db->order_by('total', 'DESC');
        return $this->db->get()->result();
    }

    public function update_amount($invoice, $amount){

        $query = "UPDATE sales SET amount_paid=amount_paid + ".$amount.",amount_due=amount_due - ".$amount." WHERE invoice_number=".$invoice;
        $result = $this->db->query($query);
        $query2 = "UPDATE sales SET sale_status = IF((SELECT amount_due FROM (SELECT * FROM sales WHERE invoice_number=".$invoice.") AS t2 WHERE invoice_number=".$invoice.")<>0,1,0) WHERE invoice_number=".$invoice;
        $result2 =$this->db->query($query2);
        if($result && $result2){
            return true;
        }else{
            return false;
        }
    }

}
