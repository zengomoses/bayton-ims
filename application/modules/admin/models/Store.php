<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Store extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
	Determines if a given store_id is exists
	*/
    public function exists($store_id, $ignore_deleted = FALSE, $deleted = FALSE)
    {
        if (ctype_digit($store_id))
        {
            $this->db->from('stores');
            $this->db->where('id', (int) $store_id);
            if ($ignore_deleted == FALSE)
            {
                $this->db->where('deleted', $deleted);
            }

            return ($this->db->get()->num_rows() == 1);
        }

        return FALSE;
    }

    /*
	Gets total of rows
	*/
    public function get_total_rows()
    {
        $this->db->from('stores');
        $this->db->where('deleted', 0);

        return $this->db->count_all_results();
    }

    /*
    Returns all the stores
    */
    function get_all()
    {
        $this->db->from('stores');
        $this->db->order_by("name", "asc");
        return $this->db->get()->result();
    }

    /*
	Get number of rows
	*/
    public function get_found_rows($search, $filters)
    {
        return $this->search($search, $filters)->num_rows();
    }


    function count_all()
    {
        $this->db->from('stores');
        $this->db->where('deleted',0);
        return $this->db->count_all_results();
    }

    /*
     Inserts or updates a category
     */
    public function save_store(&$store_data, $store_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(!$store_id || !$this->exists($store_id))
        {
            $success = $this->db->insert('stores', $store_data);
        }
        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    public function update_store($store_data, $store_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
        $this->db->where('id', $store_id);
        $success = $this->db->update('stores', $store_data);

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }
    /*
   Gets information about a particular store
   */
    function get_info($store_id)
    {
        $this->db->select('*', FALSE);
        $this->db->from('stores');
        $this->db->where('id', $store_id );

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $store_id is NOT an store
            $store_obj=new stdClass();

            //Get all the fields from stores table
            $fields = $this->db->list_fields('stores');

            foreach ($fields as $field)
            {
                $store_obj->$field='';
            }

            return $store_obj;
        }
    }
    function get_items_store($store_id)
    {
        $this->db->select('i.id as item_id,i.brand, i.item_name, i.description as desc, s.quantity_in,l.name, l.id');
        $this->db->from('items i');
        $this->db->join('stock_store s','i.id = s.item_id');
        $this->db->join('stores l','s.location = l.id');
        $this->db->where('s.location',$store_id);

        return $this->db->get()->result();
    }


    function get_item_quantity()
    {
        $this->db->select('i.id as item_id,i.brand, i.item_name, i.description,i.min_quantity, SUM(s.quantity_in) AS quantity_in');
        $this->db->from('items i');
        $this->db->join('stock_store s','i.id = s.item_id');
        $this->db->group_by('i.id');

        return $this->db->get()->result();
    }
}
