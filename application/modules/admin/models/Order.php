<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Order extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
	Determines if a given order_id is an order
	*/
    public function exists($order_id, $ignore_deleted = FALSE, $deleted = FALSE)
    {
        if (ctype_digit($order_id))
        {
            $this->db->from('orders');
            $this->db->where('order_id', (int) $order_id);
            if ($ignore_deleted == FALSE)
            {
                $this->db->where('deleted', $deleted);
            }

            return ($this->db->get()->num_rows() == 1);
        }

        return FALSE;
    }

    /*
	Determines if a given order_number exists
	*/
    public function order_number_exists($order_number, $order_id = '')
    {
        $this->db->from('orders');
        $this->db->where('order_number', (string) $order_number);
        if(ctype_digit($order_id))
        {
            $this->db->where('order_id !=', $order_id);
        }

        return ($this->db->get()->num_rows() == 1);
    }

    /*

    /*
    Returns all the orders
    */
    public function get_all()
    {
        $this->db->from('orders');
        //$this->db->order_by("order_id", "asc");
        return $this->db->get()->result();
    }

    /*
     Inserts or updates a category
     */
    public function save_order(&$order_data, $order_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(!$order_id || !$this->exists($order_id))
        {
            $success = $this->db->insert('orders', $order_data);
        }
        else
        {
            $this->db->where('order_id', $order_id);
            $success = $this->db->update('orders', $order_data);
        }

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    /*
   Gets information about a particular order

   */
    function get_order_items($order_id)
    {
        $this->db->from('order_items');
        $this->db->where('order_id', $order_id);
        return $this->db->get()->result();
    }

    public function get_info($order_id)
    {
        $this->db->from('orders');
        $this->db->where('ref_number', $order_id);
        return $this->db->get()->row();
    }

    public function approve_order($order_id, $order_info){

        $this->db->trans_start();
        $query = 'UPDATE orders SET status="'.$order_info["status"].'",
            approved_by="'.$order_info["approved_by"].'",
            approval_date="'.$order_info["approval_date"].'" WHERE id="'.$order_id.'"';
        $this->db->query($query);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function reject_order($order_id, $order_info){

        $this->db->trans_start();
        $query = 'UPDATE orders SET status="'.$order_info["status"].'"  WHERE id="'.$order_id.'"';
        $this->db->query($query);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function process_order($order_id, $stock_info, $order_items, $order_info){

        $this->db->trans_start();
            $query = 'UPDATE orders SET status="'.$order_info["status"].'",
            processed_by="'.$order_info["processed_by"].'",
            processed_date="'.$order_info["processed_date"].'", ref_number="'.$order_info["ref_number"].'" WHERE id="'.$order_id.'"';
            $this->db->query($query);

            foreach($order_items as $item):
                $query_1 = "UPDATE stock_store SET quantity_in = quantity_in - ".$item['quantity'].", quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id']." AND location=".$item['store'];
                //$query_2 = "UPDATE stock_store SET quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id']." AND location=".$item['store'];
                $query_2 = "UPDATE items SET quantity_in = quantity_in - ".$item['quantity'].", quantity_out = quantity_out + ".$item['quantity']." WHERE id=".$item['item_id'];
                //$query_4 = "UPDATE items SET quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id'];
                if($this->db->query($query_1) && $this->db->query($query_2)){
                    $this->db->insert('stock_out_items', $item);
                }
            endforeach;

            $this->db->insert('stock_out', $stock_info);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function revoke_order($order_id, $stock_info, $order_items, $order_info){

        $this->db->trans_start();
        $query = 'UPDATE orders SET status="'.$order_info["status"].'",
            ref_number="'.$order_info["ref_number"].'" WHERE id="'.$order_id.'"';
        $this->db->query($query);

        foreach($order_items as $item):
            $query_1 = "UPDATE stock_store SET quantity_in = quantity_in + ".$item['quantity'].", quantity_out = quantity_out - ".$item['quantity']." WHERE item_id=".$item['item_id']." AND location=".$item['store'];
            //$query_2 = "UPDATE stock_store SET quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id']." AND location=".$item['store'];
            $query_2 = "UPDATE items SET quantity_in = quantity_in + ".$item['quantity'].", quantity_out = quantity_out - ".$item['quantity']." WHERE id=".$item['item_id'];
            //$query_4 = "UPDATE items SET quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id'];
            if($this->db->query($query_1) && $this->db->query($query_2)){
                $this->db->insert('stock_out_items', $item);
            }
        endforeach;

        $this->db->insert('stock_out', $stock_info);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
