<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Unit extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
	Determines if a given unit_id is exists
	*/
    public function exists($unit_id, $ignore_deleted = FALSE, $deleted = FALSE)
    {
        if (ctype_digit($unit_id))
        {
            $this->db->from('units');
            $this->db->where('id', (int) $unit_id);
            if ($ignore_deleted == FALSE)
            {
                $this->db->where('deleted', $deleted);
            }

            return ($this->db->get()->num_rows() == 1);
        }

        return FALSE;
    }

    /*
	Gets total of rows
	*/
    public function get_total_rows()
    {
        $this->db->from('units');
        $this->db->where('deleted', 0);

        return $this->db->count_all_results();
    }

    /*
    Returns all the units
    */
    function get_all()
    {
        $this->db->from('units');
        $this->db->order_by("name", "asc");
        return $this->db->get()->result();
    }

    /*
	Get number of rows
	*/
    public function get_found_rows($search, $filters)
    {
        return $this->search($search, $filters)->num_rows();
    }


    function count_all()
    {
        $this->db->from('units');
        $this->db->where('deleted',0);
        return $this->db->count_all_results();
    }

    /*
     Inserts or updates a category
     */
    public function save_unit(&$unit_data, $unit_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(!$unit_id || !$this->exists($unit_id))
        {
            $success = $this->db->insert('units', $unit_data);
        }
        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    public function update_unit($unit_data, $unit_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
        $this->db->where('id', $unit_id);
        $success = $this->db->update('units', $unit_data);

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }
    /*
   Gets information about a particular unit
   */
    function get_info($unit_id)
    {
        $this->db->select('*', FALSE);
        $this->db->from('units');
        $this->db->where('id', $unit_id );

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $unit_id is NOT an unit
            $unit_obj=new stdClass();

            //Get all the fields from units table
            $fields = $this->db->list_fields('units');

            foreach ($fields as $field)
            {
                $unit_obj->$field='';
            }

            return $unit_obj;
        }
    }

    public function delete_unit($unit_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        $this->db->where('id', $unit_id);
        $success = $this->db->delete('units');

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }
}
