<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Employee extends Person
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
     Determines if a given person_id is an employee
     */
    function exists($person_id)
    {
        $this->db->from('employees');
        $this->db->join('person', 'person.id = employees.person_id');
        $this->db->where('employees.person_id', $person_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    /*
	Gets total of rows
	*/
    public function get_total_rows()
    {
        $this->db->from('employees');
        $this->db->where('deleted', 0);

        return $this->db->count_all_results();
    }


    /*
    Returns all the employees
    */
    function get_all()
    {
        $this->db->from('employees');
        $this->db->join('person', 'employees.person_id=person.id');
        $this->db->order_by("last_name", "asc");
        return $this->db->get()->result();
    }

    /*
    Gets information about a particular employee
    */
    public function get_info($employee_id)
    {
        $this->db->from('employees');
        $this->db->join('person', 'person.id = employees.person_id');
        $this->db->where('employees.person_id', $employee_id);
        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $employee_id is NOT an employee
            $person_obj = parent::get_info(-1);

            //Get all the fields from employee table
            //append those fields to base parent object, we we have a complete empty object
            foreach($this->db->list_fields('employees') as $field)
            {
                $person_obj->$field = '';
            }

            return $person_obj;
        }
    }

    /*
	Inserts or updates an employee
	*/
    public function save_employee(&$person_data, &$employee_data, $employee_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(parent::save($person_data, $employee_id))
        {
            if(!$employee_id || !$this->exists($employee_id))
			{
                $employee_data['person_id'] = $employee_id = $person_data['person_id'];
                $success = $this->db->insert('employees', $employee_data);
            }
		}

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    public function update_employee($person_data, $employee_data, $employee_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
            $this->db->where('person_id', $employee_id);
            $success = $this->db->update('employees', $employee_data);

            $this->db->where('id', $employee_id);
            $success &= $this->db->update('person', $person_data);
        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    /*
    Deletes one employee
    */
    public function update_status_to_active($employee_id)
    {
        $success = FALSE;

        $this->db->trans_start();
            $this->db->where('person_id', $employee_id);
            $success = $this->db->update('employees', array('status' => 1));
        $this->db->trans_complete();

        return $success;
    }

    public function update_status_to_inactive($employee_id)
    {
        $success = FALSE;

        $this->db->trans_start();
            $this->db->where('person_id', $employee_id);
            $success = $this->db->update('employees', array('status' => 0));
        $this->db->trans_complete();

        return $success;
    }
}
