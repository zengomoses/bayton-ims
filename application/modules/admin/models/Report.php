<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Report extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function sale_report($startDate)
    {
        $query = "SELECT s.id,s.sale_time,s.amount_paid,c.item_id,sum(c.amount) AS amount,
                    SUM(c.quantity_purchased) AS quantity_purchased,t.item_name,t.brand,
                    t.description
                  FROM sales AS s 
                  JOIN(sales_items AS c,items AS t) ON(s.invoice_number = c.invoice AND c.item_id=t.id)
                  WHERE s.sale_time='".$startDate."'";
        $result = $this->db->query($query)->result();

        return $result;
    }

    public function sale_report_range($startDate, $endDate)
    {
        $query = "SELECT s.id,s.sale_time,s.amount_paid,c.item_id,sum(c.amount) AS amount,
                    SUM(c.quantity_purchased) AS quantity_purchased,t.item_name,t.brand,
                    t.description
                  FROM sales AS s 
                  JOIN(sales_items AS c,items AS t) ON(s.invoice_number = c.invoice AND c.item_id=t.id)
                  WHERE s.sale_time>='".$startDate."' AND s.sale_time<='".$endDate."'";
        $result = $this->db->query($query)->result();

        return $result;
    }
}
