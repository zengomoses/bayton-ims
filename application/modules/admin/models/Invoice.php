<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Invoice extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function exists($invoice)
    {
        $this->db->from('proforma_items');
        $this->db->where('invoice_number', $invoice);
        
        return ($this->db->get()->num_rows() == 1);
    }

    public function save_invoice_data($invoice_data, $invoice_items){

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

            $this->db->insert('proforma_invoice', $invoice_data);
            $this->db->insert_batch('proforma_items', $invoice_items);
            
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    
    /*
    Returns all the invoices
    */
    public function get_all()
    {
        $this->db->from('proforma_invoice');
        return $this->db->get()->result();
    }

    public function get_info($invoice)
    {

        $this->db->from('proforma_invoice');
        $this->db->where('invoice_number', $invoice);
        return $this->db->get()->row();
    }

    function get_items($invoice)
    {
        $this->db->select('c.invoice_number, c.quantity,i.item_name, c.unit, i.brand, c.unit_price', FALSE);
        $this->db->from('proforma_items c');
        $this->db->join('items i', 'i.id = c.item', 'left');
        $this->db->where('c.invoice_number', $invoice);
      
        
        return $this->db->get()->result();
    }
}
