<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Units extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/unit','admin/category', 'admin/unit', 'admin/supplier', 'admin/employee', 'admin/stock'));
        $this->load->helper(array('form'));

    }

    public function units() {
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "items/units";
        $data['units'] = $this->unit->get_all();
        $data['title'] = "UNITS";
        $this->load->view($this->_container, $data);
    }

    public function view($unit_id=-1) {
        $data['unit_info'] = $this->unit->get_info($unit_id);
        $this->load->view('items/unit_form', $data);
    }

    public  function save()
    {
        $unit_data = array(
            'name'=>$this->input->post('unit_name'),
            'description'=>$this->input->post('description'),
        );

        if($this->unit->save_unit($unit_data))
        {
            echo json_encode(array('success'=>true));

        }
        else//failure
        {
            echo json_encode(array('success'=>false));
        }
    }

    public  function update($unit_id)
    {
        $unit_data = array(
            'name'=>$this->input->post('unit_name'),
        );

        if($this->unit->update_unit($unit_data, $unit_id))
        {
            echo json_encode(array('success'=>true));

        }
        else
        {
            echo json_encode(array('success'=>false));
        }
    }

    function delete($unit_id){
        if($this->unit->delete_unit($unit_id)){
            echo json_encode(array("success"=>true));
        }else{
            echo json_encode(array("success"=>false));
        }
    }
}
