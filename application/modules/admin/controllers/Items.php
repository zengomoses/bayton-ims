<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Items extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/item','admin/category', 'admin/item', 'admin/supplier', 'admin/employee', 'admin/stock', 'admin/unit'));
        $this->load->helper(array('form'));

    }

    public function items() {
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "items/items";
        $data['items'] = $this->item->get_all();
        $data['categories'] = $this->category->get_all();
        $data['suppliers'] = $this->supplier->get_all();
        $data['title'] = "ITEMS";
        $this->load->view($this->_container, $data);
    }

    public function stock() {
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "items/stocks";
        $data['suppliers'] = $this->supplier->get_all();
        $data['employees'] = $this->employee->get_all();
        $data['stocks'] = $this->stock->get_all();
        $data['title'] = "STOCKS";
        $this->load->view($this->_container, $data);
    }

    public function categories() {
        $data['categories'] = $this->category->get_all();
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "items/categories";
        $data['title'] = "CATEGORIES";
        $this->load->view($this->_container, $data);
    }

    public function barcode() {
        $this->load->view('admin/barcode');
    }

    public function view($item_id=-1) {
        $data['item_info'] = $this->item->get_info($item_id);
        $data['categories'] = $this->category->get_all();
        $data['units'] = $this->unit->get_all();
        $data['suppliers'] = $this->supplier->get_all();
        $this->load->view('items/form', $data);
    }

    public function delete($item_id=-1) {
        $data['item_info'] = $this->item->get_info($item_id);
        $this->load->view('items/form', $data);
    }


    public  function save()
    {
        $item_data = array(
            'item_name'=>$this->input->post('item_name'),
            'brand'=>$this->input->post('brand'),
            'category_id'=>$this->input->post('category'),
            'supplier_id'=>$this->input->post('supplier'),
            'barcode'=>$this->input->post('barcode'),
            'unit'=>$this->input->post('unit'),
            'description'=>$this->input->post('description'),
            'unit_selling_price'=>$this->input->post('unit_selling_price'),
            'outer_selling_price'=>$this->input->post('outer_selling_price'),
            'ctn_selling_price'=>$this->input->post('ctn_selling_price'),
            'dzn_selling_price'=>$this->input->post('dzn_selling_price'),
            'min_quantity'=>$this->input->post('min_quantity'),
            'buying_price'=>$this->input->post('buying_price'),
        );

        $item_units = array(
            'unit_id'=>$this->input->post('unit'),
            'outers_per_ctn'=>$this->input->post('qty_outer'),
            'pcs_per_outer'=>$this->input->post('qty_pcs'),
        );

        if($this->item->save_item($item_data, $item_units))
        {
             echo json_encode(array('success'=>true));

        }
        else//failure
        {
            echo json_encode(array('success'=>false));
        }
    }

    public  function update($item_id)
    {
        $item_data = array(
            'item_name'=>$this->input->post('item_name'),
            'brand'=>$this->input->post('brand'),
            'category_id'=>$this->input->post('category'),
            'supplier_id'=>$this->input->post('supplier'),
            'barcode'=>$this->input->post('barcode'),
            'unit'=>$this->input->post('unit'),
            'description'=>$this->input->post('description'),
            'unit_selling_price'=>$this->input->post('unit_selling_price'),
            'outer_selling_price'=>$this->input->post('outer_selling_price'),
            'ctn_selling_price'=>$this->input->post('ctn_selling_price'),
            'dzn_selling_price'=>$this->input->post('dzn_selling_price'),
            'buying_price'=>$this->input->post('buying_price'),
        );

        $item_units = array(
            'unit_id'=>$this->input->post('unit'),
            'outers_per_ctn'=>$this->input->post('qty_outer'),
            'pcs_per_outer'=>$this->input->post('qty_pcs'),
        );


        if($this->item->update_item($item_data,$item_units, $item_id))
        {
             echo json_encode(array('success'=>true));

        }
        else
        {
            echo json_encode(array('success'=>false));
        }
    }

    function Header()
    {
        $this->fpdf->Image('assets/images/byton_logo.png',10,6,40);
        $this->fpdf->SetFont('Arial','B',22);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(60,6,'BYTON GROUP LIMITED',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(60,5,'P.O Box 3314 MOB:0755 480057, 0658 480057, 0745 800802, 0766 800802',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(60,3,'MWANJELWA-MBEYA, TUNDUMA ROAD OPPOSITE MWANJELWA NEW MARKET',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(60,3,'Dealers in: Stationaries, Lamination, Binding, Printing all types of Cards,Book Sales, Ink Refills, e.t.c',0,0,'C');
        $this->fpdf->SetLineWidth(0.6);
        $this->fpdf->Line(0,28,418,28);
        $this->fpdf->Ln();
    }
    function Footer()
    {
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(10);
        date_default_timezone_set("Africa/Nairobi");
        $this->fpdf->Cell(0,10,date("F d, Y h:i A",time()),0,0,'L');

        $this->fpdf->Ln();
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(0,10,'Page '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
    }

    function items_list()
    {
        $this->fpdf->AddPage('L');
        $this->header();
        $header = array('ID', 'NAME', 'BRAND', 'DESCRIPTION','CATEGORY', 'UNIT', 'S.P/PCS','S.P/OUTER','S.P/CTN');

        $data = $this->item->get_all_print();

        $this->fpdf->SetFont('Helvetica','B',10);
        $this->fpdf->Cell(90);
        $this->fpdf->Cell(90,15, 'LIST OF GOODS',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Helvetica','',7);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFillColor(153,153,153);
        $this->fpdf->SetTextColor(255);
        $this->fpdf->SetDrawColor(89,89,89);
        $this->fpdf->SetLineWidth(.1);
        $this->fpdf->SetFont('','B');
        $w = array(10,38,38,80,38,15,20,20,20);
        for($i=0;$i<count($header);$i++)
            $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->fpdf->Ln();
        $this->fpdf->SetFillColor(224,235,255);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFont('');
        $fill = false;
        foreach($data as $row)
        {
            $this->fpdf->Cell($w[0],6,$row->id,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[1],6,$row->item_name,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[2],6,$row->brand,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[3],6,$row->description,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[4],6,$row->category,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[5],6,$row->unit,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[6],6,$row->unit_selling_price,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[7],6,$row->outer_selling_price,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[8],6,$row->ctn_selling_price,'LR',0,'L',$fill);
            $this->fpdf->Ln();
            $fill = !$fill;
        }
        $this->fpdf->Cell(array_sum($w),0,'','T');
        $this->footer();
        $this->fpdf->Output();

    }
}
