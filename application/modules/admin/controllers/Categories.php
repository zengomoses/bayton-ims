<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/20/17
 * Time: 5:02 PM
 */

class Categories extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('admin/category', 'admin/item'));
        $this->load->helper(array('form'));
    }

    public function view($category_id=-1) {
        $data['category_info'] = $this->category->get_info($category_id);
        $this->load->view('items/category_form',$data);
    }

    /*
   Inserts/updates an category
   */
    function save()
    {
        $category_data = array(
            'name'=>$this->input->post('category_name'),
        );

        if($this->category->save_category($category_data))
        {
            echo json_encode(array('success'=>true,));
        }else
        {
            echo json_encode(array('success'=>false,));
        }

    }

    function update($category_id)
    {
        $category_data = array(
            'name'=>$this->input->post('category_name'),
        );

        if($this->category->update_category($category_data, $category_id))
        {
            echo json_encode(array('success'=>true,));
        }else
        {
            echo json_encode(array('success'=>false,));
        }

    }

    function delete($category_id){
        if($this->category->delete_category($category_id)){
            echo json_encode(array("success"=>true));
        }else{
            echo json_encode(array("success"=>false));
        }
    }
}