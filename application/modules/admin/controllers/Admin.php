<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/store'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "dashboard";
        $data['title'] = "Dashboard";
        $this->load->view($this->_container, $data);
    }

    public function items(){
        $data = $this->store->get_item_quantity();
        echo json_encode($data);
    }
}
