<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Reports extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/report'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "reports";
        $data['title'] = "REPORTS";
        $this->load->view($this->_container, $data);
    }

    public function search_by_date()
    {
        //$daterange = "2017/12/15-2017/12/15";
        $daterange = $this->input->post('daterange');
        list($startDate, $endDate ) = explode("-","$daterange");
        $startDate = new DateTime("$startDate");
        $endDate = new DateTime("$endDate");

        $startDate = $startDate->format('Y-m-d');
        $endDate = $endDate->format('Y-m-d');

        if($startDate == $endDate){
            $data = $this->report->sale_report($startDate);
            echo json_encode($data);
        }else{
            $data = $this->report->sale_report_range($startDate, $endDate);
            echo json_encode($data);
        }
    }

    function Header()
    {
        $this->fpdf->Image('assets/images/byton_logo.png',10,6,40);
        $this->fpdf->SetFont('Arial','B',22);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(90,6,'BYTON GROUP LIMITED',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(90,5,'P.O Box 3314 MOB:0755 480057, 0658 480057, 0745 800802, 0766 800802',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(90,3,'MWANJELWA-MBEYA, TUNDUMA ROAD OPPOSITE MWANJELWA NEW MARKET',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(90,3,'Dealers in: Stationaries, Lamination, Binding, Printing all types of Cards,Book Sales, Ink Refills, e.t.c',0,0,'C');
        $this->fpdf->SetLineWidth(0.6);
        $this->fpdf->Line(1,28,418,28);
        $this->fpdf->Ln();
    }
    function Footer()
    {
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(10);
        date_default_timezone_set("Africa/Nairobi");
        $this->fpdf->Cell(0,10,date("F d, Y h:i A",time()),0,0,'L');

        $this->fpdf->Ln();
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(0,10,'Page '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
    }

    function print_report()
    {
        $this->fpdf->AddPage('L');
        $this->header();

        $header = array('ITEM #','ITEM NAME','BRAND','DESCRIPTION','QTY','SALES AMOUNT');
        $daterange = urldecode($this->uri->segment(4)."/".$this->uri->segment(5)."/".$this->uri->segment(6)."/".$this->uri->segment(7)."/".$this->uri->segment(8));
        list($startDate, $endDate ) = explode("-","$daterange");
        $startDate1 = new DateTime("$startDate");
        $endDate1 = new DateTime("$endDate");

        $startDate = $startDate1->format('Y-m-d');
        $endDate = $endDate1->format('Y-m-d');

        if($startDate == $endDate){
            $data = $this->report->sale_report($startDate);
            $this->fpdf->SetFont('Times','B',14);
            $this->fpdf->Cell(110);
            $this->fpdf->Cell(110,15, 'DAILY SALES REPORT',0,0,'C');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Times','B',11);
            $this->fpdf->SetXY( 20 , 40 );
            $this->fpdf->Cell(20, 4, "DATE # : ", '', '', "L");
            $this->fpdf->SetFont('Times','',11);
            $this->fpdf->SetXY( 45 , 40 );
            $this->fpdf->Cell(20, 4,str_replace('-','/',$startDate), '', '', "L");
            $this->fpdf->Ln();
            $this->fpdf->Ln();
            $this->fpdf->Cell(10);
            $this->fpdf->SetFont('Helvetica','',7);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetFillColor(153,153,153);
            $this->fpdf->SetTextColor(255);
            $this->fpdf->SetDrawColor(89,89,89);
            $this->fpdf->SetLineWidth(0.1);
            $this->fpdf->SetFont('','B');
            $w = array(12,60,40,60,30,32);
            for($i=0;$i<count($header);$i++)
                $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
            $this->fpdf->Ln();
            $this->fpdf->SetFillColor(224,235,255);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetFont('');
            $fill = false;
            $sum = 0;
            foreach($data as $row)
            {
                $this->fpdf->Cell(10);
                $this->fpdf->Cell($w[0],6,$row->id,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[1],6,$row->item_name,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[2],6,$row->brand,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[3],6,$row->description,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[4],6,$row->quantity_purchased,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[5],6,money_format('%i',$row->amount),'LR',0,'L',$fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $sum += $row->amount;
            }
            $this->fpdf->Cell(10);
            $this->fpdf->Cell(array_sum($w),0,'','T');
            $this->fpdf->Ln();
            $this->fpdf->Cell(135);
            $this->fpdf->SetFont('Helvetica','B',9);
            $this->fpdf->Cell(17,6,'Total Sales : ','LB',0,'L');
            $this->fpdf->SetFont('Helvetica','',9);
            $this->fpdf->Cell(31,6,money_format('%i',$sum),'RB',0,'R');

            $this->footer();
            $this->fpdf->Output();

        }else{
            $data = $this->report->sale_report_range($startDate, $endDate);
//            $option ="";
//            $d = $startDate1->diff($endDate1)->format("%a");
//            if($d <6){
//              $option = "DAILY";
//            }elseif ($d >=6 && $d <= 7 ){
//                $option = "WEEKLY";
//            }elseif ($d >=27 && $d <=31){
//                $option = "MONTHLY";
//            }elseif ($d >=89 && $d<=92){
//                $option = "QUARTERLY";
//            }elseif ($d>=364 && $d<=366){

            $this->fpdf->SetFont('Times','B',14);
            $this->fpdf->Cell(90);
            $this->fpdf->Cell(90,15,"SALES REPORT",0,0,'C');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Times','B',11);
            $this->fpdf->SetXY( 20 , 40 );
            $this->fpdf->Cell(20, 4, "DATE # : ", '', '', "L");
            $this->fpdf->SetFont('Times','',11);
            $this->fpdf->SetXY( 45 , 40 );
            $this->fpdf->Cell(0, 4, "From ".str_replace('-','/',$startDate)." To ".str_replace('-','/',$endDate), '', '', "L");
            $this->fpdf->Ln();
            $this->fpdf->Ln();
            $this->fpdf->Cell(10);
            $this->fpdf->SetFont('Helvetica','',7);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetFillColor(153,153,153);
            $this->fpdf->SetTextColor(255);
            $this->fpdf->SetDrawColor(89,89,89);
            $this->fpdf->SetLineWidth(0.1);
            $this->fpdf->SetFont('','B');
            $w = array(12,60,40,60,30,32);
            for($i=0;$i<count($header);$i++)
                $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
            $this->fpdf->Ln();
            $this->fpdf->SetFillColor(224,235,255);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetFont('');
            $fill = false;
            $sum = 0;
            foreach($data as $row)
            {
                $this->fpdf->Cell(10);
                $this->fpdf->Cell($w[0],6,$row->id,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[1],6,$row->item_name,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[2],6,$row->brand,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[3],6,$row->description,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[4],6,$row->quantity_purchased,'LR',0,'L',$fill);
                $this->fpdf->Cell($w[5],6,money_format('%i',$row->amount),'LR',0,'L',$fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $sum += $row->amount;
            }
            $this->fpdf->Cell(10);
            $this->fpdf->Cell(array_sum($w),0,'','T');
            $this->fpdf->Ln();
            $this->fpdf->Cell(135);
            $this->fpdf->SetFont('Helvetica','B',9);
            $this->fpdf->Cell(17,6,'Total Sales : ','LB',0,'L');
            $this->fpdf->SetFont('Helvetica','I',9);
            $this->fpdf->Cell(31,6,money_format('%i',$sum),'RB',0,'R');

            $this->footer();
            $this->fpdf->Output();

        }
    }



}
