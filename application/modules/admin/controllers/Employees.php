<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Employees extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/employee'));
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "employees/employees";
        $data['employees'] = $this->employee->get_all();
        $data['title'] = "EMPLOYEES";
        $this->load->view($this->_container, $data);
    }

    public function view($employee_id=-1) {
        $data['person_info'] = $this->employee->get_info($employee_id);
        $this->load->view('employees/form',$data);
    }


    /*
	Inserts/updates an employee
	*/
    function save()
    {
        $phone_number_1 = "255".substr($this->input->post('phone_number_1'),1);
        if($this->input->post('phone_number_2') != ""){
            $phone_number_2 = "255".substr($this->input->post('phone_number_2'),1);
        }else{
            $phone_number_2 = "";
        }
        if($this->input->post('phone_number_3') != ""){
            $phone_number_3 = "255".substr($this->input->post('phone_number_3'),1);
        }else{
            $phone_number_3 = "";
        }

        $person_data = array(
            'first_name'=>$this->input->post('first_name'),
            'last_name'=>$this->input->post('last_name'),
            'gender'=>$this->input->post('gender'),
            'email'=>$this->input->post('email'),
            'phone_number_1'=>$phone_number_1,
            'phone_number_2'=>$phone_number_2,
            'phone_number_3'=>$phone_number_3,
            'address'=>$this->input->post('address'),
            'city'=>$this->input->post('city'),
            'region'=>$this->input->post('region'),
            'district'=>$this->input->post('district'),
            'comments'=>$this->input->post('comments')
        );

        $employee_data=array(
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'level'=>$this->input->post('level')
        );

        if($this->employee->save_employee($person_data,$employee_data))
        {
            echo json_encode(array('success'=>true,));
        }
    }

   function update($employee)
    {
        $phone_number_1 = "255".substr($this->input->post('phone_number_1'),1);
        if($this->input->post('phone_number_2') != ""){
            $phone_number_2 = "255".substr($this->input->post('phone_number_2'),1);
        }else{
            $phone_number_2 = "";
        }
        if($this->input->post('phone_number_3') != ""){
            $phone_number_3 = "255".substr($this->input->post('phone_number_3'),1);
        }else{
            $phone_number_3 = "";
        }

        $person_data = array(
            'first_name'=>$this->input->post('first_name'),
            'last_name'=>$this->input->post('last_name'),
            'gender'=>$this->input->post('gender'),
            'email'=>$this->input->post('email'),
            'phone_number_1'=>$phone_number_1,
            'phone_number_2'=>$phone_number_2,
            'phone_number_3'=>$phone_number_3,
            'address'=>$this->input->post('address'),
            'city'=>$this->input->post('city'),
            'region'=>$this->input->post('region'),
            'district'=>$this->input->post('district'),
            'comments'=>$this->input->post('comments')
        );

        //Password has been changed OR first time password set
        if($this->input->post('password')!='')
        {
            $employee_data=array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'level'=>$this->input->post('level')
            );
        }
        else //Password not changed
        {
            $employee_data=array('username'=>$this->input->post('username'));
        }

        if($this->employee->update_employee($person_data,$employee_data, $employee))
        {
            echo json_encode(array('success'=>true));
        }
        else//failure
        {
            echo json_encode(array('success'=>false));
        }
    }

   public function activate_account($employee){
        $result = $this->employee->update_status_to_active($employee);

        if($result){
            echo json_encode(array('success'=>true,'message'=>'Account has been activated'));
        }else{
            echo json_encode(array('success'=>false,'message'=>'Account cannot be activated'));
        }
   }

   public function deactivate_account($employee){
        $result = $this->employee->update_status_to_inactive($employee);

        if($result){
            echo json_encode(array('success'=>true,'message'=>'Account has been activated'));
        }else{
            echo json_encode(array('success'=>false,'message'=>'Account cannot be activated'));
        }
   }

   function Header()
   {
       $this->fpdf->Image('assets/images/byton_logo.png',10,6,40);
       $this->fpdf->SetFont('Arial','B',22);
       $this->fpdf->Cell(110);
       $this->fpdf->Cell(10,6,'BYTON GROUP LIMITED',0,0,'C');
       $this->fpdf->Ln();
       $this->fpdf->SetFont('Arial','B',9);
       $this->fpdf->Cell(110);
       $this->fpdf->Cell(10,5,'P.O Box 3314 MOB:0755 480057, 0658 480057, 0745 800802, 0766 800802',0,0,'C');
       $this->fpdf->Ln();
       $this->fpdf->SetFont('Arial','',8);
       $this->fpdf->Cell(110);
       $this->fpdf->Cell(10,3,'MWANJELWA-MBEYA, TUNDUMA ROAD OPPOSITE MWANJELWA NEW MARKET',0,0,'C');
       $this->fpdf->Ln();
       $this->fpdf->SetFont('Arial','',8);
       $this->fpdf->Cell(110);
       $this->fpdf->Cell(10,3,'Dealers in: Stationaries, Lamination, Binding, Printing all types of Cards,Book Sales, Ink Refills, e.t.c',0,0,'C');
       $this->fpdf->SetLineWidth(0.6);
       $this->fpdf->Line(1,28,209,28);
       $this->fpdf->Ln();
   }
   function Footer()
   {
       $this->fpdf->SetY(-15);
       $this->fpdf->SetFont('Arial','I',8);
       $this->fpdf->Cell(10);
       date_default_timezone_set("Africa/Nairobi");
       $this->fpdf->Cell(0,10,date("F d, Y h:i A",time()),0,0,'L');

       $this->fpdf->Ln();
       $this->fpdf->SetY(-15);
       $this->fpdf->SetFont('Arial','I',8);
       $this->fpdf->Cell(0,10,'Page '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
   }

   function emp_print()
   {
        $this->fpdf->AddPage('P');
        $this->header();
        $header = array('ID', 'FIRST NAME', 'LAST NAME', 'GENDER', 'PHONE #', 'E-MAIL');

        $data = $this->employee->get_all();

        $this->fpdf->SetFont('Helvetica','B',11);
        $this->fpdf->Cell(90);
        $this->fpdf->Cell(10,15, 'LIST OF EMPLOYEES',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Helvetica','',9);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFillColor(153,153,153);
        $this->fpdf->SetTextColor(255);
        $this->fpdf->SetDrawColor(89,89,89);
        $this->fpdf->SetLineWidth(.1);
        $this->fpdf->SetFont('','B');
        $w = array(8, 30, 30, 30, 30, 45);
        for($i=0;$i<count($header);$i++)
            $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->fpdf->Ln();
        $this->fpdf->SetFillColor(224,235,255);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFont('');
        $fill = false;
        foreach($data as $row)
        {
            $this->fpdf->Cell(10);
            $this->fpdf->Cell($w[0],6,$row->id,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[1],6,$row->first_name,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[2],6,$row->last_name,'LR',0,'L',$fill);
            if($row->gender == 0){
                $this->fpdf->Cell($w[3],6,'Female','LR',0,'L',$fill);
            }
            if($row->gender == 1){
                $this->fpdf->Cell($w[3],6,'Male','LR',0,'L',$fill);
            }
            $this->fpdf->Cell($w[4],6,"+".$row->phone_number_1,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[5],6,$row->email,'LR',0,'L',$fill);
            $this->fpdf->Ln();
            $fill = !$fill;
        }
        $this->fpdf->Cell(10);
        $this->fpdf->Cell(array_sum($w),0,'','T');
        $this->footer();
        $this->fpdf->Output();

   }
}
