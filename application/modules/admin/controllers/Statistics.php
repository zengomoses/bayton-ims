<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Statistics extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('admin/item','admin/category', 'admin/item', 'admin/supplier', 'admin/employee', 'admin/stock', 'admin/store', 'admin/unit'));
    }

    public function index() {
        $data['page'] = $this->config->item('bayton_ims_template_dir_admin')."statistics";
        $data['stores'] = $this->store->get_all();
        $data['title'] = "STATS";
        $this->load->view($this->_container, $data);
    }

    public function item_quantity_out(){
        $data = $this->item->items_taken_out();
         echo json_encode($data);
    }
    public function item_ranking_by_category(){
        $data = $this->item->items_taken_out_by_category();
         echo json_encode($data);
    }
    public function top_5_profiting_items(){
        $data = $this->item->top_5_items();
         echo json_encode($data);
    }
    public function byton(){
        $data = $this->item->byton();
         echo json_encode($data);
    }
}
