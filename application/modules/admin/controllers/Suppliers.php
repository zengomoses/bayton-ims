<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Suppliers extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/supplier'));
        $this->load->helper(array('form'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "suppliers/suppliers";
        $data['suppliers'] = $this->supplier->get_all();
        $data['title'] = "SUPPLIERS";
        $this->load->view($this->_container, $data);
    }

    public function view($supplier_id=-1) {
        $data['person_info'] = $this->supplier->get_info($supplier_id);
        $this->load->view('suppliers/form',$data);
    }

    /*
  Inserts/updates an supplier
  */
    function save()
    {
        $phone_number_1 = "255".substr($this->input->post('phone_number_1'),1);
        if($this->input->post('phone_number_2') != ""){
            $phone_number_2 = "255".substr($this->input->post('phone_number_2'),1);
        }else{
            $phone_number_2 = "";
        }
        if($this->input->post('phone_number_3') != ""){
            $phone_number_3 = "255".substr($this->input->post('phone_number_3'),1);
        }else{
            $phone_number_3 = "";
        }

        $person_data = array(
            'first_name'=>$this->input->post('first_name'),
            'last_name'=>$this->input->post('last_name'),
            'gender'=>$this->input->post('gender'),
            'email'=>$this->input->post('email'),
            'phone_number_1'=>$phone_number_1,
            'phone_number_2'=>$phone_number_2,
            'phone_number_3'=>$phone_number_3,
            'address'=>$this->input->post('address'),
            'city'=>$this->input->post('city'),
            'region'=>$this->input->post('region'),
            'district'=>$this->input->post('district'),
            'comments'=>$this->input->post('comments')
        );

        $supplier_data=array(
            'company_name'=>$this->input->post('company_name'),
        );

        if($this->supplier->save_supplier($person_data,$supplier_data))
        {
            echo json_encode(array('success'=>true,));
        }
    }

    function update($supplier_id)
    {
        $phone_number_1 = "255".substr($this->input->post('phone_number_1'),1);
        if($this->input->post('phone_number_2') != ""){
            $phone_number_2 = "255".substr($this->input->post('phone_number_2'),1);
        }else{
            $phone_number_2 = "";
        }
        if($this->input->post('phone_number_3') != ""){
            $phone_number_3 = "255".substr($this->input->post('phone_number_3'),1);
        }else{
            $phone_number_3 = "";
        }

        $person_data = array(
            'first_name'=>$this->input->post('first_name'),
            'last_name'=>$this->input->post('last_name'),
            'gender'=>$this->input->post('gender'),
            'email'=>$this->input->post('email'),
            'phone_number_1'=>$phone_number_1,
            'phone_number_2'=>$phone_number_2,
            'phone_number_3'=>$phone_number_3,
            'address'=>$this->input->post('address'),
            'city'=>$this->input->post('city'),
            'region'=>$this->input->post('region'),
            'district'=>$this->input->post('district'),
            'comments'=>$this->input->post('comments')
        );

        $supplier_data=array(
            'company_name'=>$this->input->post('company_name'),
        );

        if($this->supplier->update_supplier($person_data,$supplier_data, $supplier_id))
        {
            echo json_encode(array('success'=>true,));
        }else{
            echo json_encode(array('success'=>false,));
        }
    }

}
