<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
} else {
    header("location:". base_url('auth')."");
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="row">
            <div class="col-md-6">
                <i class="fa fa-2x fa-home">&nbsp;STATISTICS</i>
            </div>
            <div class="col-md-6">
                <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                    <i class="fa fa-lg fa-refresh"></i> Refresh Page
                </button>
            </div>
        </div><hr/>
        <div class="col-md-12 col-sm-12 col-xs-12">
<!--            <div><h4>ITEM STATISTICS</h4></div>-->
            <div class="dashboard_graph" style="background-color: #D5D4D3">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <canvas id="myChart" width="400" height="100"></canvas>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div style="text-align: center;">
                    <h5 style="font-weight: bold; font-size: 12px; color: #3c3c3c;">ITEMS IN STORE</h5>
                </div>
                <div>
                   <table class="table table-bordered table-condensed">
                      <?php foreach ($stores as $store):;?>
                          <tr>
                              <th><?= ucfirst($store->name)?></th>
                              <td>
                                  <table class="table table-condensed">
                                      <tr id="store_<?= $store->id?>"></tr>
                                  </table>
                              </td>
                          </tr>
                       <?php endforeach;?>
                   </table>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div>
                    <div style="text-align: center;">
                        <h5 style="font-weight: bold; font-size: 12px; color: #3c3c3c;">RANKING CATEGORIES FREQUENTLY TAKEN FROM STORE</h5>
                    </div>
                    <div>
                        <ol id="categories"></ol>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div>
                    <div style="text-align: center;">
                        <h5 style="font-weight: bold; font-size: 12px; color: #3c3c3c;">TOP 5 HIGHLY PROFITING ITEMS</h5>
                    </div>
                    <div>
                        <ol id="items"></ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function(){
        var items = [];
        var quantity = [];
        var bgColor = [];
        $.post("<?= base_url('admin/statistics/item_quantity_out')?>", function(data){
            var obj = JSON.parse(data);
            $.each(obj, function(i,item){
                var num = Math.round(0xffffff * Math.random());
                var r = num >> 16;
                var g = num >> 8 & 255;
                var b = num & 255;

                items.push(item.item_name);
                quantity.push(item.quantity_out);
                bgColor.push('rgba('+r+','+g+','+b+',0.8)');
            });
            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: items,
                    datasets: [{
                        label: 'Quantity',
                        data: quantity,
                        backgroundColor: bgColor,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            },
                            offsetGridLines: false
                        }]
                    },
                    title: {
                        display: true,
                        text: 'GRAPH SHOWING GOODS TAKEN OUT OF STORE'
                    }
                }
            });
        });
    });

    $(document).ready(function(){
        $.post("<?= base_url('admin/statistics/item_ranking_by_category')?>", function(data) {
            var obj = JSON.parse(data);
            $.each(obj, function(i,category){
                // CREATE AND ADD SUB LIST ITEMS.
                var li = $('<li/>')
                    .text(category.name+" ("+category.quantity_out+")")
                    .appendTo('#categories');
            });
        });
        $.post("<?= base_url('admin/statistics/top_5_profiting_items')?>", function(data) {

            var obj = JSON.parse(data);
            $.each(obj, function(i,item){
                // CREATE AND ADD SUB LIST ITEMS.
                var li = $('<li/>')
                    .text(item.item_name+" ("+item.category_name+"["+item.quantity_out+"])")
                    .appendTo('#items');
            });
        });
        $.post("<?= base_url('admin/statistics/items_in_store')?>", function(data) {

            var obj = JSON.parse(data);
            $.each(obj, function(i,item){
                var store_id = "#store_"+item.id;
                var th = $('<th/>')
                    .text(item.item_name)
                    .appendTo(store_id);
                var td = $('<td/>')
                    .text(item.quantity_in)
                    .appendTo(store_id);
            });
        });
    });
</script>