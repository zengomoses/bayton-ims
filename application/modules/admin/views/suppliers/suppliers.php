<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;SUPPLIERS</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
            <button class="btn btn-info btn-md pull-right add-supplier-btn" data-target="#add_supplier" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> New Supplier
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                        <tr>
                            <th>ID</th>
                            <th>COMPANY</th>
                            <th>CONTACT PERSON</th>
                            <th>PHONE #</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($suppliers as $supplier):?>
                        <tr>
                            <td class="supplier_id"><?= $supplier->person_id?></td>
                            <td><?= $supplier->company_name?></td>
                            <td><?= $supplier->first_name.' '.$supplier->last_name?></td>
                            <td><?= $supplier->phone_number_1?></td>
                            <td>
                                <button class="btn btn-xs btn-primary view-supplier-btn" data-target="#view_supplier" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>View
                                </button>
                                <button class="btn btn-xs btn-success edit-supplier-btn" data-target="#edit_supplier" data-toggle="modal">
                                    <i class="fa fa-pencil-square-o">&nbsp;</i>Edit
                                </button>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".add-supplier-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/suppliers/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_supplier').modal(/* options object here*/);
            $('#add_supplier').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/suppliers/')?>";
            });
        });
    });
    $(".edit-supplier-btn").click(function () {
        var supplier_id = $(this).closest("tr").find(".supplier_id").text();
        // get needed html
        $.get("<?= base_url('admin/suppliers/view/')?>" + supplier_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_supplier').modal(/* options object here*/);
            $('#edit_supplier').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/suppliers/')?>";
            });
        });
    });

    $(".view-supplier-btn").click(function () {
        var supplier_id = $(this).closest("tr").find(".supplier_id").text();
        // get needed html
        $.get("<?= base_url('admin/suppliers/view/')?>" + supplier_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_supplier').modal(/* options object here*/);
            $('#view_supplier').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/suppliers/')?>";
            });
        });
    });
</script>
