<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">
        <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-shopping-cart"></i> Total Sales</span>
            <div class="count" style="height: 45px" id="sales_today">0</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i></i>  Today</span>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-8 tile_stats_count">
            <span class="count_top"><i class="fa fa-money"></i>  Income</span>
            <div class="count" id="income">0.00</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i></i> Today</span>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-8 tile_stats_count">
            <span class="count_top"><i class="fa fa-shopping-bag"></i>  Top Sold</span>
            <div class="count green" id="top_sold">None</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i></i> Today</span>
        </div>
    </div>
    <!-- /top tiles -->

    <div class="row">
        <div class="col-md-12"><h3>What would you like to do?</h3></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <p>
                <a href="<?= base_url()?>admin/employees" class="btn btn-sq-lg btn-primary">
                    <i class="fa fa-user fa-5x"></i><br/><br/><br/>
                    View Employees
                </a>
                <a href="<?= base_url()?>admin/items/stock" class="btn btn-sq-lg btn-success">
                    <i class="fa fa-shopping-bag fa-5x"></i><br/><br/><br/>
                    View Stock
                </a>
                <a href="<?= base_url()?>admin/sales" class="btn btn-sq-lg btn-info">
                    <i class="fa fa-cart-arrow-down fa-5x"></i><br/><br/><br/>
                    View Sales
                </a>
                <a href="<?= base_url()?>admin/statistics" class="btn btn-sq-lg btn-warning">
                    <i class="fa fa-bar-chart fa-5x"></i><br/><br/><br/>
                    View Statistics
                </a>
                <a href="<?= base_url()?>auth/logout" class="btn btn-sq-lg btn-danger">
                    <i class="fa fa-sign-out fa-5x"></i><br/><br/><br/>
                    Log out
                </a>
            </p>
        </div>
    <br />
</div>
<!-- /page content -->
<script>
    function updateSalesCount() {
        $("#sales_today").empty();
        $.ajax({
            url: "<?= base_url('admin/sales/total_sales')?>",
            success: function(data) {
              $("#sales_today").append(data);
            }
        });

        setTimeout(updateSalesCount, 10000); // you could choose not to continue on failure...

    }
    function updateIncome() {
        $("#income").empty();
        $.ajax({
            url: "<?= base_url('admin/sales/total_income_today')?>",
            success: function(data) {
              $("#income").append(data);
            }
        });

        setTimeout(updateIncome, 10000); // you could choose not to continue on failure...

    }
    function top_sold() {
        $("#top_sold").empty();
        $.ajax({
            url: "<?= base_url('admin/sales/top_sold_item_today')?>",
            success: function(data) {
                $("#top_sold").append(data);
            }
        });

        setTimeout(top_sold, 10000); // you could choose not to continue on failure...

    }
    // function check_stock_status() {
    //     $.ajax({
    //         dataType:"JSON",
    //         url: "<?= base_url('admin/admin/items')?>",
    //         success: function(data) {
    //             $.each(data, function(i, val){
    //                 if(val.quantity_in < val.min_quantity){
    //                     Notifier.warning("The "+val.item_name+" "+val.brand+" "+val.description+" is below the required stock limit", "Stock Warning");
    //                 }
    //             });

    //         }
    //     });
    //     setTimeout(check_stock_status, 50000);
    // }
    $(function() {
        updateSalesCount();
        updateIncome();
        top_sold();
        // check_stock_status();
    });
</script>