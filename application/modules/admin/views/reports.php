<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    /*main {*/
        /*min-width: 320px;*/
        /*max-width: 1100px;*/
        /*padding: 20px;*/
        /*margin: 0 auto;*/
        /*background: #fff;*/
    /*}*/

    section {
        display: none;
        padding: 20px 0 0;
        border-top: 1px solid #ddd;
    }

    input {
        display: none;
    }

    label {
        display: inline-block;
        margin: 0 0 -1px;
        padding: 15px 25px;
        font-weight: 600;
        text-align: center;
        color: #bbb;
        border: 1px solid transparent;
    }

    label:before {
        font-family: fontawesome;
        font-weight: normal;
        margin-right: 10px;
    }

    label[for*='1']:before { content: '\f2bc'; }
    label[for*='2']:before { content: '\f0ca'; }
    label[for*='3']:before { content: '\f02f'; }
    label[for*='4']:before { content: '\f1a9'; }

    label:hover {
        color: #888;
        cursor: pointer;
    }

    input:checked + label {
        color: #555;
        border: 1px solid #ddd;
        border-top: 2px solid orange;
        border-bottom: 1px solid #fff;
    }

    #tab1:checked ~ #content1,
    #tab2:checked ~ #content2,
    #tab3:checked ~ #content3,
    #tab4:checked ~ #content4 {
        display: block;
    }

</style>
<div class="right_col" role="main">
    <div class="row">
        <div class="row">
            <div class="col-md-6">
                <i class="fa fa-2x fa-home">&nbsp;REPORTS</i>
            </div>
            <div class="col-md-6">
                <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                    <i class="fa fa-lg fa-refresh"></i> Refresh Page
                </button>
            </div>
        </div><hr/>
        <div class="col-md-12" style="margin-top: -20px;">
            <div class="col-md-12">
                <div>
                    <input id="tab1" type="radio" name="tabs" checked>
                    <label for="tab1">SALES REPORT</label>

<!--                    <input id="tab2" type="radio" name="tabs">-->
<!--                    <label for="tab2">GOODS REPORTS</label>-->

                    <section id="content1">
                        <div id="report-options">
                            <div class="form-group col-md-4">
                                <input class="form-control" type="text" name="daterange" value="" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%"/>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary" id="search_date">Search</button>
                                <button class="btn btn-default" id="print-btn">Print</button>
                            </div>
                        </div>
                        <table id="tableData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                            <thead style="background-color: #34495E; color: #f6f6f6">
                            <tr>
                                <th>ITEM #</th>
                                <th>ITEM NAME</th>
                                <th>BRAND</th>
                                <th>DESCRIPTION</th>
                                <th>QUANTITY</th>
                                <th>SALES AMOUNT</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            locale: {
                format: 'YYYY/MM/DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        $("#search_date").on('click', function(){
           // $("#sales_content").empty();
            var daterange = $('input[name="daterange"]').val();
            var dataRow = "";
            $.ajax({
                type: "POST",
                dataType:"JSON",
                data: {daterange:daterange},
                url: "<?= base_url('admin/reports/search_by_date')?>",
                success: function(data){
                    $.each(data, function(i, val){
                        dataRow += "<tr>";
                        dataRow += "<td>" + val.id + "</td>";
                        dataRow += "<td>" + val.item_name + "</td>";
                        dataRow += "<td>" + val.brand + "</td>";
                        dataRow += "<td>" + val.description + "</td>";
                        dataRow += "<td>" + val.quantity_purchased + "</td>";
                        dataRow += "<td>" + val.amount + "</td>";
                        dataRow += "</tr>"
                    });
                    $("#tableData tbody").html(dataRow);
                }
            });
        });

        $("#print-btn").on('click', function(){
            var daterange = $('input[name="daterange"]').val();
            window.open("<?= base_url('admin/reports/print_report/')?>"+daterange, 'Sales Report', 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=0,left=0,top=0');
            //window.location.assign('<?=base_url('admin/reports')?>');
        });
    });
</script>
