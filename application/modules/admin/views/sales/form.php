<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    .modal-dialog{
        width: 800px;
    }
</style>
<div class="modal fade" id="view_sales">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--           modal header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Sales Info </h4>
            </div>
            <!--                 modal body-->
            <div class="modal-body">
                <div class="row">
                    <div class="panel panel-default mypanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-responsive table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th>Invoice ID</th>
                                                <td class="stock_id"><?= $sales_info->invoice_number?></td>
                                            </tr>
                                            <tr>
                                                <th>Date</th>
                                                <td><?= $sales_info->sale_time ?></td>
                                            </tr>
                                            <tr>
                                                <th>Customer</th>
                                                <?php foreach ($customers as $customer):?>
                                                    <?php if($sales_info->customer_id == $customer->id):?>
                                                        <td><?= $customer->first_name." ".$customer->last_name;?></td>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </tr>
                                            <tr>
                                                <th>Transaction made by</th>
                                                <?php foreach ($employees as $employee):?>
                                                    <?php if($sales_info->employee_id == $employee->id):?>
                                                        <td><?= $employee->first_name." ".$employee->last_name;?></td>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </tr>
                                        <tr><td></td><td></td></tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-responsive table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th>Total Amount</th>
                                                <td><?= $sales_info->amount_paid + $sales_info->amount_due ?></td>
                                            </tr>
                                            <tr>
                                                <th>Total Discount</th>
                                                <td><?= $sales_info->discount." %"?></td>
                                            </tr>
                                            <tr>
                                                <th>Amount Paid</th>
                                                <td><?= $sales_info->amount_paid?></td>
                                            </tr>
                                            <tr>
                                                <th>Amount Due</th>
                                                <td><?= $sales_info->amount_due?></td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <?php if($sales_info->sale_status == 1):?>
                                                    <td>Pending</td>
                                                <?php endif;?>
                                                <?php if($sales_info->sale_status == 0):?>
                                                    <td>Paid</td>
                                                <?php endif;?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                                        <thead style="background-color: #34495E; color: #f6f6f6">
                                        <tr>
                                            <th>ITEM NAME</th>
                                            <th>BRAND</th>
                                            <th>DESCRIPTION</th>
                                            <th>UNIT PRICE</th>
                                            <th>DISCOUNT</th>
                                            <th>QUANTITY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($items_info as $item):?>
                                            <tr>
                                                <td><?= $item->item_name?></td>
                                                <td><?= $item->brand?></td>
                                                <td><?= $item->description?></td>
                                                <td><?= $item->unit_selling_price?></td>
                                                <td><?= $item->disc?></td>
                                                <td><?= $item->quantity_purchased?></td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" id="btn-print">Print Invoice</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_sales">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit sales</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div id="message"></div>
                <?php echo form_open('',array('id'=>'edit_sales_form'));?>
                <div class="sales-info">
                    <fieldset id="sales-info">
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Invoice #', 'invoice', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'invoice',
                                        'id'=>'invoice',
                                        'class'=>'form-control input-sm',
                                        'value' => $sales_info->invoice_number,
                                        'disabled'=>TRUE,)
                                );?>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Amount Due', 'amount_due', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'amount_due',
                                        'id'=>'amount_due',
                                        'class'=>'form-control input-sm',
                                        'value' => $sales_info->amount_due,
                                        'disabled'=>TRUE)
                                );?>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Payed amount', 'new_amount', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'new_amount',
                                        'id'=>'new_amount',
                                        'class'=>'form-control input-sm',
                                        'required'=>TRUE,)
                                );?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="updatebutton">Update</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#edit_sales_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                new_amount: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter sales name'
                        },
                        numeric: {
                            message: 'Must contain figures only'
                        }
                    }
                }
            }
        });

        $("#updatebutton").click(function(e){  // passing down the event
            var validator = $('#edit_sales_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/sales/update/').$sales_info->invoice_number?>',
                    type: 'POST',
                    data: $("#edit_sales_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Sales Amount due has been update successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
        $('#btn-print').on('click', function(){
            window.open("<?= base_url('admin/sales/print_invoice/').$sales_info->invoice_number?>", 'Invoice', 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=0,left=0,top=0');
            window.location.assign('<?=base_url('admin/sales')?>');
        })
    });
</script>





