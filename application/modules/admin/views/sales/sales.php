<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;SALES</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">

            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>INVOICE</th>
                        <th>CUSTOMER</th>
                        <th>EMPLOYEE</th>
                        <th>SALE DATE</th>
                        <th>DISCOUNT</th>
                        <th>STATUS</th>
                        <th width="55"></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($sales as $sale):?>
                        <tr>
                            <td><?= $sale->id?></td>
                            <td  class="sales_invoice"><?= $sale->invoice_number?></td>
                            <?php foreach ($customers as $customer):?>
                                <?php if($sale->customer_id === $customer->id):?>
                                    <td><?= $customer->first_name." ".$customer->last_name;?></td>
                                <?php endif;?>
                            <?php endforeach;?>
                            <?php foreach ($employees as $employee):?>
                                <?php if($sale->employee_id == $employee->id):?>
                                    <td><?= $employee->first_name." ".$employee->last_name;?></td>
                                <?php endif;?>
                            <?php endforeach;?>
                            <td><?= $sale->sale_time?></td>
                            <td><?= $sale->discount?></td>
                            <?php if($sale->sale_status == 1):?>
                                <td>Pending</td>
                            <?php elseif ($sale->sale_status == 0):;?>
                                <td>Paid</td>
                            <?php endif;?>
                            <td>
                                <button class="btn btn-xs btn-primary view-sales-btn" data-target="#view_sale" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>
                                </button>
                                <button class="btn btn-xs btn-success edit-sales-btn" data-target="#edit_sale" data-toggle="modal">
                                    <i class="fa fa-pencil-square-o">&nbsp;</i>
                                </button>
<!--                                <button class="btn btn-xs btn-danger delete-sales-btn" data-target="#delete_sale" data-toggle="modal">-->
<!--                                    <i class="fa fa-trash-o">&nbsp;</i>Delete-->
<!--                                </button>-->
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>

                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".edit-sales-btn").click(function () {
        var sales_invoice = $(this).closest("tr").find(".sales_invoice").text();
        // get needed html
        $.get("<?= base_url('admin/sales/view_edit/')?>" + sales_invoice, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_sales').modal(/* options object here*/);
            $('#edit_sales').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/sales')?>";
            });
        });
    });

    $(".view-sales-btn").click(function () {
        var sales_invoice = $(this).closest("tr").find(".sales_invoice").text();
        // get needed html
        $.get("<?= base_url('admin/sales/view/')?>" + sales_invoice, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_sales').modal(/* options object here*/);
            $('#view_sales').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/sales')?>";
            });
        });
    });
</script>

