<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;EMPLOYEES</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh
            </button>
            <button class="btn btn-info btn-md pull-right add-emp-btn" data-target="#add_employee" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> New Employee
            </button>
            <form action="<?= base_url('admin/employees/emp_print')?>" target="_blank">
                <button class="btn btn-danger btn-md pull-right print-btn" style="font-size: small; font-weight: bold">
                    <i class="fa fa-lg fa-print"></i> Print
                </button>
            </form>
        </div>
    </div><hr/>
    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                        <tr>
                            <th>ID</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>GENDER</th>
                            <th>PHONE #</th>
                            <th>E-MAIL</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($employees as $employee):?>
                            <tr>
                                <td class="employee_id"><?= $employee->person_id?></td>
                                <td> <?= $employee->first_name?></td>
                                <td><?= $employee->last_name?></td>
                                <?php if($employee->gender == 0):?>
                                <td>Female</td>
                                <?php elseif($employee->gender == 1):?>
                                <td>Male</td>
                                <?php endif;?>
                                <td><?= $employee->phone_number_1?></td>
                                <td><?= $employee->email?></td>
                                <td>
                                    <button class="btn btn-xs btn-primary view-emp-btn" data-target="#view_employee" data-toggle="modal">
                                        <i class="fa fa-eye">&nbsp;</i>View
                                    </button>

                                    <button class="btn btn-xs btn-success edit-emp-btn" data-target="#edit_employee" data-toggle="modal">
                                        <i class="fa fa-pencil-square-o">&nbsp;</i>Edit
                                    </button>
                                    <?php if($employee->status == 1 && $employee->username == $username):?>
                                            <button id="deactivate" class="btn btn-xs btn-danger" disabled>
                                                <i class="fa fa-trash-o">&nbsp;</i>Disable
                                            </button>
                                    <?php elseif($employee->status == 1 && $employee->username != $username):?>
                                        <button id="deactivate" class="btn btn-xs btn-danger deactivate">
                                            <i class="fa fa-trash-o">&nbsp;</i>Disable
                                        </button>
                                    <?php elseif($employee->status == 0):?>
                                        <?php if(!empty($employee->username) && !empty($employee->password)):?>
                                            <button id="activate" class="btn btn-xs btn-default activate">
                                                <i class="fa fa-user">&nbsp;</i>Enable
                                            </button>
                                        <?php else:?>
                                            <button id="Set" class="btn btn-xs btn-default ui-selectmenu-text">
                                                <i class="fa fa-user">&nbsp;</i>Set Acc
                                            </button>
                                        <?php endif;?>
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(".add-emp-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/employees/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_employee').modal(/* options object here*/);
            $('#add_employee').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/employees/')?>";
            });
        });
    });
    $(".edit-emp-btn").click(function () {
            var employee_id = $(this).closest("tr").find(".employee_id").text();
        // get needed html
        $.get("<?= base_url('admin/employees/view/')?>" + employee_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_employee').modal(/* options object here*/);
            $('#edit_employee').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/employees/')?>";
            });
        });
    });

    $(".view-emp-btn").click(function () {
        var employee_id = $(this).closest("tr").find(".employee_id").text();
        // get needed html
        $.get("<?= base_url('admin/employees/view/')?>" + employee_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_employee').modal(/* options object here*/);
            $('#view_employee').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/employees/')?>";
            });
        });
    });
    $(".activate").click(function () {
        var employee_id = $(this).closest("tr").find(".employee_id").text();

        swal({
                title: "Confirm",
                text: "Do want to activate this account?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-success',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.get("<?= base_url('admin/employees/activate_account/')?>" + employee_id, function (result) {
                        res = $.parseJSON(result);
                        if (res.success === true) {
                            swal('Success!', 'Account has been activated', 'success');
                            window.setTimeout(function () {
                                location.reload();
                            }, 3000);
                        } else {
                            swal('Failed!', 'Account not activated', 'error');
                            window.setTimeout(function () {
                                window.location.href = "<?= base_url('admin/employees/')?>";
                            }, 3000);
                        }
                    });
                }
            }
        )
    });

    $(".deactivate").click(function () {
        var employee_id = $(this).closest("tr").find(".employee_id").text();

        swal({
                title: "Confirm",
                text: "Do you want to Deactivate this account?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-success',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.get("<?= base_url('admin/employees/deactivate_account/')?>" + employee_id, function (result) {
                        res = $.parseJSON(result);
                        if (res.success === true) {
                            swal('Success!', 'Account has been activated', 'success');
                            window.setTimeout(function () {
                                location.reload();
                            }, 3000);
                        } else {
                            swal('Failed!', 'Account not activated', 'error');
                            window.setTimeout(function () {
                                window.location.href = "<?= base_url('admin/employees/')?>";
                            }, 3000);
                        }
                    });
                }
            }
        )
    });
</script>
