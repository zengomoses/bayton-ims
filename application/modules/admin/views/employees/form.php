<?php
    if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
        $name = ($this->session->userdata['logged_in']['name']);
        $gender = ($this->session->userdata['logged_in']['gender']);
        $username = ($this->session->userdata['logged_in']['username']);
        $user_id = ($this->session->userdata['logged_in']['id']);
    } else {
        header("location:". base_url('auth')."");
    }
?>
<style>
    th{
        max-width: 100px;
        font-style: italic;
    }
    .mypanel{
        color: #0f0f0f;
    }
</style>
<div class="modal fade" id="add_employee">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Employee</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('', array('id'=>'employee_form'));?>
                    <ul class="nav nav-tabs" style="font-size: 16px;">
                        <li class="active"><a href="#employee_basic_info" data-toggle="tab">Basic Information</a></li>
                        <li><a href="#employee_login_info" data-toggle="tab">Login Information</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="employee_basic_info" style="text-align: center; background-color: #e2e2e2">
                            <hr>
                            <fieldset id="employee_basic_info">
                                <?php $this->load->view("people/form_basic_info"); ?>
                            </fieldset>
                        </div>
                        <div class="tab-pane" id="employee_login_info" style="text-align: center; background-color: #e2e2e2">
                            <hr>
                            <fieldset id="employee_login_info">
                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Username', 'username', array('class'=>'required control-label col-xs-3')); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_input(array(
                                            'name'=>'username',
                                            'id'=>'username',
                                            'class'=>'form-control input-sm',)
                                        );?>
                                    </div>
                                </div>

                                <?php $password_label_attributes = $person_info->person_id == "" ? array('class'=>'required') : array(); ?>

                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Password', 'password', array_merge($password_label_attributes, array('class'=>'control-label col-xs-3'))); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_password(array(
                                                'name'=>'password',
                                                'id'=>'password',
                                                'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                </div>

                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Confirm Password', 'confirm_password', array_merge($password_label_attributes, array('class'=>'control-label col-xs-3'))); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_password(array(
                                            'name'=>'confirm_password',
                                            'id'=>'confirm_password',
                                            'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                </div>

                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Access Level', 'level', array('class'=>'control-label col-xs-3')); ?>
                                    <div class='col-xs-8'>
                                         <select id="level" name="level" class="form-control input-sm">
                                            <option selected disabled>Select Access Level</option><option value="admin">Administrator</option>
                                            <option value="sales">Sales Personnel</option>
                                            <option value="chief">Chief Employee</option>
                                         </select>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div><!-- tab content -->
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="submitbutton">Save</button>
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_employee">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Employee Details</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'edit_employee_form'));?>
                <ul class="nav nav-tabs" style="font-size: 16px";>
                    <li class="active"><a href="#edit_employee_basic_info" data-toggle="tab">Basic Information</a></li>
                    <li><a href="#edit_employee_login_info" data-toggle="tab">Login Information</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="edit_employee_basic_info" style="text-align: center; background-color: #e2e2e2">
                        <fieldset id="employee_basic_info">
                           <hr>
                            <?php $this->load->view("people/form_edit_basic_info"); ?>
                        </fieldset>
                    </div>
                    <div class="tab-pane" id="edit_employee_login_info" style="text-align: center; background-color: #e2e2e2">
                        <fieldset id="employee_login_info">
                            <hr>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Username', 'username', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                        <?php echo form_input(array(
                                            'name'=>'username',
                                            'id'=>'username',
                                            'class'=>'form-control input-sm',
                                            'value'=>$person_info->username)
                                        );?>
                                </div>
                            </div>

                            <?php $password_label_attributes = $person_info->person_id == "" ? array('class'=>'required') : array(); ?>

                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Password', 'password', array_merge($password_label_attributes, array('class'=>'control-label col-xs-3'))); ?>
                                <div class='col-xs-8'>
                                        <?php echo form_password(array(
                                            'name'=>'password',
                                            'id'=>'password',
                                            'class'=>'form-control input-sm')
                                        );?>
                                </div>
                            </div>

                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Confirm Password', 'confirm_password', array_merge($password_label_attributes, array('class'=>'control-label col-xs-3'))); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_password(array(
                                        'name'=>'confirm_password',
                                        'id'=>'confirm_password',
                                        'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>

                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Access Level', 'level', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <select id="level" name="level" class="form-control input-sm">
                                        <?php if($person_info->level == "admin" ):?>
                                            <option disabled>Select Access Level</option>
                                            <option value="admin" selected>Administrator</option>
                                            <option value="sales">Sales Personnel</option>
                                            <option value="chief">Chief Employee</option>
                                        <?php endif;?>
                                        <?php if($person_info->level == "sales" ):?>
                                            <option disabled>Select Access Level</option>
                                            <option value="admin">Administrator</option>
                                            <option value="sales" selected>Sales Personnel</option>
                                            <option value="chief">Chief Employee</option>
                                        <?php endif;?>
                                        <?php if($person_info->level == "chief" ):?>
                                            <option disabled>Select Access Level</option>
                                            <option value="admin">Administrator</option>
                                            <option value="sales">Sales Personnel</option>
                                            <option value="chief" selected>Chief Employee</option>
                                        <?php endif;?>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div><!-- tab content -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="updatebutton">Update</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#employee_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                first_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter first name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                last_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter last name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: 'The gender is required'
                        }
                    }
                },
                email: {
                    validators: {
                        emailAddress: {
                            message: 'Please enter valid email address'
                        }
                    }
                },

                phone_number_1: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter phone number'
                        },
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                phone_number_2: {
                    validators: {
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                phone_number_3: {
                    validators: {
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                address: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },
                city: {
                    validators: {
                        stringLength: {
                            min: 4
                        },
                    }
                },
                region: {
                    validators: {
                        notEmpty: {
                            message: 'Please select Region'
                        }
                    }
                },
                district: {
                    validators: {
                        notEmpty: {
                            message: 'Please select District'
                        }
                    }
                },
                comments: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 200,
                            message:'Please enter at least 2 characters and no more than 200'
                        }
                    }
                },

                username: {
                    validators: {
                        stringLength: {
                            min: 4,
                            max: 8,
                            message: "The username should be 4-8 characters long"
                        },
                        stringCase: {
                            message: 'The username must be in lower',
                            'case': 'lower'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: 'The username can only consist of alphabetical, number, dot and underscore'
                        },
                        notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        },
                    }
                },

                password: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        },
                        identical: {
                            field: 'confirmPassword',
                            message: 'Password does not match'
                        },

                        callback: {
                            callback: function (value, validator) {
                                // Check the password strength
                                if (value.length < 6) {
                                    return {
                                        valid: false,
                                        message: 'The password must be more than 6 characters'
                                    }
                                }
                                if (value === value.toLowerCase()) {
                                    return {
                                        valid: false,
                                        message: 'The password must contain at least one upper case character'
                                    }
                                }
                                if (value === value.toUpperCase()) {
                                    return {
                                        valid: false,
                                        message: 'The password must contain at least one lower case character'
                                    }
                                }
                                if (value.search(/[0-9]/) < 0) {
                                    return {
                                        valid: false,
                                        message: 'The password must contain at least one digit'
                                    }
                                }
                                return true;
                            }
                        }
                    }
                },

                confirm_password: {
                    validators: {
                        notEmpty: {
                            message: 'The confirm password is required and can\'t be empty'
                        },
                        identical: {
                            field: 'password',
                            message: 'Password does not match'
                        }
                    }
                },

                level: {
                    validators: {
                        notEmpty: {
                            message: 'You must select the User Access level'
                        }
                    }
                }

            }
        });

        $("#submitbutton").click(function(e){  // passing down the event
            var validator = $('#employee_form').data('bootstrapValidator');
            var  data = $("#employee_form").find(":input").filter(
                function(index, element){
                    return $(element).val() !== '';
                }
            ).serialize();
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/employees/save/')?>',
                    type: 'POST',
                    data: data,
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Employee has been added successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){
//                    location.reload();
                } ,3000);
            }
        });
    });


    $(document).ready(function() {
        $('#edit_employee_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                first_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter first name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                last_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter last name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: 'The gender is required'
                        }
                    }
                },
                email: {
                    validators: {
                        emailAddress: {
                            message: 'Please enter valid email address'
                        }
                    }
                },
                phone_number: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter phone number'
                        },
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                address: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },
                city: {
                    validators: {
                        stringLength: {
                            min: 4
                        }
                    }
                },
                state: {
                    validators: {
                        notEmpty: {
                            message: 'Please select state'
                        }
                    }
                },

                comments: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 200,
                            message:'Please enter at least 2 characters and no more than 200'
                        }
                    }
                },

                username: {
                    validators: {
                        stringLength: {
                            min: 4,
                            max: 8,
                            message: "The username should be 4-8 characters long"
                        },
                        stringCase: {
                            message: 'The username must be in lower',
                            'case': 'lower'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: 'The username can only consist of alphabetical, number, dot and underscore'
                        },
                        notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        }
                    }
                },

                password: {
                    validators: {
                        identical: {
                            field: 'confirmPassword',
                            message: 'Password does not match'
                        },

                        callback: {
                            callback: function (value, validator) {
                                // Check the password strength
                                if (value.length < 6) {
                                    return {
                                        valid: false,
                                        message: 'The password must be more than 6 characters'
                                    }
                                }
                                if (value === value.toLowerCase()) {
                                    return {
                                        valid: false,
                                        message: 'The password must contain at least one upper case character'
                                    }
                                }
                                if (value === value.toUpperCase()) {
                                    return {
                                        valid: false,
                                        message: 'The password must contain at least one lower case character'
                                    }
                                }
                                if (value.search(/[0-9]/) < 0) {
                                    return {
                                        valid: false,
                                        message: 'The password must contain at least one digit'
                                    }
                                }
                                return true;
                            }
                        }
                    }
                },

                confirm_password: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Password does not match'
                        }
                    }
                },

                level: {
                    validators: {
                        notEmpty: {
                            message: 'You must select the User Access level'
                        }
                    }
                }

            }
        });

        $("#updatebutton").click(function(e){  // passing down the event
            var validator = $('#edit_employee_form').data('bootstrapValidator');
            validator.validate();
            var  data = $("#edit_employee_form").find(":input").filter(
                function(index, element){
                    return $(element).val() !== '';
                }
            ).serialize();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/employees/update/').$person_info->person_id?>',
                    type: 'POST',
                    data: data,
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Employee has been updated successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
            e.preventDefault(); // could also use: return false;
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){
//                    location.reload();
                } ,3000);
            }
        });
    });
</script>

<div class="modal fade" id="view_employee">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">View Employee</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="panel panel-default mypanel">
                       <div class="panel-heading">
                           <h3 class="panel-title">Employee Details</h3>
                       </div>
                       <div class="panel-body">
                           <div class="row">
                               <div class="col-md-8">
                                   <table class="table table-responsive table-bordered table-striped">
                                       <tbody>
                                           <tr>
                                               <th>First Name</th>
                                               <td><?= $person_info->first_name; ?></td>
                                           </tr>
                                           <tr>
                                               <th>Last Name</th>
                                               <td><?= $person_info->last_name; ?></td>
                                           </tr>
                                           <tr>
                                               <th>Gender</th>
                                               <?php if($person_info->gender == 1):?>
                                                    <td>Male</td>
                                               <?php endif;?>
                                               <?php if($person_info->gender == 0):?>
                                                    <td>Female</td>
                                               <?php endif;?>
                                           </tr>
                                           <tr>
                                               <th>Contact Details</th>
                                               <td>
                                                   Email:<?= $person_info->email; ?><br>
                                                   Phone Number:<?= $person_info->phone_number_1?>
                                                   <?php if($person_info->phone_number_2 != NULL):?>
                                                       <?= ', '.$person_info->phone_number_2?><br>
                                                   <?php endif;?>
                                                   <?php if($person_info->phone_number_3 != NULL):?>
                                                       <?= ', '.$person_info->phone_number_3?><br>
                                                   <?php endif;?>
                                               </td>
                                           </tr>

                                           <tr>
                                               <th>Account Status</th>
                                               <?php if($person_info->status == 1):?>
                                                    <td>Active</td>
                                               <?php else:?>
                                                    <td>Inactive</td>
                                               <?php endif;?>
                                           </tr>
                                       </tbody>
                                   </table>
                               </div>
                               <div class="col-md-4">
                                   <?php if($person_info->gender == 1):?>
                                       <img src="<?= base_url() ?>assets/images/default/male.jpeg" class="img-responsive img-thumbnail" alt="">
                                   <?php endif;?>
                                   <?php if($person_info->gender == 0):?>
                                       <img src="<?= base_url() ?>assets/images/default/female.jpeg" class="img-responsive img-thumbnail" alt="">
                                   <?php endif;?>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
           </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit" id="submitButton">Save</button>
            <button class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
