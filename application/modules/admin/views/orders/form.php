<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    .modal-dialog{
        width: 800px;
    }
</style>
<div class="modal fade" id="view_order">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--           modal header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Order Request Details</h4>
            </div>
            <!--                 modal body-->
            <div class="modal-body">
                <input id="employee_id" name="employee_id" value="<?= $user_id?>" hidden>
                <div class="row">
                    <div class="panel panel-default mypanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class='col-xs-8' hidden>
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'stock_id',
                                                'id'=>'stock_id',
                                                'class'=>'form-control input-md',
                                                'value'=> time() ,
                                                'disabled' => 'true')
                                        );?>
                                    </div>
                                    <table class="table table-responsive table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th>ORDER #</th>
                                                <td><?= $order_info->ref_number?></td>
                                            </tr>
                                            <tr>
                                                <th>DATE REQUESTED</th>
                                                <td><?= $order_info->order_date ?></td>
                                            </tr>
                                            <tr>
                                                <th>ORDERED BY</th>
                                                <?php foreach ($employees as $employee):?>
                                                    <?php if($order_info->ordered_by == $employee->id):?>
                                                        <td><?= ucwords($employee->username) .' ['. $employee->first_name.' '.$employee->last_name.']';?></td>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </tr>
                                            <tr>
                                                <th>STATUS</th>
                                                <td><?= ucwords($order_info->status)?></td>
                                            </tr>
                                            <?php if($order_info->status == 'Processed'):?>
                                            <tr>
                                                <th>DATE PROCESSED</th>
                                                <td><?= $order_info->processed_date ?></td>
                                            </tr>
                                            <tr>
                                                <th>PROCESSED BY</th>
                                                <?php foreach ($employees as $employee):?>
                                                    <?php if($order_info->ordered_by == $employee->id):?>
                                                        <td><?= $employee->username;?></td>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </tr>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <table id="order_items" class="table table-responsive table-bordered table-striped">
                                        <thead>
                                        <th>ITEM ID</th>
                                        <th>ITEM NAME</th>
                                        <th>QTY</th>
                                        <th>STORE</th>
                                        </thead>
                                        <tbody>
                                        <?php foreach($order_items as $order):?>
                                            <tr>
                                                <td><?= $order->item ?></td>
                                                <?php foreach ($items as $item):?>
                                                    <?php if($order->item == $item->id):?>
                                                        <td><?=$item->item_name." ".$item->description?></td>
                                                    <?php endif?>
                                                <?php endforeach;?>
                                                <td><?= $order->quantity ?></td>
                                                <td hidden><?= $order->store ?></td>
                                                <?php foreach ($stores as $store):?>
                                                   <?php if($order->store == $store->id):?>
                                                        <td><?=$store->name?></td>
                                                    <?php endif?>
                                                <?php endforeach;?>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php if($order_info->status == 'Pending'):?>
                    <button class="btn btn-success" id="btn-approve">Approve Order</button>
                    <button class="btn btn-danger" id="btn-reject">Reject Order</button>
                <?php endif;?>
                <?php if($order_info->status == 'Approved'):?>
                    <button class="btn btn-success" id="btn-process">Process Order</button>
<!--                    <button class="btn btn-warning" id="btn-revoke">Revoke Order</button>-->
                <?php endif;?>
                <?php if($order_info->status == 'Processed'):?>
<!--                    <button class="btn btn-warning" id="btn-revoke">Revoke Order</button>-->
                <?php endif;?>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
    $(function(){

        $("#btn-approve").on("click", function () {
            var employee_id = $('#employee_id').val();
            var stock_date = '<?= date('Y-m-d H:m:s');?>';
            var order_info = {
                'status': 'Approved',
                'approved_by': employee_id,
                'approval_date': stock_date
            }

            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "<?php echo base_url('admin/orders/approve/') . $order_info->id; ?>",
                data: {order_info: order_info},

                success: function (response) {
                    if (response === "true") {
                        swal('Submitted!', 'Order has been successfully approved ', 'success');

                    }else if(response === "false"){
                        swal('Error!', 'Something went wrong. Please Try again', 'error');
                    }
                }

            });
        });

        $("#btn-reject").on("click", function () {
            var employee_id = $('#employee_id').val();
            var stock_date = '<?= date('Y-m-d H:m:s');?>';
            var order_info = {
                'status': 'Rejected'
                // 'approved_by': employee_id,
                // 'approval_date': stock_date
            }

            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "<?php echo base_url('admin/orders/reject/') . $order_info->id; ?>",
                data: {order_info: order_info},

                success: function (response) {
                    console.log(response);
                    if (response) {
                        swal('Submitted!', 'Order has been rejected ', 'success');

                    }else if(response === "false"){
                        swal('Error!', 'Something went wrong. Please Try again', 'error');
                    }
                }

            });
        });

        $("#btn-revoke").on("click", function () {
            var employee_id = $('#employee_id').val();
            var revoke_date = '<?= date('Y-m-d H:m:s');?>';
            var order_info = {
                'status': 'Cancelled'
                // 'approved_by': employee_id,
                // 'approval_date': revoke_date
            }

            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "<?php echo base_url('admin/orders/revoke/') . $order_info->id; ?>",
                data: {order_info: order_info},

                success: function (response) {
                    if (response === "true") {
                        swal('Submitted!', 'Order has been revoked ', 'success');

                    }else if(response === "false"){
                        swal('Error!', 'Something went wrong. Please Try again', 'error');
                    }
                }

            });
        });

        $("#btn-process").click(function(e) {  // passing down the event
            var employee_id = $('#employee_id').val();
            var stock_id = $('#stock_id').val();
            var stock_date = '<?= date('Y-m-d H:m:s');?>';
            var stock_info = {
                'employee_id': employee_id,
                'stock_id': stock_id,
                'stock_date': stock_date
            };

            var order_info = {
                'status': 'Processed',
                'ref_number': stock_id,
                'processed_by': employee_id,
                'processed_date': stock_date
            }

            var TableData = new Array();
            $('#order_items tr').each(function (index, tr) {

                TableData[index - 1] = {
                    'stock_id': stock_id,
                    'item_id': $(tr).find("td:eq(0)").text(),
                    'quantity': $(tr).find("td:eq(2)").text(),
                    'store': $(tr).find("td:eq(3)").text()
                }
            });
            var order_items = JSON.stringify([TableData]);
            order_items = order_items.substring(0, order_items.length - 1).substring(1, order_items.length);
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "<?php echo base_url('admin/orders/process/') . $order_info->id; ?>",
                data: {order_items: order_items, stock_info: stock_info, order_info: order_info},

                success: function (response) {
                    if (response === "true") {
                        swal('Submitted!', 'Order has been successfully Processed ', 'success');

                    }else if(response === "false"){
                        swal('Error!', 'Something went wrong. Please Try again', 'error');
                    }
                }

            });
        });
    });
</script>





