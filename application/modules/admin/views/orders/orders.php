<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;ORDERS</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">

            <div class="row">
                <div class="col col-md-12">
                    <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>REF #</th>
                        <th>ORDERED BY</th>
                        <th>ORDER DATE</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($orders as $order):?>
                        <tr>
                            <td><?= $order->id?></td>
                            <td class="order_id"><?= $order->ref_number?></td>
                            <?php foreach ($employees as $employee):?>
                                <?php if( $employee->id == $order->ordered_by):?>
                                    <td><?= $employee->username;?></td>
                                <?php endif;?>
                            <?php endforeach;?>
                            <td><?= $order->order_date?></td>
                            <td><?= ucwords($order->status)?></td>
                            <td>
                                <button class="btn btn-xs btn-primary view-orders-btn" data-target="#view_order" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>View
                                </button>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>

                </table>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(".edit-orders-btn").click(function () {
        var orders_id = $(this).closest("tr").find(".orders_id").text();
        // get needed html
        $.get("<?= base_url('admin/orders/view_edit/')?>" + orders_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_orders').modal(/* options object here*/);
            $('#edit_orders').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/orders')?>";
            });
        });
    });

    $(".view-orders-btn").click(function () {
        var order_id = $(this).closest("tr").find(".order_id").text();
        // get needed html
        $.get("<?= base_url('admin/orders/view/')?>" + order_id, function (result) {
        //   // append response to body
            $('body').append(result);
        //   // open modal
            $('#view_order').modal(/* options object here*/);
            $('#view_order').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/orders')?>";
            });
        });
    });
</script>

