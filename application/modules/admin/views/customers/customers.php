<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
    if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
        $name = ($this->session->userdata['logged_in']['name']);
        $gender = ($this->session->userdata['logged_in']['gender']);
        $username = ($this->session->userdata['logged_in']['username']);
        $user_id = ($this->session->userdata['logged_in']['id']);
    } else {
        header("location:". base_url('auth')."");
    }
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;CUSTOMERS</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
            <button class="btn btn-info btn-md pull-right add-customer-btn" data-target="#add_customer" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> New Customer
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                        <tr>
                            <th>ID</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>GENDER</th>
                            <th>PHONE #</th>
                            <th>E-MAIL</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($customers as $customer):?>
                        <tr>
                            <td class="customer_id"><?= $customer->person_id?></td>
                            <td><?= $customer->first_name?></td>
                            <td><?= $customer->last_name?></td>
                            <?php if($customer->gender == 0):?>
                                <td>Female</td>
                            <?php elseif($customer->gender == 1):?>
                                <td>Male</td>
                            <?php endif;?>
                            <td><?= $customer->phone_number_1?></td>
                            <td><?= $customer->email?></td>
                            <td>
                                <button class="btn btn-xs btn-primary view-customer-btn" data-target="#view_customer" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>View
                                </button>
                                <button class="btn btn-xs btn-success edit-customer-btn" data-target="#edit_customer" data-toggle="modal">
                                    <i class="fa fa-pencil-square-o">&nbsp;</i>Edit
                                </button>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".add-customer-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/customers/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_customer').modal(/* options object here*/);
            $('#add_customer').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/customers/')?>";
            });
        });
    });
    $(".edit-customer-btn").click(function () {
        var customer_id = $(this).closest("tr").find(".customer_id").text();
        // get needed html
        $.get("<?= base_url('admin/customers/view/')?>" + customer_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_customer').modal(/* options object here*/);
            $('#edit_customer').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/customers/')?>";
            });
        });
    });

    $(".view-customer-btn").click(function () {
        var customer_id = $(this).closest("tr").find(".customer_id").text();
        // get needed html
        $.get("<?= base_url('admin/customers/view/')?>" + customer_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_customer').modal(/* options object here*/);
            $('#view_customer').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/customers/')?>";
            });
        });
    });
</script>
