<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;STORES</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
            <button class="btn btn-info btn-md pull-right add-store-btn" data-target="#add_store" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> Add Store
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped ">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                    <tr>
                        <th>ID</th>
                        <th>STORE NAME</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($stores as $store):?>
                        <tr>
                            <td class="store_id"><?= $store->id?></td>
                            <td><?= $store->name?></td>
                            <td>
                                <button class="btn btn-xs btn-primary view-store-btn" data-target="#view_store" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>View
                                </button>
                                <button class="btn btn-xs btn-success edit-store-btn" data-target="#edit_store" data-toggle="modal">
                                    <i class="fa fa-pencil-square-o">&nbsp;</i>Edit
                                </button>
                            </td>
                        </tr>
                    <?php endforeach ;?>
                    </tbody>

                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".add-store-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/stores/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_store').modal(/* options object here*/);
            $('#add_store').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/stores/stores')?>";
            });
        });
    });
    $(".edit-store-btn").click(function () {
        var store_id = $(this).closest("tr").find(".store_id").text();
        // get needed html
        $.get("<?= base_url('admin/stores/view/')?>" + store_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_store').modal(/* options object here*/);
            $('#edit_store').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/stores/stores')?>";
            });
        });
    });

    $(".view-store-btn").click(function () {
        var store_id = $(this).closest("tr").find(".store_id").text();
        // get needed html
        $.get("<?= base_url('admin/stores/view/')?>" + store_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_store').modal(/* options object here*/);
            $('#view_store').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/stores/stores')?>";
            });
        });
    });
</script>

