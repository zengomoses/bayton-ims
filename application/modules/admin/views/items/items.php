<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;ITEMS</i>
        </div>
        <div class="col-md-6">
             <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh
            </button>
            <button class="btn btn-info btn-md pull-right add-item-btn" data-target="#add_item" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> New Item
            </button>
            <form action="<?= base_url('admin/items/items_list')?>" target="_blank">
                <button class="btn btn-danger btn-md pull-right print-btn" style="font-size: small; font-weight: bold">
                    <i class="fa fa-lg fa-print"></i> Print
                </button>
            </form>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condense">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                        <tr>
                            <th>ID</th>
                            <th>ITEM</th>
                            <th>BRAND</th>
                            <th>CATEGORY</th>
                            <th>S/PRICE</th>
                            <th>B/PRICE</th>
                            <th>STATUS</th>
                            <th width="60px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($items as $item):?>
                        <tr>
                            <td class="item_id"><?= $item->id?></td>
                            <td><?= $item->item_name." ".$item->description;?></td>
                            <td><?= $item->brand;?></td>
                            <?php foreach ($categories as $category): ;?>
                                <?php if($category->id == $item->category_id ): ;?>
                                    <td><?= $category->name?></td>
                                <?php endif ;?>
                            <?php endforeach ;?>
                            <td><?= $item->unit_selling_price?></td>
                            <td><?= $item->buying_price?></td>
                            <?php if($item->quantity_in == 0):?>
                                <td><span  style="background-color: red; color: #eeeeee; padding: 1px; border-radius: 2px">Out Stock</span></td>
                            <?php elseif ($item->quantity_in > 0 && $item->quantity_in < 10):;?>
                                <td><span  style="background-color: #2b542c; color: #eeeeee; padding: 1px; border-radius: 2px">Low Stock</span></td>
                            <?php else:;?>
                                <td><span>In Stock</span></td>
                            <?php endif;?>
                            <td>
                                <button class="btn btn-xs btn-primary view-item-btn" data-target="#view_item" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>
                                </button>
                                <button class="btn btn-xs btn-success edit-item-btn" data-target="#edit_item" data-toggle="modal">
                                    <i class="fa fa-pencil-square-o">&nbsp;</i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach ;?>
                    </tbody>

                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".add-item-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/items/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_item').modal(/* options object here*/);
            $('#add_item').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/items')?>";
            });
        });
    });
    $(".edit-item-btn").click(function () {
        var item_id = $(this).closest("tr").find(".item_id").text();
        // get needed html
        $.get("<?= base_url('admin/items/view/')?>" + item_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_item').modal(/* options object here*/);
            $('#edit_item').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/items')?>";
            });
        });
    });

    $(".view-item-btn").click(function () {
        var item_id = $(this).closest("tr").find(".item_id").text();
        // get needed html
        $.get("<?= base_url('admin/items/view/')?>" + item_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_item').modal(/* options object here*/);
            $('#view_item').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/items')?>";
            });
        });
    });
</script>

