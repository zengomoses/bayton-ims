<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-1">
            <i class="fa fa-2x fa-home">&nbsp;STOCK/<small>Levels</small></i>
        </div>
        <div class="col-md-8">
            <a href="<?= base_url('admin') ?>" class="btn btn-info btn-md pull-right active" style="font-size: small; font-weight: bold;">
                <i class="fa fa-lg fa-book"></i> View Stock Levels
            </a>
            <button class="btn btn-success btn-md pull-right get-stock-btn" data-target="#get_stock" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> Take Stock
            </button>
            <button class="btn btn-info btn-md pull-right add-stock-btn" data-target="#add_stock" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> Add New Stock
            </button>
        </div>
        <div class="col-md-3">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <form action="<?= base_url('admin/stocks/stock_list')?>" target="_blank">
            <button class="btn btn-danger btn-md pull-right print-btn" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-print"></i> Print
            </button>
        </form>
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                        <tr>
                            <th>ID</th>
                            <th>PRODUCT NAME</th>
                            <th>BRAND</th>
                            <th>AVAILABLE</th>
                            <th>STOCK LEVEL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($items as $item):?>
                            <tr>
                                <td><?= $item->id ?></td>
                                <td><?= $item->item_name." ".$item->description ?></td>
                                <td><?= $item->brand ?></td>
                                <td><?= $item->quantity_in ?></td>
                                <td>
                                    <?php if($item->quantity_in < $item->min_quantity): ?>
                                        <i style="color: red; font-weight:bolder">Danger</i>
                                    <?php elseif($item->quantity_in < ($item->min_quantity + ($item->min_quantity * 0.5)) && $item->quantity_in > $item->min_quantity): ?>
                                        <i style="color: blue; font-weight:bolder">Reorder</i>
                                    <?php else:?>
                                        <i style="font-weight:bolder">High</i>
                                    <?php endif;?>
                            </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(".add-stock-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/stocks/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_stock').modal(/* options object here*/);
            $('#add_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock/')?>";
            });
        });
    });


    $(".get-stock-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/stocks/view_get/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#get_stock').modal(/* options object here*/);
            $('#get_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock')?>";
            });
        });
    });
    $(".edit-stock-btn").click(function () {
        var stock_id = $(this).closest("tr").find(".stock_id").text();
        // get needed html
        $.get("<?= base_url('admin/stocks/view_edit/')?>" + stock_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_stock').modal(/* options object here*/);
            $('#edit_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock')?>";
            });
        });
    });

    $(".view-stock-btn").click(function () {
        var stock_id = $(this).closest("tr").find(".stock_id").text();
        // get needed html
        $.get("<?= base_url('admin/stocks/view/')?>" + stock_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_stock').modal(/* options object here*/);
            $('#view_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock')?>";
            });
        });
    });
</script>


