<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    #view_dialog{
        width: 800px;
    }
    th{
        max-width: 100px;
        font-style: italic;
    }
    .mypanel{
        color: #0f0f0f;
    }
</style>
<div class="modal fade" id="add_store">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New store</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'store_form'));?>
                <div class="store-info">
                    <fieldset id="store-info">
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Store Name', 'store_name', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'store_name',
                                        'id'=>'store_name',
                                        'class'=>'form-control input-sm',)
                                );?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="submitbutton">Save</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_store">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit store</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div id="message"></div>
                <?php echo form_open('',array('id'=>'edit_store_form'));?>
                <div class="store-info">
                    <fieldset id="store-info">
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Store Name', 'store_name', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'store_name',
                                        'id'=>'store_name',
                                        'class'=>'form-control input-sm',
                                        'value' => $store_info->name,
                                        'required'=>TRUE,)
                                );?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="updatebutton">Update</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#store_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                store_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter store name'
                        }
                    }
                }
            }
        });

        $("#submitbutton").click(function(e){  // passing down the event
            var validator = $('#store_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/stores/save/')?>',
                    type: 'POST',
                    data: $("#store_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'store has been added successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });

        $('#edit_store_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                store_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter store name'
                        }
                    }
                }
            }
        });

        $("#updatebutton").click(function(e){  // passing down the event
            var validator = $('#edit_store_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/stores/update/').$store_info->id?>',
                    type: 'POST',
                    data: $("#edit_store_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'store has been update successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    });
</script>

<div class="modal fade" id="view_store">
    <div class="modal-dialog" id="view_dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Store</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-responsive table-bordered table-striped">
                            <tbody>
                            <tr>
                                <th>Store ID</th>
                                <td class="stock_id"><?= $store_info->id?></td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td><?= $store_info->name ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                            <thead style="background-color: #34495E; color: #f6f6f6">
                            <tr>
                                <th>ID</th>
                                <th>ITEM NAME</th>
                                <th>BRAND</th>
                                <th>DESCRIPTION</th>
                                <th>QUANTITY</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($store_items as $stock):?>
                                <tr>
                                    <td><?= $stock->item_id;?></td>
                                    <td><?= $stock->item_name?></td>
                                    <td><?= $stock->brand?></td>
                                    <td><?= $stock->desc?></td>
                                    <td><?= $stock->quantity_in?></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="submitbutton" type="submit">Save</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
