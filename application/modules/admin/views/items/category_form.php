<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    th{
        max-width: 100px;
        font-style: italic;
    }
    .mypanel{
        color: #0f0f0f;
    }
</style>
<div class="modal fade" id="add_category">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Category</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div id="message"></div>
                <?php echo form_open('',array('id'=>'category_form'));?>
                    <div class="category-info">
                        <fieldset id="category-info">
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Category Name', 'category_name', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'name'=>'category_name',
                                            'id'=>'category_name',
                                            'class'=>'form-control input-sm',)
                                    );?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="submitbutton">Save</button>
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_category">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Category</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div id="message"></div>
                <?php echo form_open('',array('id'=>'edit_category_form'));?>
                <div class="category-info">
                    <fieldset id="category-info">
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Category Name', 'category_name', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'category_name',
                                        'id'=>'category_name',
                                        'class'=>'form-control input-sm',
                                        'value' => $category_info->name,
                                        'required'=>TRUE,)
                                );?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="updatebutton">Update</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#category_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                category_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter category name'
                        }
                    }
                }
            }
        });

        $("#submitbutton").click(function(e){  // passing down the event
            var validator = $('#category_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/categories/save/')?>',
                    type: 'POST',
                    data: $("#category_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Category has been added successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });

        $('#edit_category_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                category_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter category name'
                        }
                    }
                }
            }
        });

        $("#updatebutton").click(function(e){  // passing down the event
            var validator = $('#edit_category_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/categories/update/').$category_info->id?>',
                    type: 'POST',
                    data: $("#edit_category_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Category has been update successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    });
</script>

<div class="modal fade" id="view_category">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Category</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <table class="table table-responsive table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>Category Name</th>
                            <td><?= $category_info->name; ?></td>
                        </tr>
                        <tr>
                            <th>Items</th>
                            <td><?= $category_info->total; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="submitbutton" type="submit">Save</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
</div>