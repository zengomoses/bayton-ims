<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;UNITS</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
            <button class="btn btn-info btn-md pull-right add-unit-btn" data-target="#add_unit" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> Add Unit
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped ">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th width="400px">DESCRIPTION</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($units as $unit):?>
                        <tr>
                            <td class="unit_id"><?= $unit->id?></td>
                            <td><?= $unit->name?></td>
                            <td><?= $unit->description?></td>
                            <td>
                                <button class="btn btn-xs btn-primary view-unit-btn" data-target="#view_unit" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>View
                                </button>
                                <button class="btn btn-xs btn-success edit-unit-btn" data-target="#edit_unit" data-toggle="modal">
                                    <i class="fa fa-pencil-square-o">&nbsp;</i>Edit
                                </button>
                                <button class="btn btn-xs btn-danger delete-units-btn">
                                    <i class="fa fa-trash">&nbsp;</i>Delete
                                </button>

                            </td>
                        </tr>
                    <?php endforeach ;?>
                    </tbody>

                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".add-unit-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/units/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_unit').modal(/* options object here*/);
            $('#add_unit').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/units/units')?>";
            });
        });
    });
    $(".edit-unit-btn").click(function () {
        var unit_id = $(this).closest("tr").find(".unit_id").text();
        // get needed html
        $.get("<?= base_url('admin/units/view/')?>" + unit_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_unit').modal(/* options object here*/);
            $('#edit_unit').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/units/units')?>";
            });
        });
    });

    $(".view-unit-btn").click(function () {
        var unit_id = $(this).closest("tr").find(".unit_id").text();
        // get needed html
        $.get("<?= base_url('admin/units/view/')?>" + unit_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_unit').modal(/* options object here*/);
            $('#view_unit').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/units/units')?>";
            });
        });
    });
    $(".delete-units-btn").click(function (e) {
        var unit_id = $(this).closest("tr").find(".unit_id").text();
        swal({
                title: "Confirm",
                text: "Are you sure you want to delete this Category?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-success',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('admin/units/delete/')?>" + unit_id,
                        success: function (resp) {
                            response = $.parseJSON(resp);
                            if (response.success) {
                                swal('Success!', 'Unit has been deleted successfully', 'success');
                                window.setTimeout(function(){
                                    location.reload();
                                } ,3000);
                            }else{
                                swal('Failed!', 'Something went wrong. Please Try again', 'error');
                                window.setTimeout(function(){
                                    location.reload();
                                } ,3000);
                            }
                        }
                    });
                }
            }
        );
        e.preventDefault;
    });
</script>

