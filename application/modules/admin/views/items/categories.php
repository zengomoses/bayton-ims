<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;CATEGORIES</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
            <button class="btn btn-info btn-md pull-right add-category-btn" data-target="#add_category" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> New Category
            </button>
        </div>
    </div><hr/>
    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                        <tr>
                            <th>ID</th>
                            <th width="600px">CATEGORY NAME</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($categories as $category):?>
                        <tr>
                            <td class="category_id"><?= $category->id?></td>
                            <td><?= $category->name?></td>
                            <td>
                                <button class="btn btn-xs btn-primary view-category-btn" data-target="#view_category" data-toggle="modal">
                                    <i class="fa fa-eye">&nbsp;</i>View
                                </button>
                                <button class="btn btn-xs btn-success edit-category-btn" data-target="#edit_category" data-toggle="modal">
                                    <i class="fa fa-pencil-square-o">&nbsp;</i>Edit
                                </button>
                                <button class="btn btn-xs btn-danger delete-category-btn">
                                    <i class="fa fa-trash">&nbsp;</i>Delete
                                </button>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".add-category-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/categories/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_category').modal(/* options object here*/);
            $('#add_category').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/categories/')?>";
            });
        });
    });
    $(".edit-category-btn").click(function () {
        var category_id = $(this).closest("tr").find(".category_id").text();
        // get needed html
        $.get("<?= base_url('admin/categories/view/')?>" + category_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_category').modal(/* options object here*/);
            $('#edit_category').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/categories/')?>";
            });
        });
    });

    $(".view-category-btn").click(function () {
        var category_id = $(this).closest("tr").find(".category_id").text();
        // get needed html
        $.get("<?= base_url('admin/categories/view/')?>" + category_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_category').modal(/* options object here*/);
            $('#view_category').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/categories/')?>";
            });
        });
    });

    $(".delete-category-btn").click(function (e) {
        var category_id = $(this).closest("tr").find(".category_id").text();
        swal({
                title: "Confirm",
                text: "Are you sure you want to delete this Category?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-success',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('admin/categories/delete/')?>" + category_id,
                        success: function (resp) {
                            response = $.parseJSON(resp);
                            if (response.success) {
                                swal('Success!', 'Category has been deleted successfully', 'success');
                                window.setTimeout(function(){
                                    location.reload();
                                } ,3000);
                            }else{
                                swal('Failed!', 'Something went wrong. Please Try again', 'error');
                                window.setTimeout(function(){
                                    location.reload();
                                } ,3000);
                            }
                        }
                    });
                }
            }
        );
        e.preventDefault;
    });
</script>
