<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    th{
        max-width: 100px;
        font-style: italic;
    }
    .mypanel{
        color: #0f0f0f;
    }
</style>
<div class="modal fade" id="add_unit">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New unit</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'unit_form'));?>
                <div class="unit-info">
                    <fieldset id="unit-info">
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Unit Name', 'unit_name', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'unit_name',
                                        'id'=>'unit_name',
                                        'class'=>'form-control input-sm',)
                                );?>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Description', 'description', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'description',
                                        'id'=>'description',
                                        'class'=>'form-control input-sm')
                                );?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="submitbutton">Save</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_unit">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit unit</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div id="message"></div>
                <?php echo form_open('',array('id'=>'edit_unit_form'));?>
                <div class="unit-info">
                    <fieldset id="unit-info">
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Unit Name', 'unit_name', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'unit_name',
                                        'id'=>'unit_name',
                                        'class'=>'form-control input-sm',
                                        'value' => $unit_info->name)
                                );?>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-xs-12">
                            <?php echo form_label('Description', 'description', array('class'=>'required control-label col-xs-3')); ?>
                            <div class='col-xs-8'>
                                <?php echo form_input(array(
                                        'name'=>'description',
                                        'id'=>'description',
                                        'class'=>'form-control input-sm',
                                        'value' => $unit_info->description)
                                );?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="updatebutton">Update</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#unit_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                unit_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter unit name'
                        }
                    }
                },

                description: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter description'
                        }
                    }
                }
            }
        });

        $("#submitbutton").click(function(e){  // passing down the event
            var validator = $('#unit_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/units/save/')?>',
                    type: 'POST',
                    data: $("#unit_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'unit has been added successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });

        $('#edit_unit_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                unit_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter unit name'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter description'
                        }
                    }
                }
            }
        });

        $("#updatebutton").click(function(e){  // passing down the event
            var validator = $('#edit_unit_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/units/update/').$unit_info->id?>',
                    type: 'POST',
                    data: $("#edit_unit_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Unit has been update successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    });
</script>

<div class="modal fade" id="view_unit">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Unit</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <table class="table table-responsive table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>Unit Name</th>
                            <td><?= $unit_info->name; ?></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><?= $unit_info->description; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="submitbutton" type="submit">Save</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
