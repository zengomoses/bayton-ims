<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-1">
            <i class="fa fa-2x fa-home">&nbsp;STOCK</i>
        </div>
        <div class="col-md-8">
            <a href="<?= base_url('admin/stocks/stock_levels') ?>" class="btn btn-info btn-md pull-right" style="font-size: small; font-weight: bold;">
                <i class="fa fa-lg fa-book"></i> View Stock Levels
            </a>
            <button class="btn btn-success btn-md pull-right get-stock-btn" data-target="#get_stock" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-minus-circle"></i> Take Stock
            </button>
            <button class="btn btn-info btn-md pull-right add-stock-btn" data-target="#add_stock" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> Add New Stock
            </button>
        </div>
        <div class="col-md-3">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <form action="<?= base_url('admin/stocks/stock_list')?>" target="_blank">
            <button class="btn btn-danger btn-md pull-right print-btn" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-print"></i> Print
            </button>
        </form>
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                        <tr>
                            <th>STOCK ID</th>
                            <th>DATE ENTERED</th>
                            <th>EMPLOYEE</th>
                            <th>LOCATION</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php foreach ($stocks as $stock):?>
                            <tr>
                                <td class="stock_id"><?= $stock->stock_id;?></td>
                                <td><?= $stock->stock_date;?></td>
                                <?php foreach ($employees as $employee):?>
                                    <?php if($employee->person_id == $stock->employee_id):?>
                                        <td><?= $employee->first_name.' '.$employee->last_name?></td>
                                    <?php endif;?>
                                <?php endforeach ?>
                                <td><?= $stock->name;?></td>
                                <td>
                                    <button class="btn btn-xs btn-primary view-stock-btn" data-target="#view_stock" data-toggle="modal">
                                        <i class="fa fa-eye">&nbsp;</i>View
                                    </button>
                                    <button class="btn btn-xs btn-success edit-stock-btn" data-target="#edit_stock" data-toggle="modal">
                                        <i class="fa fa-pencil-square-o">&nbsp;</i>Edit
                                    </button>
                                </td>
                            </tr>
                       <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(".add-stock-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/stocks/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_stock').modal(/* options object here*/);
            $('#add_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock/')?>";
            });
        });
    });


    $(".get-stock-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/stocks/view_get/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#get_stock').modal(/* options object here*/);
            $('#get_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock')?>";
            });
        });
    });
    $(".edit-stock-btn").click(function () {
        var stock_id = $(this).closest("tr").find(".stock_id").text();
        // get needed html
        $.get("<?= base_url('admin/stocks/view_edit/')?>" + stock_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#edit_stock').modal(/* options object here*/);
            $('#edit_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock')?>";
            });
        });
    });

    $(".view-stock-btn").click(function () {
        var stock_id = $(this).closest("tr").find(".stock_id").text();
        // get needed html
        $.get("<?= base_url('admin/stocks/view/')?>" + stock_id, function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_stock').modal(/* options object here*/);
            $('#view_stock').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/items/stock')?>";
            });
        });
    });
</script>


