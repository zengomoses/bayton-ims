<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    th{
        max-width: 200px;
        font-style: italic;
    }
    .mypanel{
        color: #0f0f0f;
    }
    .alert {
        padding: 2px;
    }
    .modal-dialog {
        width: 720px;
        margin: 30px auto;
    }
</style>
<div class="modal fade" id="add_item">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Item</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'item_form'));?>
                <div class="tab-content">
                    <div class="tab-pane active" id="item_basic_info">
                        <fieldset id="item_info">
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Item Name', 'item_name', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'name'=>'item_name',
                                            'id'=>'item_name',
                                            'class'=>'form-control input-sm',)
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Brand', 'brand', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'name'=>'brand',
                                            'id'=>'brand',
                                            'class'=>'form-control input-sm',)
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Description', 'description', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_textarea(array(
                                            'rows'=>'1',
                                            'name'=>'description',
                                            'id'=>'description',
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Category', 'category', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <select class="form-control input-sm" id="category" name="category">
                                        <option selected disabled>Select Category</option>
                                        <?php foreach ($categories as $category):?>
                                            <option value="<?= $category->id;?>"><?= $category->name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Supplier', 'supplier', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <select class="form-control input-sm" id="supplier" name="supplier">
                                        <option selected>None</option>
                                        <?php foreach ($suppliers as $supplier):?>
                                            <option value="<?= $supplier->person_id ;?>"><?= $supplier->company_name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Barcode', 'barcode', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-4'>
                                    <?php echo form_input(array(
                                            'name'=>'barcode',
                                            'id'=>'barcode',
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Buying Price', 'buying_price', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'text',
                                            'name'=>'buying_price',
                                            'id'=>'buying_price',
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Unit Selling Price', 'selling_price', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'text',
                                            'name'=>'unit_selling_price',
                                            'id'=>'unit_selling_price',
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Units', 'units', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <select class="form-control input-sm" id="unit" name="unit">
                                        <option selected disabled>Select Item Unit</option>
                                        <?php foreach ($units as $unit):?>
                                            <option value="<?= $unit->id ;?>"><?= $unit->description?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12 price">
                                <?php echo form_label('', 'selling_price', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8' >
                                    <div class="form-group form-group-sm col-xs-4">
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'ctn_selling_price',
                                                'id'=>'ctn_selling_price',
                                                'placeholder'=>'Carton Price',
                                                'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                    <div class="form-group form-group-sm col-xs-4">
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'outer_selling_price',
                                                'id'=>'outer_selling_price',
                                                'placeholder'=>'Outer Price',
                                                'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                    <div class="form-group form-group-sm col-xs-4">
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'dzn_selling_price',
                                                'id'=>'dzn_selling_price',
                                                'placeholder'=>'Dozen Price',
                                                'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Set Minimum Quantity in Store', 'min_quantity', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'number',
                                            'name'=>'min_quantity',
                                            'id'=>'min_quantity',
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div><!-- tab content -->
            <div class="modal-footer">
                <button class="btn btn-primary" id="submitbutton">Save</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_item">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Item</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'edit_item_form'));?>
                <div class="tab-content">
                    <div class="tab-pane active" id="item_basic_info">
                        <fieldset id="item_info">
                            <legend>Item Details</legend>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Item Name', 'item_name', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'name'=>'item_name',
                                            'id'=>'item_name',
                                            'class'=>'form-control input-sm',
                                            'value' => $item_info->item_name,)
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Brand', 'brand', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'name'=>'brand',
                                            'id'=>'brand',
                                            'class'=>'form-control input-sm',
                                            'value'=>$item_info->brand)
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Description', 'description', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_textarea(array(
                                            'rows'=>'2',
                                            'name'=>'description',
                                            'id'=>'description',
                                            'value' => $item_info->description,
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Category', 'category', array('class'=>'required control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <select class="form-control input-sm" id="category" name="category">
                                        <option disabled>Select Category</option>
                                        <?php foreach ($categories as $category):?>
                                            <?php if($category->id == $item_info->category_id):?>
                                                <option value="<?= $item_info->category_id;?>" selected><?= $item_info->name;?></option>
                                            <?php endif;?>
                                            <option value="<?= $category->id;?>"><?= $category->name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Supplier', 'supplier', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <select class="form-control input-sm" id="supplier" name="supplier">
                                        <option selected>None</option>
                                        <?php foreach ($suppliers as $supplier):?>
                                            <?php if($supplier->person_id == $item_info->supplier_id):?>
                                                <option value="<?= $item_info->supplier_id ;?>" selected><?= $item_info->first_name.' '.$item_info->last_name ;?></option>
                                            <?php endif;?>
                                            <option value="<?= $supplier->person_id ;?>"><?= $supplier->company_name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Barcode', 'barcode', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-4'>
                                    <?php echo form_input(array(
                                            'name'=>'barcode',
                                            'id'=>'barcode',
                                            'value' => $item_info->barcode,
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Buying Price', 'buying_price', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'text',
                                            'name'=>'buying_price',
                                            'id'=>'buying_price',
                                            'value'=> $item_info->buying_price,
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Unit Selling Price', 'selling_price', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'text',
                                            'name'=>'unit_selling_price',
                                            'id'=>'unit_selling_price',
                                            'value'=>$item_info->unit_selling_price,
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12" id="unitSelect">
                                <?php echo form_label('Units', 'units', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <select class="form-control input-sm" id="unit_2" name="unit">
                                        <?php foreach ($units as $unit):?>
                                            <?php if($item_info->unit_id == $unit->id):?>
                                                <option value="<?= $item_info->unit_id?>" selected hidden><?= $item_info->unit_desc?></option>
                                            <?php endif;?>
                                            <option value="<?= $unit->id ;?>"><?= $unit->description?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12 price_2" style="display: block">
                                <?php echo form_label('', 'selling_price', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8' >
                                    <div class="form-group form-group-sm col-xs-4">
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'ctn_selling_price',
                                                'id'=>'ctn_selling_price',
                                                'placeholder'=>'Carton Price',
                                                'value'=>$item_info->ctn_selling_price,
                                                'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                    <div class="form-group form-group-sm col-xs-4">
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'outer_selling_price',
                                                'id'=>'outer_selling_price',
                                                'placeholder'=>'Outer Price',
                                                'value'=>$item_info->outer_selling_price,
                                                'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                    <div class="form-group form-group-sm col-xs-4">
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'dzn_selling_price',
                                                'id'=>'dzn_selling_price',
                                                'placeholder'=>'Dozen Price',
                                                'value'=>$item_info->dzn_selling_price,
                                                'class'=>'form-control input-sm')
                                        );?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Set Minimum Quantity in Store', 'min_quantity', array('class'=>'control-label col-xs-3')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'number',
                                            'name'=>'min_quantity',
                                            'id'=>'min_quantity',
                                            'value'=>$item_info->min_quantity,
                                            'class'=>'form-control input-sm')
                                    );?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div><!-- tab content -->
            <div class="modal-footer">
                <button class="btn btn-primary" id="updatebutton">Update</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close();?>
            <input type="hidden" class="item_id" value="<?= $item_info->id; ?>">
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#item_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                item_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter Item name'
                        }
                    }
                },
                brand: {
                    validators: {
                        stringLength: {
                            min: 1
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the item category'
                        }
                    }
                },
                unit: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the item unit'
                        }
                    }
                },
                supplier: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the supplier of the item'
                        }
                    }
                },
                barcode: {
                    validators: {
                        stringLength: {
                            min: 6
                        },
                        regexp: {
                            regexp: /^[0-9]*$/,
                            message: 'The barcode must contain digits only'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter the item description'
                        }
                    }
                },
                buying_price: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter the item buying price'
                        },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                unit_selling_price: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Please enter the item selling price'
                        // },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                ctn_selling_price: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Please enter the Carton selling price'
                        // },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                outer_selling_price: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Please enter the Outer selling price'
                        // },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                dzn_selling_price: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Please enter the Outer selling price'
                        // },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                // qty_outer: {
                //     validators: {
                //         regexp: {
                //             regexp: /^[0-9]*$/,
                //             message: 'The quantity must be a number'
                //         }
                //     }
                // },
                // qty_pcs: {
                //     validators: {
                //         regexp: {
                //             regexp: /^[0-9]*$/,
                //             message: 'The quantity must be a number'
                //         }
                //     }
                // }
            }
        });

        $("#submitbutton").click(function(e){  // passing down the event
            var validator = $('#item_form').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/items/save/')?>',
                    type: 'POST',
                    data: $("#item_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Item has been added successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    });

    $(document).ready(function(){
        $('#edit_item_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                item_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter Item name'
                        }
                    }
                },
                brand: {
                    validators: {
                        stringLength: {
                            min: 1
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the item category'
                        }
                    }
                },
                unit: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the item unit'
                        }
                    }
                },
                supplier: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the supplier of the item'
                        }
                    }
                },
                barcode: {
                    validators: {
                        stringLength: {
                            min: 6
                        },
                        regexp: {
                            regexp: /^[0-9]*$/,
                            message: 'The barcode must contain digits only'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter the item description'
                        }
                    }
                },
                buying_price: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter the item buying price'
                        },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                unit_selling_price: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter the item selling price'
                        },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                ctn_selling_price: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Please enter the Carton selling price'
                        // },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                outer_selling_price: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Please enter the Outer selling price'
                        // },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                dzn_selling_price: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Please enter the Outer selling price'
                        // },
                        numeric:{
                            message: 'The price must contain digits only'
                        }
                    }
                },
                // qty_outer: {
                //     validators: {
                //         numeric: {
                //             message: 'The quantity must be a number'
                //         }
                //     }
                // },
                // qty_pcs: {
                //     validators: {
                //         numeric: {
                //             message: 'The quantity must be a number'
                //         }
                //     }
                // }
            }
        });

        $("#updatebutton").click(function(e){
            var validator = $('#edit_item_form').data('bootstrapValidator');
            var item_id = $('input.item_id').val();
            validator.validate();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/items/update/')?>' + item_id,
                    type: 'POST',
                    data: $("#edit_item_form").serialize(),
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Item has been updated successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });

       /* $('#unit').change(function(){
            if($(this).val() === "1"){
                $('.price').css('display', 'block');
            }else{
                $('.price').css('display', 'none');
            }
        });*/

        /*$('#unit_2').change(function(){
            if($(this).val() === "1"){
                $('.price_2').css('display', 'block');
            }else{
                $('.price_2').css('display', 'none');
            }
        });*/
        /*$('#unitSelect option').each(function() {
            if($(this).is(':selected') && $(this).val()==1){
                $('.price_2').css('display', 'block');
            }else{
                $('.price_2').css('display', 'none');
            }
        });*/
    });
</script>
<div class="modal fade" id="view_item">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--           modal header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">View item</h4>
            </div>
            <!--                 modal body-->
            <div class="modal-body">
                <div class="row">
                    <div class="panel panel-default mypanel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Item Details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-responsive table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <th>Item</th>
                                            <td><?= $item_info->item_name." ". $item_info->description ?></td>
                                        </tr>
                                        <tr>
                                            <th>Brand</th>
                                            <td><?= $item_info->brand ?></td>
                                        </tr>
                                        <tr>
                                            <th>Category</th>
                                            <td><?= $item_info->name ?></td>
                                        </tr>
                                        <tr>
                                            <th>Supplier</th>
                                            <td><?= $item_info->first_name." ".$item_info->last_name; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Barcode</th>
                                            <td><?= $item_info->barcode; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Buying Price</th>
                                            <td><?= $item_info->buying_price; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Unit Selling Price</th>
                                            <td><?= $item_info->unit_selling_price; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Carton Selling Price</th>
                                            <td><?= $item_info->ctn_selling_price; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Outer Selling Price</th>
                                            <td><?= $item_info->outer_selling_price; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Dozen Selling Price</th>
                                            <td><?= $item_info->dzn_selling_price; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Quantity In</th>
                                            <td><?= $item_info->quantity_in; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Quantity Out</th>
                                            <td><?= $item_info->quantity_out; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-4">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

