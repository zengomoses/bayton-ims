<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_admin'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;PROFORMA INVOICES</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
            <button class="btn btn-info btn-md pull-right add-proforma-btn" data-target="#add_proforma_invoice" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> Create Proforma Invoice
            </button>
            <form action="<?= base_url('admin/invoice/proforma_invoice_print')?>" target="_blank">
                <button class="btn btn-danger btn-md pull-right print-btn" style="font-size: small; font-weight: bold">
                    <i class="fa fa-lg fa-print"></i> Print
                </button>
            </form>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>INVOICE #</th>
                            <th>CUSTOMER</th>
                            <th>CREATED BY</th>
                            <th>DATE CREATED</th>
                            <th>SUB TOTAL</th>
                            <th>VAT</th>
                            <th>TOTAL(Inc VAT)</th>
                            <!-- <th>STATUS</th> -->
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($invoices as $invoice):?>
                            <tr>
                                <td class="invoice_id"><?= $invoice->id; ?></td>
                                <td class="invoice_num"><?= $invoice->invoice_number; ?></td>
                                <td><?php foreach($customers as $customer): ?>
                                        <?php if($customer->id == $invoice->customer_id):?>
                                            <?= $customer->first_name." ".$customer->last_name?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </td>
                                <td><?php foreach($employees as $employee): ?>
                                        <?php if($employee->id == $invoice->employee_id):?>
                                            <?= $employee->first_name." ".$employee->last_name?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </td>
                                <td><?= $invoice->date_created ?></td>
                                <td><?= $invoice->sub_total ?></td>
                                <td><?= $invoice->VAT ?></td>
                                <td><?= $invoice->total_VAT ?></td>
                                <!-- <td><?= $invoice->status ?></td> -->
                                <td>
                                    <!-- <button class="btn btn-xs btn-default view-proforma-btn" data-target="#view_proforma_invoice" data-toggle="modal">
                                        <i class="fa fa-eye">&nbsp;</i>View
                                    </button> -->
                                    <button class="btn btn-xs btn-primary print-proforma-btn">
                                        <i class="fa fa-copy">&nbsp;</i>Print
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(".add-proforma-btn").click(function () {
        // get needed html
        $.get("<?= base_url('admin/invoices/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_proforma_invoice').modal(/* options object here*/);
            $('#add_proforma_invoice').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/invoices/')?>";
            });
        });
    });

    $(".view-proforma-btn").click(function () {
        var invoice_id = $(this).closest("tr").find(".invoice_id").text();
        console.log(invoice_id);
        // get needed html
        $.get("<?= base_url('admin/invoices/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#view_proforma_invoice').modal(/* options object here*/);
            $('#view_proforma_invoice').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('admin/invoices/')?>";
            });
        });
    });

    $('.print-proforma-btn').on('click', function(){
        var invoice_num = $(this).closest("tr").find(".invoice_num").text();
        window.open("<?= base_url('admin/invoices/print_invoice/')?>" + invoice_num, 'Invoice', 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=900,left=0,top=0');
        window.location.assign('<?=base_url('admin/invoices/')?>');
    });
</script>

