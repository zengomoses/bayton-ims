<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 10/15/18
 * Time: 5:37 AM
 */

class Customers extends Pos_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('admin/customer'));
        $this->load->helper(array('form'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "customers/customers";
        $data['customers'] = $this->customer->get_all();
        $data['title'] = "CUSTOMERS";
        $this->load->view($this->_container, $data);
    }

    public function view($customer_id=-1) {
        $data['person_info'] = $this->customer->get_info($customer_id);
        $this->load->view('customers/form',$data);
    }

    /*
   Inserts/updates an customer
   */
    function save()
    {
        $phone_number_1 = "255".substr($this->input->post('phone_number_1'),1);
        if($this->input->post('phone_number_2') != ""){
            $phone_number_2 = "255".substr($this->input->post('phone_number_2'),1);
        }else{
            $phone_number_2 = "";
        }
        if($this->input->post('phone_number_3') != ""){
            $phone_number_3 = "255".substr($this->input->post('phone_number_3'),1);
        }else{
            $phone_number_3 = "";
        }

        $person_data = array(
            'first_name'=>ucfirst($this->input->post('first_name')),
            'last_name'=>ucfirst($this->input->post('last_name')),
            'gender'=>$this->input->post('gender'),
            'email'=>$this->input->post('email'),
            'phone_number_1'=>$phone_number_1,
            'phone_number_2'=>$phone_number_2,
            'phone_number_3'=>$phone_number_3,
            'address'=>$this->input->post('address'),
            'city'=>$this->input->post('city'),
            'region'=>$this->input->post('region'),
            'district'=>$this->input->post('district'),
            'comments'=>$this->input->post('comments')
        );

        $customer_data=array(
            'company_name'=>$this->input->post('company_name'),
        );

        if($this->customer->save_customer($person_data,$customer_data))
        {
            echo json_encode(array('success'=>true,));
        }
    }
    function save_custom()
    {
        $phone_number_1 = "255".substr($this->input->post('phone_number_1'),1);

        $person_data = array(
            'first_name'=>$this->input->post('first_name'),
            'last_name'=>$this->input->post('last_name'),
            'phone_number_1'=>$phone_number_1,
        );

        $customer_data=array(
            'company_name'=>$this->input->post('company_name'),
        );

        if($this->customer->save_customer($person_data,$customer_data))
        {
            $customer=$this->customer->get_custom_info($person_data);
            if($customer){
                echo json_encode(array("id"=>$customer->id));
            }else{
                echo json_encode("False");
            }
        }
    }

    function update($customer_id)
    {
        $phone_number_1 = "255".substr($this->input->post('phone_number_1'),1);
        if($this->input->post('phone_number_2') != ""){
            $phone_number_2 = "255".substr($this->input->post('phone_number_2'),1);
        }else{
            $phone_number_2 = "";
        }
        if($this->input->post('phone_number_3') != ""){
            $phone_number_3 = "255".substr($this->input->post('phone_number_3'),1);
        }else{
            $phone_number_3 = "";
        }

        $person_data = array(
            'first_name'=>$this->input->post('first_name'),
            'last_name'=>$this->input->post('last_name'),
            'gender'=>$this->input->post('gender'),
            'email'=>$this->input->post('email'),
            'phone_number_1'=>$phone_number_1,
            'phone_number_2'=>$phone_number_2,
            'phone_number_3'=>$phone_number_3,
            'address'=>$this->input->post('address'),
            'city'=>$this->input->post('city'),
            'region'=>$this->input->post('region'),
            'district'=>$this->input->post('district'),
            'comments'=>$this->input->post('comments')
        );

        $customer_data=array(
            'company_name'=>$this->input->post('company_name'),
        );

        if($this->customer->update_customer($person_data,$customer_data, $customer_id))
        {
            echo json_encode(array('success'=>true,));
        }else{
            echo json_encode(array('success'=>false,));
        }
    }
}