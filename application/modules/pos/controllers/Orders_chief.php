<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 10/15/18
 * Time: 5:37 AM
 */

class Orders_chief extends Pos_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('admin/order','admin/employee', 'admin/store', 'admin/item', 'admin/unit', 'admin/stock'));
        $this->load->helper(array('form'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "orders/orders";
        $data['orders'] = $this->order->get_all();
        $data['employees'] = $this->employee->get_all();
        $data['title'] = "ORDERS";
        $this->load->view($this->_container, $data);
    }

    public function view($order_id=-1) {
        $data['order_info'] = $this->order->get_info($order_id);
        $data['order_items'] = $this->order->get_order_items($order_id);
        $data['employees'] = $this->employee->get_all();
        $data['items'] = $this->item->get_all();
        $data['stores'] = $this->store->get_all();
//        $data['units'] = $this->unit->get_all();
        $this->load->view('orders/form',$data);
    }

    public function process($order_id){
        $stock_info = $this->input->post('stock_info');
        $order_info = $this->input->post('order_info');
        $order_items = $this->input->post('order_items');
        $order_items = json_decode($order_items,true);

        if(!$this->stock->exists($stock_info['stock_id'])){
            if($data = $this->order->process_order($order_id, $stock_info, $order_items, $order_info)== true)
            {
                echo json_encode('true');
            }else{
                echo json_encode('false');
            }
        }else{
            echo json_encode('1');
        }
    }

    public function approve($order_id){
        $order_info = $this->input->post('order_info');

        if($data = $this->order->approve_order($order_id, $order_info)== true)
        {
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }

    }

    public function reject($order_id){
        $order_info = $this->input->post('order_info');

        if($data = $this->order->approve_order($order_id, $order_info)== true)
        {
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }

    }
}