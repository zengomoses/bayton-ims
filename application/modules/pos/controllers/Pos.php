<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 9/27/17
 * Time: 12:58 PM
 */

class Pos extends Pos_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model(array('pos/stock','sms/sms_model','pos/item','pos/pos_model','admin/category', 'pos/store', 'admin/customer'));
        $this->load->helper(array('form'));
        $this->load->library(array('nexmo'));
    }

    public function index() {
        $data['customers'] = $this->customer->get_all();
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "dashboard";
        $data['module'] = 'pos';
        $data['title'] = 'POS REGISTER';
        $this->load->view($this->_container, $data);
    }

    public function pos_sms()
    {
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "send_sms";
        $data['customers'] = $this->sms_model->get_customer_contacts();
        $data['employees'] = $this->sms_model->get_employee_contacts();
        $data['suppliers'] = $this->sms_model->get_supplier_contacts();
        $data['module'] = 'pos';
        $data['title'] = "SMS";
        $this->load->view($this->_container, $data);
    }
    public function view_by_barcode($item_number) {
        $data['item_info'] = $this->pos_model->get_info_by_barcode($item_number);

        echo json_encode($data);
    }

    public function view_by_id($item_id) {
        $data['item_info'] = $this->pos_model->get_info_by_id($item_id);

        echo json_encode($data);
    }

    public function get_units() {
        $data['units'] = $this->pos_model->get_item_units();
        echo json_encode($data);
    }

    public function search(){

        $search_data = $this->input->post('search_data');

        $result = $this->pos_model->search($search_data);

        if (!empty($result))
        {
            foreach ($result as $row):
                echo "<option class='item' value='". $row->id ."'>" . $row->item_name . ", " . $row->description ."</option>";
            endforeach;
        }
        else
        {
            echo "<option class='item' disabled>Not found ... </option>";
        }
    }


    function save()
    {
        $sales_info = $this->input->post('sales_data');
        $sales_items = $this->input->post('data');
        $sales = $this->input->post('data_2');
        $sales_items = json_decode($sales_items,true);
        if(!$this->pos_model->exists($sales_info['invoice_number'])){
            if($this->pos_model->save_sales($sales_info, $sales_items) == true)
            {
                $this->session->set_userdata('sales_info', $sales_info);
                $this->session->set_userdata('sales_items', $sales);
                $customer = $this->customer->get_info($this->session->userdata['sales_info']['customer_id']);
                $this->send_sms($customer->phone_number_1, $customer->first_name,$sales_info['invoice_number']);
                echo json_encode('true');
            }else{
                echo json_encode('false');
            }
        }else{
            echo json_encode('1');
        }
    }

    public function send_sms($customer_id, $customer_name, $invoice){
        $from = 'BYTON GROUP';
        $to = $customer_id;
        $sms = $this->sms_model->get_custom_sms() ;
        $msg = str_replace(array("\$name","\$invoice"),array("$customer_name", "$invoice"),$sms->message);
        $message = array(
            'text' => $msg,
        );
        $response = $this->nexmo->send_message($from, $to, $message);
        if($response['messages']['0']['status'] == 0){
            return true;
        }
    }

    function Header()
    {
        $this->fpdf->Image('assets/images/byton_logo.png',10,6,40);
        $this->fpdf->SetFont('Arial','B',22);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,6,'BYTON GROUP LIMITED',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,5,'P.O Box 3314 MOB:0755 480057, 0658 480057, 0745 800802, 0766 800802',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,3,'MWANJELWA-MBEYA, TUNDUMA ROAD OPPOSITE MWANJELWA NEW MARKET',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,3,'Dealers in: Stationaries, Lamination, Binding, Printing all types of Cards,Book Sales, Ink Refills, e.t.c',0,0,'C');
        $this->fpdf->SetLineWidth(0.6);
        $this->fpdf->Line(1,28,209,28);
        $this->fpdf->Ln();
    }
    function Footer()
    {
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(10);
        date_default_timezone_set("Africa/Nairobi");
        $this->fpdf->Cell(0,10,date("F d, Y h:i A",time()),0,0,'L');

        $this->fpdf->Ln();
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(0,10,'Page '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
    }

    function print_invoice($invoice)

    {
        $this->fpdf->AddPage('P');
        $this->header();
        $header = array('DESCRIPTION','QTY','PRICE','DISCOUNT','TOTAL');

        $data = $this->pos_model->get_sales_items($invoice);
        $sale = $this->pos_model->get_invoice_info($invoice);
        $customers = $this->customer->get_all();
        $this->fpdf->SetFont('Times','B',14);
        $this->fpdf->Cell(90);
        $this->fpdf->Cell(10,15, 'INVOICE',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','B',11);
        $this->fpdf->SetXY( 20 , 40 );
        $this->fpdf->Cell(20, 4, "INVOICE # : ", '', '', "L");
        $this->fpdf->SetFont('Times','',11);
        $this->fpdf->SetXY( 45 , 40 );
        $this->fpdf->Cell(20, 4,$invoice, '', '', "L");
        $this->fpdf->SetFont('Times','B',11);
        $this->fpdf->SetXY( 135 , 40 );
        $this->fpdf->Cell(20, 4, "DATE : ", '', '', "L");
        $this->fpdf->SetFont('Times','',11);
        $this->fpdf->SetXY( 150 , 40 );
        $this->fpdf->Cell(20, 4,$sale->sale_time, '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','B',11);
        $this->fpdf->SetXY( 20 , 50 );
        $this->fpdf->Cell(40, 4, "BILL TO :", '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','',11);
        $this->fpdf->SetXY( 20 , 54 );
        foreach ($customers as $customer):
            if($sale->customer_id == $customer->id):
                $this->fpdf->Cell(40, 4,$customer->first_name." ".$customer->last_name, '', '', "L");
                $this->fpdf->Ln();
                $this->fpdf->SetXY( 20 , 58 );
                $this->fpdf->Cell(40, 4, $customer->address, '', '', "L");
                $this->fpdf->Ln();
            endif;
        endforeach;
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->SetXY(20,66);
        $this->fpdf->SetFillColor(244,247, 252);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetDrawColor(0);
        $this->fpdf->SetLineWidth(.2);
        $w = array(95,10,27,20,27);
        for($i=0;$i<count($header);$i++)
            $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->SetFillColor(224,235,255);
        $this->fpdf->SetTextColor(0);
        $fill = false;
        foreach($data as $row)
        {
            $this->fpdf->Cell(10);
            $this->fpdf->Cell($w[0],6,$row->item_name,'LR',0,'L');
            $this->fpdf->Cell($w[1],6,$row->quantity_purchased,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[2],6," Tsh. ".number_format(($row->amount / $row->quantity_purchased), 2,".", ','),'LR',0,'L',$fill);
            $this->fpdf->Cell($w[3],6,$row->disc. " %",'LR',0,'L',$fill);
            $this->fpdf->Cell($w[4],6," Tsh. ".number_format(($row->amount), 2,".", ','),'LR',0,'R',$fill);
            $this->fpdf->Ln();
            $fill = !$fill;
        }
        $this->fpdf->Cell(10);
        $this->fpdf->Cell(array_sum($w),0,'','T');
        $this->fpdf->Ln();
        $this->fpdf->Cell(142);
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->Cell(17,6,'Total Discount : ','LB',0,'L');
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->Cell(30,6,$sale->discount." %",'RB',0,'R');
        $this->fpdf->Ln();
        $this->fpdf->Cell(142);
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->Cell(17,6,'Total Payout :','LB',0,'L');
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->Cell(30,6," Tsh. ".number_format(($sale->amount_paid + $sale->amount_due), 2,".", ','),'RB',0,'R');
        $this->fpdf->Ln();
        $this->fpdf->Cell(142);
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->Cell(17,6,'Amount Paid :','LB',0,'L');
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->Cell(30,6," Tsh. ".number_format($sale->amount_paid, 2,".", ','),'RB',0,'R');
        $this->fpdf->Ln();
        $this->fpdf->Cell(142);
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->Cell(17,6,'Amount Due ','LB',0,'L');
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->Cell(30,6," Tsh. ".number_format($sale->amount_due, 2,".", ','),'RB',0,'R');
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Times','B', 11);
        $this->fpdf->Cell(0,8, "Make all checks payable to BYTON GROUP LIMITED");
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Times','B', 12);
        $this->fpdf->Cell(0,10,  "THANK YOU FOR MAKING BUSINESS WITH US!!");
        $this->footer();
        $this->fpdf->Output();
    }

}
