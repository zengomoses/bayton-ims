<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Invoices extends Pos_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('pos/item','pos/category', 'pos/employee', 'pos/customer', 'pos/invoice'));
        $this->load->helper(array('form'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "invoices/invoices";
        $data['title'] = "INVOICES";
        $data['invoices'] = $this->invoice->get_all();
        $data['customers'] = $this->customer->get_all();
        $data['employees'] = $this->employee->get_all();
        $data['title'] = 'PROFORMA INVOICES';
        $this->load->view($this->_container, $data);
    }

    public function view($invoice_id=-1) {
        $data['customers'] = $this->customer->get_all();
        $data['invoice_info'] = $this->invoice->get_info($invoice_id);
        $data['item_info'] = $this->item->get_all();
        $this->load->view('invoices/form',$data);
    }

    public function get_items($invoice){
        $data['item_info'] = $this->invoice->get_info($invoice);
        var_dump($data);
    }
    
    public function save()
    {
        $invoice_info = $this->input->post('invoice_data');
        $invoice_items = $this->input->post('invoice_items');
        if(!$this->invoice->exists($invoice_info['invoice_number'])){

            if($this->invoice->save_invoice_data($invoice_info, $invoice_items) == true)
            {
                echo json_encode('true');
            }else{
                echo json_encode('false');
            }
        }else{
            echo json_encode('1');
        }
    }

    function Header()
    {
        $this->fpdf->Image('assets/images/byton_logo.png',10,6,30);
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->Cell(0,0,'BYTON GROUP LIMITED',0,0,'R');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','I',10);
        $this->fpdf->Cell(0,8,'Tunduma Road, Opp. Mwanjelwa Market ',0,0,'R');
        $this->fpdf->Cell(0,16,'P.O Box 3314 Mwanjelwa, Mbeya', 0, 0,'R');
        $this->fpdf->Cell(0,24,'Tel:0755 480057, 0658 480057',0,0,'R');
        $this->fpdf->SetLineWidth(0.4);
        $this->fpdf->Line(10,25,200,25);
        $this->fpdf->Ln();
    }

    function Footer()
    {
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(10);
        date_default_timezone_set("Africa/Nairobi");
        $this->fpdf->Cell(0,10,date("F d, Y h:i A",time()),0,0,'L');

        $this->fpdf->Ln();
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(0,10,'Page '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
    }

    public function print_invoice($invoice_number)

    {
        $this->fpdf->AddPage('P');
        $this->header();
        $header = array('No', 'DESCRIPTION', 'Qty', 'Unit','Unit Price', 'Total');
        
        $data = $this->invoice->get_items($invoice_number);
        $invoice = $this->invoice->get_info($invoice_number);
        $customers = $this->customer->get_all();
        $employees = $this->employee->get_all();
        $this->fpdf->SetFont('Times','B',12);
        $this->fpdf->SetXY( 20 , 35 );
        $this->fpdf->Cell(0, 0, "Bill To", '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','I',11);

        foreach ($customers as $customer):
            if($invoice->customer_id == $customer->id):
                $this->fpdf->SetXY( 20 , 40 );
                $this->fpdf->Cell(0, 0,'Customer #: '. $customer->id , '', "L");
                $this->fpdf->Ln();
                $this->fpdf->SetXY( 20 , 40 );
                $this->fpdf->Cell(0, 8,$customer->first_name." ".$customer->last_name, '', '', "L");
                $this->fpdf->Ln();
                $this->fpdf->SetXY( 20 , 40 );
                $this->fpdf->Cell(10, 16, $customer->address, '', '', "L");
                $this->fpdf->Ln();
            endif;
        endforeach;

        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','B',12);
        $this->fpdf->SetXY( 130 , 35 );
        $this->fpdf->Cell(0, 0, "Proforma Invoice", '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','', 11);
        $this->fpdf->SetXY( 130 , 40 );
        $this->fpdf->Cell(0, 0,'Ref no.', '', "L");
        $this->fpdf->SetXY( 160 , 40 );
        $this->fpdf->Cell(0, 0, ': '.$invoice->invoice_number , '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetXY( 130 , 40 );
        $this->fpdf->Cell(0, 8,'Date', '', "L");
        $this->fpdf->SetXY( 160 , 40 );
        $this->fpdf->Cell(0, 8,': '.$invoice->date_created, '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetXY( 130 , 40 );
        $this->fpdf->Cell(10, 16, 'Valid until : ', '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetXY( 130 , 40 );
        $this->fpdf->Cell(10, 24, 'Contact Person : ', '', '', "L");
        $this->fpdf->SetXY( 160 , 40 );

        foreach ($employees as $employee):
            if($invoice->employee_id == $employee->id):
                $this->fpdf->Cell(10, 24, ': '.$employee->first_name .' '.$employee->last_name, '', '', "L");
            endif;
        endforeach; 
    
        $this->fpdf->Ln();
    
        $this->fpdf->SetFont('Times','B',8);
        $this->fpdf->SetXY(15,70);
        $this->fpdf->SetFillColor(255,255,255);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetDrawColor(0);
        $this->fpdf->SetLineWidth(.2);
        $w = array(10,95,10,10,30,30);
        for($i=0;$i<count($header);$i++)
            $this->fpdf->Cell($w[$i],6,$header[$i],1,0,'C',true);
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','',9);
        $this->fpdf->SetFillColor(255,255,255);
        $this->fpdf->SetTextColor(0);
        $fill = false;
        $count = 0;
        $total = 0;
        foreach($data as $row)
        {

            $count += 1;
            $total += $row->unit_price * $row->quantity;
            $this->fpdf->Cell(5);
            $this->fpdf->Cell($w[0],6, $count ,'LR',0,'C');
            $this->fpdf->Cell($w[1],6,$row->item_name." " .$row->brand, 'LR',0,'L');
            $this->fpdf->Cell($w[2],6,$row->quantity,'LR',0,'C',$fill);
            $this->fpdf->Cell($w[3],6,$row->unit,'LR',0,'C',$fill);
            $this->fpdf->Cell($w[4],6,number_format(($row->unit_price), 2,".", ','),'LR',0,'R',$fill);
            $this->fpdf->Cell($w[5],6,number_format(($row->unit_price * $row->quantity), 2,".", ','),'LR',0,'R',$fill);
            $this->fpdf->Ln();
            $fill = !$fill;
        }
        $this->fpdf->Cell(5);
        $this->fpdf->Cell(array_sum($w),0,'','T');
        $this->fpdf->Ln();
        $this->fpdf->Cell(130);
        $this->fpdf->SetFont('Times','B',9);
        $this->fpdf->Cell(30,6,'Total (TZS) :','LB',0,'L');
        $this->fpdf->SetFont('Times','B',9);
        $this->fpdf->Cell(30,6,number_format(($total), 2,".", ','),'RB',0,'R');
        $this->fpdf->Ln();
        $this->fpdf->Cell(130);
        $this->fpdf->SetFont('Times','',9);
        $this->fpdf->Cell(30,6,'18% VAT of '. $total,'LB',0,'L');
        $this->fpdf->SetFont('Times','',9);
        $this->fpdf->Cell(30,6,number_format($total * 0.18, 2,".", ','),'RB',0,'R');
        $this->fpdf->Ln();
        $this->fpdf->Cell(130);
        $this->fpdf->SetFont('Times','B',9);
        $this->fpdf->Cell(30,6,'Total (TZS) Inc. VAT ','B',0,'L');
        $this->fpdf->SetFont('Times','',9);
        $this->fpdf->Cell(30,6,number_format($total + $total * 0.18, 2,".", ','),'RB',0,'R');
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Times','B', 11);
        $this->fpdf->Cell(0,8, "Make all checks payable to BYTON GROUP LIMITED");
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Times','B', 12);
        $this->fpdf->Cell(0,10,  "THANK YOU FOR MAKING BUSINESS WITH US!!");
        $this->footer();
        $this->fpdf->Output();
    }
}
