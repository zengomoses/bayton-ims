<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Stores extends Pos_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/category', 'admin/supplier', 'admin/employee', 'admin/stock', 'pos/store'));
        $this->load->helper(array('form'));

    }

    public function stores() {
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "items/stores";
        $data['stores'] = $this->store->get_all();
        $data['title'] = "STORES";
        $this->load->view($this->_container, $data);
    }

    public function view($store_id=-1) {
        $data['store_info'] = $this->store->get_info($store_id);
        $data['store_items'] = $this->store->get_items_store($store_id);
        $this->load->view('items/store_form', $data);
    }

    public function delete($store_id=-1) {
        $data['store_info'] = $this->store->get_info($store_id);
        $this->load->view('items/store_form', $data);
    }

    /*
 Inserts/updates an category
 */
    public  function save()
    {
        $store_data = array(
            'name'=>$this->input->post('store_name'),
        );

        if($this->store->save_store($store_data))
        {
            echo json_encode(array('success'=>true));

        }
        else//failure
        {
            echo json_encode(array('success'=>false));
        }
    }

    public  function update($store_id)
    {
        $store_data = array(
            'name'=>$this->input->post('store_name'),
        );

        if($this->store->update_store($store_data, $store_id))
        {
            echo json_encode(array('success'=>true));

        }
        else
        {
            echo json_encode(array('success'=>false));
        }
    }

}
