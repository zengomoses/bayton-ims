<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 10/27/17
 * Time: 2:23 PM
 */

class Stocks extends Pos_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('pos/stock', 'pos/item','admin/category', 'pos/store', 'admin/supplier', 'admin/employee'));
        $this->load->helper(array('form'));
    }
    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "items/stocks";
        $data['suppliers'] = $this->supplier->get_all();
        $data['employees'] = $this->employee->get_all();
        $data['stocks'] = $this->stock->get_all();
        $data['title'] = "STOCKS";
        $this->load->view($this->_container, $data);
    }


    public function view($stock_id=-1) {
        $data['stock_info'] = $this->stock->get_stock($stock_id);
        $data['items_info'] = $this->stock->get_info($stock_id);
        $data['item_info'] = $this->item->get_all();
        $data['stock_items'] = $this->item->get_all();
        $data['stores'] = $this->store->get_all();
        $this->load->view('items/stock_form', $data);
    }

    public function view_edit($stock_id=-1) {
        $data['stock_info'] = $this->stock->get_info($stock_id);
        $data['stock'] = $this->stock->get_distinct_info($stock_id);
        $data['item_info'] = $this->item->get_all();
        $data['stores'] = $this->store->get_all();
        $this->load->view('items/edit_stock_form', $data);
    }
    public function view_get($stock_id=-1) {
        $data['stock_info'] = $this->stock->get_info($stock_id);
        $data['item_info'] = $this->item->get_all();
        $data['stores'] = $this->store->get_all();
        $this->load->view('items/get_stock_form', $data);
    }

    public function save()
    {
        $stock_info = $this->input->post('stock_info');
        $stock_items = $this->input->post('data');;
        $stock_items = json_decode($stock_items,true);

        if(!$this->stock->exists($stock_info['stock_id'])){
            if($this->stock->save_stock($stock_info, $stock_items) == true)
            {
                echo json_encode('true');
            }else{
                echo json_encode('false');
            }
        }else{
            echo json_encode('1');
        }
    }
    public function save_stock_out()
    {
        $stock_info = $this->input->post('stock_info');
        $stock_items = $this->input->post('data');;
        $stock_items = json_decode($stock_items,true);

        if(!$this->stock->exists($stock_info['stock_id'])){
            if($this->stock->save_stock_out($stock_info, $stock_items) == true)
            {
                echo json_encode('true');
            }else{
                echo json_encode('false');
            }
        }else{
            echo json_encode('1');
        }
    }

    public function update($stock_id)
    {
        $stock_info = $this->input->post('stock_info');
        $stock_items = $this->input->post('data');;
        $stock_items = json_decode($stock_items,true);

        if($this->stock->update_stock($stock_info, $stock_items, $stock_id) == true)
        {
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }
    }

    public function get_stocks(){
        $data = $this->stock->get_info("1512539723");
        var_dump($data);
    }

    function Header()
    {
        $this->fpdf->Image('assets/images/byton_logo.png',10,6,40);
        $this->fpdf->SetFont('Arial','B',22);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,6,'BYTON GROUP LIMITED',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,5,'P.O Box 3314 MOB:0755 480057, 0658 480057, 0745 800802, 0766 800802',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,3,'MWANJELWA-MBEYA, TUNDUMA ROAD OPPOSITE MWANJELWA NEW MARKET',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,3,'Dealers in: Stationaries, Lamination, Binding, Printing all types of Cards,Book Sales, Ink Refills, e.t.c',0,0,'C');
        $this->fpdf->SetLineWidth(0.6);
        $this->fpdf->Line(1,28,209,28);
        $this->fpdf->Ln();
    }
    function Footer()
    {
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(10);
        date_default_timezone_set("Africa/Nairobi");
        $this->fpdf->Cell(0,10,date("F d, Y h:i A",time()),0,0,'L');

        $this->fpdf->Ln();
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(0,10,'Page '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
    }

    function stock_list()
    {
        $this->header();
        $header = array('ID', 'ITEM NAME', 'BRAND', 'DESCRIPTION','CATEGORY', 'QTY IN STOCK', 'LOCATION');

        $data = $this->stock->get_all_print();

        $this->fpdf->SetFont('Helvetica','B',11);
        $this->fpdf->Cell(90);
        $this->fpdf->Cell(10,15, 'STOCK IN ALL STORES',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Helvetica','',9);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFillColor(153,153,153);
        $this->fpdf->SetTextColor(255);
        $this->fpdf->SetDrawColor(89,89,89);
        $this->fpdf->SetLineWidth(.1);
        $this->fpdf->SetFont('','B');
        $w = array(7,30,30,30,30,30,20);
        for($i=0;$i<count($header);$i++)
            $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->fpdf->Ln();
        $this->fpdf->SetFillColor(224,235,255);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFont('');
        $fill = false;
        foreach($data as $row)
        {
            $this->fpdf->Cell(10);
            $this->fpdf->Cell($w[0],6,$row->id,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[1],6,$row->item_name,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[2],6,$row->brand,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[3],6,$row->description,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[4],6,$row->category,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[5],6,$row->quantity,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[6],6,$row->store,'LR',0,'L',$fill);
            $this->fpdf->Ln();
            $fill = !$fill;
        }
        $this->fpdf->Cell(10);
        $this->fpdf->Cell(array_sum($w),0,'','T');
        $this->footer();
        $this->fpdf->Output();
    }

    function stock($stock_id)
    {
        $this->header();
        $header = array('ID', 'ITEM NAME', 'BRAND', 'DESCRIPTION','COST PRICE', 'UNIT PRICE', 'QUANTITY');

        $stock_info =  $this->stock->get_distinct_info($stock_id);
        $data = $this->stock->get_info($stock_id);

        $this->fpdf->SetFont('Helvetica','B',11);
        $this->fpdf->Cell(90);
        $this->fpdf->Cell(10,15, 'STOCK #'.$stock_id,0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Helvetica','B',9);
        $this->fpdf->Cell(32,4,'Stock ID : '.$stock_info->stock_id,0,0,0);
        $this->fpdf->Ln();
        $this->fpdf->Cell(12);
        $this->fpdf->Cell(32,4,'Stock Date : '.$stock_info->stock_date,0,0,0);
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->Ln();
        $this->fpdf->Cell(12);
        $this->fpdf->Cell(24,4,'Location : '.$stock_info->name,0,0,0);
        $this->fpdf->Ln();
        $this->fpdf->Cell(10);
        $this->fpdf->SetFont('Helvetica','',9);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFillColor(153,153,153);
        $this->fpdf->SetTextColor(255);
        $this->fpdf->SetDrawColor(89,89,89);
        $this->fpdf->SetLineWidth(.1);
        $this->fpdf->SetFont('','B');
        $w = array(7,30,30,30,30,30,20);
        for($i=0;$i<count($header);$i++)
            $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->fpdf->Ln();
        $this->fpdf->SetFillColor(224,235,255);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetFont('');
        $fill = false;
        foreach($data as $row)
        {
            $this->fpdf->Cell(10);
            $this->fpdf->Cell($w[0],6,$row->item_id,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[1],6,$row->item_name,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[2],6,$row->brand,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[3],6,$row->description,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[4],6,$row->cost_price,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[5],6,$row->stock_unit_price,'LR',0,'L',$fill);
            $this->fpdf->Cell($w[6],6,$row->quantity_in,'LR',0,'L',$fill);
            $this->fpdf->Ln();
            $fill = !$fill;
        }
        $this->fpdf->Cell(10);
        $this->fpdf->Cell(array_sum($w),0,'','T');
        $this->footer();
        $this->fpdf->Output();
    }
}