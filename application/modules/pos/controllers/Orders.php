<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 9/6/18
 * Time: 8:39 PM
 */

class Orders extends Pos_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('pos/order', 'pos/item','admin/category', 'pos/store', 'admin/supplier', 'admin/employee', 'admin/unit', 'admin/order'));

        $this->load->helper(array('form'));
    }

    public function index()
    {
        $data['page'] = $this->config->item('byton_ims_template_dir_pos') . "items/orders";
        $data['orders'] = $this->order->get_all();
        $data['employees'] = $this->employee->get_all();
        $data['title'] = "ORDERS";
        $this->load->view($this->_container, $data);
//        var_dump($data['orders']);
    }

    public function view($order_id=-1) {
        $data['order_info'] = $this->order->get_info($order_id);
        $data['order_items'] = $this->order->get_order_items($order_id);
        $data['employees'] = $this->employee->get_all();
        $data['item_info'] = $this->item->get_all();
        $data['stores'] = $this->store->get_all();
        $data['units'] = $this->unit->get_all();
        $this->load->view('items/new_order_form', $data);
    }

    public function get_item($item_id) {
        $item = $this->item->get_info($item_id);

        echo json_encode($item);
    }
    public function save()
    {
        $order_info = $this->input->post('order_info');
        $order_items = $this->input->post('order_items');
//        $order_info = '{ref_number: "1536539217", ordered_by: "16"}';
//        $order_items = '[{"order_id":"1536539217","item":"119","quantity":"50","store":"1"}]';
        $order_items = json_decode($order_items,true);


        if($this->order->save_order($order_info, $order_items) == true)
        {
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }
    }

    function Header()
    {
        $this->fpdf->Image('assets/images/byton_logo.png',10,6,40);
        $this->fpdf->SetFont('Arial','B',22);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,6,'BYTON GROUP LIMITED',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,5,'P.O Box 3314 MOB:0755 480057, 0658 480057, 0745 800802, 0766 800802',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,3,'MWANJELWA-MBEYA, TUNDUMA ROAD OPPOSITE MWANJELWA NEW MARKET',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(110);
        $this->fpdf->Cell(10,3,'Dealers in: Stationaries, Lamination, Binding, Printing all types of Cards,Book Sales, Ink Refills, e.t.c',0,0,'C');
        $this->fpdf->SetLineWidth(0.6);
        $this->fpdf->Line(1,28,209,28);
        $this->fpdf->Ln();
    }
    function Footer()
    {
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(10);
        date_default_timezone_set("Africa/Nairobi");
        $this->fpdf->Cell(0,10,date("F d, Y h:i A",time()),0,0,'L');

        $this->fpdf->Ln();
        $this->fpdf->SetY(-15);
        $this->fpdf->SetFont('Arial','I',8);
        $this->fpdf->Cell(0,10,'Page '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
    }

    function print_order($order_id)
    {
        $this->fpdf->AddPage('P');
        $this->header();
        $header = array('No.','DESCRIPTION','QTY', 'STORE');

        $data = $this->order->get_order_items($order_id);
        $order = $this->order->get_info_by_ref($order_id);
        $employees = $this->employee->get_all();
        $store = $this->store-> get_all();

        $this->fpdf->SetFont('Times','B',14);
        $this->fpdf->Cell(90);
        $this->fpdf->Cell(10,15, 'ORDER FORM',0,0,'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','B',11);
        $this->fpdf->SetXY( 20 , 40 );
        $this->fpdf->Cell(20, 4, "REF # : ", '', '', "L");
        $this->fpdf->SetFont('Times','',11);
        $this->fpdf->SetXY( 45 , 40 );
        $this->fpdf->Cell(20, 4,$order_id, '', '', "L");
        $this->fpdf->SetFont('Times','B',11);
        $this->fpdf->SetXY( 135 , 40 );
        $this->fpdf->Cell(20, 4, "DATE : ", '', '', "L");
        $this->fpdf->SetFont('Times','',11);
        $this->fpdf->SetXY( 150 , 40 );
        $this->fpdf->Cell(20, 4,$order->order_date, '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->SetXY(20,52);
        $this->fpdf->SetFillColor(244,247, 252);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetDrawColor(0);
        $this->fpdf->SetLineWidth(.2);
        $w = array(12,100,12,50);
        for($i=0;$i<count($header);$i++)
            $this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->SetFillColor(224,235,255);
        $this->fpdf->SetTextColor(0);
        $fill = false;
        $count = 0;
        foreach($data as $row)
        {
            $count++;

            $this->fpdf->Cell(10);
            $this->fpdf->Cell($w[0],6,$count,'LR',0,'L');
            $this->fpdf->Cell($w[1],6,$row->item_name,'LR',0,'L');
            $this->fpdf->Cell($w[2],6,$row->quantity,'LR',0,'L',$fill);
            foreach ($store as $str):
                if($row->store == $str->id)
                $this->fpdf->Cell($w[3],6,$str->name,'LR',0,'L',$fill);
            endforeach;
            $this->fpdf->Ln();
            $fill = !$fill;
        }
        $this->fpdf->Cell(10);
        $this->fpdf->Cell(array_sum($w),0,'','T');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->SetXY(20,230);
        $this->fpdf->Cell(20, 4,"ORDERED BY", '', '', "L");
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->SetXY(20,240);
        foreach ($employees as $employee):
            if($order->ordered_by == $employee->id):
                $this->fpdf->Cell(40, 1,strtoupper($employee->first_name." ".$employee->last_name), '', '', "L");
                $this->fpdf->Ln();

            endif;
        endforeach;
        $this->fpdf->SetXY(20,250);
        $this->fpdf->Cell(60, 4, "___________________________", '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetXY(20,255);
        $this->fpdf->Cell(60, 4, "Signature", '', '', "L");
        $this->fpdf->SetFont('Times','',10);
        $this->fpdf->SetXY(140,230);
        $this->fpdf->Cell(20, 4,"APPROVED & PROCESSED BY", '', '', "L");
        $this->fpdf->SetFont('Times','B',10);
        $this->fpdf->SetXY(140,240);
        foreach ($employees as $employee):
            if($order->approved_by == $employee->id):
                $this->fpdf->Cell(40, 1,strtoupper($employee->first_name." ".$employee->last_name), '', '', "L");
                $this->fpdf->Ln();

            endif;
        endforeach;
        $this->fpdf->SetXY(140,250);
        $this->fpdf->Cell(60, 4, "___________________________", '', '', "L");
        $this->fpdf->Ln();
        $this->fpdf->SetXY(140,255);
        $this->fpdf->Cell(60, 4, "Signature", '', '', "L");

        $this->footer();
        $this->fpdf->Output();
    }


}