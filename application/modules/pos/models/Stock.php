<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 10/27/17
 * Time: 2:32 PM
 */?>
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Stock extends My_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
     Determines if a given person_id is an stock
     */
    function exists($stock_id)
    {
        $this->db->from('stocks');
        $this->db->where('stocks.stock_id', $stock_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    /*
	Gets information about a particular stock
	*/
    function get_stock($stock_id)
    {
        $this->db->from('stocks c');
        $this->db->join('stores s', 's.id = c.location', 'left');
        $this->db->where('c.stock_id', $stock_id );

        return $this->db->get()->result();
    }
    function get_info($stock_id)
    {
        $this->db->select('c.stock_id,c.stock_date,c.location,c.transport_carriage,c.employee_id,c.comment,i.item_id,i.cost_price,i.unit_price as stock_unit_price,i.quantity_in,i.location,s.name, it.item_name,it.description,it.buying_price, it.brand, it.unit_selling_price,it.ctn_selling_price,it.outer_selling_price', FALSE);
        $this->db->from('stocks c');
        $this->db->join('stock_items i', 'c.stock_id = i.stock_id', 'left');
        $this->db->join('stores s', 's.id = c.location', 'left');
        $this->db->join('items it', 'it.id = i.item_id', 'left');
        $this->db->where('c.stock_id', $stock_id );

        return $this->db->get()->result();
    }

    function get_distinct_info($stock_id)
    {
        $this->db->select('c.stock_id,c.stock_date,c.location,c.transport_carriage,c.employee_id,c.comment,i.item_id,i.quantity_in, i.unit_price,i.location,s.name, it.item_name,it.description,it.buying_price, it.unit_selling_price,it.ctn_selling_price,it.outer_selling_price', FALSE);
        $this->db->from('stocks c');
        $this->db->join('stock_items i', 'c.stock_id = i.stock_id', 'left');
        $this->db->join('stores s', 's.id = c.location', 'left');
        $this->db->join('items it', 'it.id = i.item_id', 'left');
        $this->db->where('c.stock_id', $stock_id );
        $this->db->group_by('c.stock_id');

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $item_id is NOT an item
            $item_obj=new stdClass();

            //Get all the fields from items table
            $fields = $this->db->list_fields('stocks');

            foreach ($fields as $field)
            {
                $item_obj->$field='';
            }

            return $item_obj;
        }
    }


    /*
    Returns all the stocks
    */
    function get_all()
    {
        $this->db->from('stocks');
        $this->db->join('stores', 'stores.id = stocks.location');
        $this->db->order_by("stock_date", "asc");
        return $this->db->get()->result();
    }

    function get_all_print()
    {
        $this->db->select('i.id,i.item_name,i.brand,i.description,s.quantity_in as quantity, c.name as category, st.name as store');
        $this->db->from('items i');
        $this->db->join('categories c','c.id=i.category_id');
        $this->db->join('stock_store s','s.item_id=i.id');
        $this->db->join('stores st','s.location=st.id');
        $this->db->order_by("i.item_name", "asc");
        return $this->db->get()->result();
    }

    public function save_stock($stock_data, $stock_items){

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
            $this->db->insert('stocks', $stock_data);
            //$this->db->insert('stock_store', $stock_store);
            foreach($stock_items as $item):
                $this->db->insert('stock_items', $item);
                $query_1 = "UPDATE items SET quantity_in = quantity_in + ". $item['quantity_in']." WHERE id = ".$item['item_id'];
                $this->db->query($query_1);
                $query_2 = "INSERT INTO stock_store(item_id, quantity_in, location) SELECT item_id, SUM(quantity_in) as quantity_in, location FROM stock_items WHERE item_id = ".$item['item_id']." AND location=".$item['location']." GROUP BY item_id, location ON DUPLICATE KEY UPDATE quantity_in = quantity_in + ". $item['quantity_in'];
                $this->db->query($query_2);
            endforeach;

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function save_stock_out($stock_data, $stock_items){
        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

            foreach($stock_items as $item):
                $query_1 = "UPDATE stock_store SET quantity_in = quantity_in - ".$item['quantity'].", quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id']." AND location=".$item['store'];
                //$query_2 = "UPDATE stock_store SET quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id']." AND location=".$item['store'];
                $query_2 = "UPDATE items SET quantity_in = quantity_in - ".$item['quantity'].", quantity_out = quantity_out + ".$item['quantity']." WHERE id=".$item['item_id'];
                //$query_4 = "UPDATE items SET quantity_out = quantity_out + ".$item['quantity']." WHERE item_id=".$item['item_id'];
                if($this->db->query($query_1) && $this->db->query($query_2)){
                    $this->db->insert('stock_out_items', $item);
                }
            endforeach;
            $this->db->insert('stock_out', $stock_data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function update_stock($stock_info, $stock_items, $stock_id){

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

            $data = $this->get_info($stock_id);

            foreach($data as $item):
                $query_1 = "UPDATE stock_store SET quantity_in = IF((quantity_in - (SELECT quantity_in FROM stock_items WHERE stock_id=".$stock_id." AND item_id=".$item->item_id." AND location=".$item->location."))<0,0,quantity_in - (SELECT quantity_in FROM stock_items WHERE stock_id=".$stock_id." AND item_id=".$item->item_id." AND location=".$item->location."))";
                $this->db->query($query_1);
                $query_2 = "UPDATE items SET quantity_in = IF((quantity_in - (SELECT quantity_in FROM stock_items WHERE stock_id=".$stock_id." AND item_id=".$item->item_id."))<0,0,quantity_in - (SELECT quantity_in FROM stock_items WHERE stock_id=".$stock_id." AND item_id=".$item->item_id.")) WHERE id=".$item->item_id ;
                $this->db->query($query_2);
            endforeach;

            $this->db->where('stock_id', $stock_id);
            $this->db->update('stocks', $stock_info);

            foreach($stock_items as $item):
                $this->db->where('stock_id', $stock_id);
                $this->db->where('item_id', $item['item_id']);
                $this->db->update('stock_items', $item);
                $query_1 = "UPDATE items SET quantity_in = quantity_in + ". $item['quantity_in']." WHERE id = ".$item['item_id'];
                $this->db->query($query_1);
                $query_2 = "INSERT INTO stock_store(item_id, quantity_in, location) SELECT item_id, SUM(quantity_in) as quantity_in, location FROM stock_items WHERE item_id = ".$item['item_id']." AND location=".$item['location']." GROUP BY item_id, location ON DUPLICATE KEY UPDATE quantity_in = quantity_in + ". $item['quantity_in'];
                $this->db->query($query_2);
            endforeach;

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}