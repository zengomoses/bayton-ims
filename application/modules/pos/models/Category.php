<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Category extends My_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
     Determines if a given person_id is an category
     */
    function exists($category_id)
    {
        $this->db->from('categories');
        $this->db->where('categories.id', $category_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    /*
	Gets total of rows
	*/
    public function get_total_rows()
    {
        $this->db->from('categories');
        $this->db->where('deleted', 0);

        return $this->db->count_all_results();
    }

    /*
	Gets information about a particular category
	*/
    function get_info($category_id)
    {
        $this->db->select('c.id, c.name,COUNT(i.id) AS total', FALSE);
        $this->db->from('categories c');
        $this->db->join('items i', 'c.id = i.category_id', 'left');
        $this->db->where('c.id', $category_id );
        //$this->db->group_by('c.id');

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $item_id is NOT an item
            $item_obj=new stdClass();

            //Get all the fields from items table
            $fields = $this->db->list_fields('categories');

            foreach ($fields as $field)
            {
                $item_obj->$field='';
            }

            return $item_obj;
        }
    }


    /*
    Returns all the categories
    */
    function get_all()
    {
        $this->db->from('categories');
        $this->db->order_by("name", "asc");
        return $this->db->get()->result();
    }

    /*
     Inserts or updates a category
     */
    public function save_category(&$category_data, $category_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

            if(!$category_id || !$this->exists($category_id))
            {
                $success = $this->db->insert('categories', $category_data);
            }else{
                $success = false;
            }
        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    public function update_category(&$category_data, $category_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

                $this->db->where('id', $category_id);
                $success = $this->db->update('categories', $category_data);

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    public function delete_category($category_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

                $this->db->where('id', $category_id);
                $success = $this->db->delete('categories');

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }
}