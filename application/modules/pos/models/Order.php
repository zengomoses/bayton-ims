<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 9/6/18
 * Time: 8:38 PM
 */

class Order extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /*
	Determines if a given order_id is an order
	*/
    public function exists($order_id, $ignore_deleted = FALSE, $deleted = FALSE)
    {
        if (ctype_digit($order_id))
        {
            $this->db->from('orders');
            $this->db->where('ref_number', (int) $order_id);
            if ($ignore_deleted == FALSE)
            {
                $this->db->where('deleted', $deleted);
            }

            return ($this->db->get()->num_rows() == 1);
        }

        return FALSE;
    }

    /*
	Determines if a given order_number exists
	*/
    public function order_number_exists($order_number, $order_id = '')
    {
        $this->db->from('orders');
        $this->db->where('order_number', (string) $order_number);
        if(ctype_digit($order_id))
        {
            $this->db->where('id !=', (int) $order_id);
        }

        return ($this->db->get()->num_rows() == 1);
    }

    /*

    /*
    Returns all the orders
    */
    public function get_all()
    {
        $this->db->from('orders');
        //$this->db->order_by("order_id", "asc");
        return $this->db->get()->result();
    }

    public function get_all_by_user($user_id)
    {
        $this->db->from('orders');
        $this->db->where('ordered_by', $user_id);
        return $this->db->get()->result();
    }

    public function get_info($order_id)
    {

        $this->db->from('orders');
        $this->db->where('id', $order_id);
        return $this->db->get()->row();
    }

    public function get_info_by_ref($order_id)
    {

        $this->db->from('orders');
        $this->db->where('ref_number', $order_id);
        return $this->db->get()->row();
    }


    public function save_order($order_info, $order_items)
    {
        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
//        $q = 'INSERT INTO orders(ref_number, ordered_by) VALUES("3535646","16")';
//        $this->db->query($q);
            foreach($order_items as $item):
                $this->db->insert('order_items', $item);
            endforeach;
            $this->db->insert('orders', $order_info);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    /*
  Gets information about a particular order

  */
    function get_order_items($order_id)
    {
        $this->db->select('o.item, o.store, o.order_id, i.item_name, i.description, o.quantity, o.store', FALSE);
        $this->db->from('order_items o');
        $this->db->join('items i', 'i.id = o.item', 'left');
        $this->db->where('o.order_id', $order_id);

        return $this->db->get()->result();
    }



}