<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Pos_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function exists($invoice)
    {
        $this->db->from('sales_items');
        $this->db->where('sales_items.invoice', $invoice);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    /*
	Determines if a given item_number exists
	*/
    public function item_number_exists($item_number, $item_id = '')
    {
        $this->db->from('items');
        $this->db->where('item_number', (string) $item_number);
        if(ctype_digit($item_id))
        {
            $this->db->where('id !=', (int) $item_id);
        }

        return ($this->db->get()->num_rows() == 1);
    }

    /*
	Gets total of rows
	*/
    public function get_total_rows()
    {
        $this->db->from('items');
        $this->db->where('deleted', 0);

        return $this->db->count_all_results();
    }

    /*
    Returns all the items
    */
    function get_all()
    {
        $this->db->from('items');
        $this->db->order_by("item_name", "asc");
        return $this->db->get()->result();
    }

   public function search($search_data){
       if(!empty($search_data))
       {
           $this->db->select('item_name, id, description');
           $this->db->like('item_name', $search_data);

           return $this->db->get('items', 10)->result();

       }
   }

    function count_all()
    {
        $this->db->from('items');
        $this->db->where('deleted',0);
        return $this->db->count_all_results();
    }

    /*
     Inserts or updates a category
     */
    public function save_item(&$item_data, $item_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(!$item_id || !$this->exists($item_id))
        {
            $success = $this->db->insert('items', $item_data);
        }
        else
        {
            $this->db->where('item_id', $item_id);
            $success = $this->db->update('items', $item_data);
        }

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    /*
   Gets information about a particular item
   */
    function get_info_by_barcode($item_number)
    {
        $this->db->select('*', FALSE);
        $this->db->from('items');
        $this->db->where('item_number', $item_number );

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $item_id is NOT an item
            $item_obj=new stdClass();

            //Get all the fields from items table
            $fields = $this->db->list_fields('items');

            foreach ($fields as $field)
            {
                $item_obj->$field='';
            }

            return $item_obj;
        }
    }

    function get_info_by_id($item_id)
    {
        $this->db->select('*', FALSE);
        $this->db->from('items');
        $this->db->where('id', $item_id );

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $item_id is NOT an item
            $item_obj=new stdClass();

            //Get all the fields from items table
            $fields = $this->db->list_fields('items');

            foreach ($fields as $field)
            {
                $item_obj->$field='';
            }

            return $item_obj;
        }
    }

    public function save_sales($sales_data, $sales_items){

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

            $this->db->insert('sales', $sales_data);
            foreach($sales_items as $item):
                $this->db->insert('sales_items', $item);
//
//                $query = "UPDATE items SET quantity_sold = quantity_sold + ". $item['quantity_purchased'].", quantity = quantity - ". $item['quantity_purchased']."  WHERE id = ".$item['item_id']." AND quantity >=".$item['quantity_purchased'];
//                $this->db->query($query);
            endforeach;
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function get_info($sale_id)
    {
        $this->db->select('*', FALSE);
        $this->db->from('sales i');
        $this->db->join('categories c', 'i.category_id = c.id', 'left');
        $this->db->join('person p', 'i.supplier_id = p.id', 'left');
        $this->db->where('i.id', $sale_id );
        //$this->db->group_by('c.id');

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $sale_id is NOT an sale
            $sale_obj=new stdClass();

            //Get all the fields from sales table
            $fields = $this->db->list_fields('sales');

            foreach ($fields as $field)
            {
                $sale_obj->$field='';
            }

            return $sale_obj;
        }
    }

    public function get_item_units() {
        $this->db->from('units');
        return $this->db->get()->result();
    }

//    public function get_item_prices() {
////        $this->db->select('unit_selling_price,ctn_selling_price, outer_selling_price,dzn_selling_price');
//        $this->db->from('units');
////        $this->db->where('id', $id);
//        return $this->db->get()->result();
//    }

    function get_sales_items($sales_invoice)
    {
        $this->db->select('c.invoice, c.amount, c.disc,c.quantity_purchased,it.item_name,it.description,it.brand, it.unit_selling_price', FALSE);
        $this->db->from('sales_items c');
        $this->db->join('items it', 'it.id = c.item_id', 'left');
        $this->db->where('c.invoice', $sales_invoice);

        return $this->db->get()->result();
    }

    public function get_invoice_info($sale_invoice)
    {

        $this->db->from('sales');
        $this->db->where('invoice_number', $sale_invoice);
        return $this->db->get()->row();
    }
}
