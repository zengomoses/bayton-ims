<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Customer extends Person
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
     Determines if a given person_id is a customer
     */
    function exists($person_id)
    {
        $this->db->from('customers');
        $this->db->join('person', 'person.id = customers.person_id');
        $this->db->where('customers.person_id', $person_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    /*
    Returns all the customers
    */
    function get_all()
    {
        $this->db->from('customers');
        $this->db->join('person', 'customers.person_id=person.id');
        $this->db->order_by("last_name", "asc");
        return $this->db->get()->result();
    }

    /*
Gets information about a particular customer
*/
    public function get_info($customer_id)
    {
        $this->db->from('customers');
        $this->db->join('person', 'person.id = customers.person_id');
        $this->db->where('customers.person_id', $customer_id);
        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $customer_id is NOT an customer
            $person_obj = parent::get_info(-1);

            //Get all the fields from customer table
            //append those fields to base parent object, we we have a complete empty object
            foreach($this->db->list_fields('customers') as $field)
            {
                $person_obj->$field = '';
            }

            return $person_obj;
        }
    }
    public function get_custom_info($person_data)
    {
        $this->db->from('customers');
        $this->db->join('person', 'person.id = customers.person_id');
        $this->db->where('person.first_name', $person_data['first_name']);
        $this->db->where('person.last_name', $person_data['last_name']);
        $this->db->where('person.phone_number_1', $person_data['phone_number_1']);
        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $customer_id is NOT an customer
            $person_obj = parent::get_info(-1);

            //Get all the fields from customer table
            //append those fields to base parent object, we we have a complete empty object
            foreach($this->db->list_fields('customers') as $field)
            {
                $person_obj->$field = '';
            }

            return $person_obj;
        }
    }


    /*
	Inserts or updates an customer
	*/
    public function save_customer(&$person_data, &$customer_data, $customer_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(parent::save($person_data, $customer_id))
        {
            if(!$customer_id || !$this->exists($customer_id))
            {
                $customer_data['person_id'] = $customer_id = $person_data['person_id'];
                $success = $this->db->insert('customers', $customer_data);
            }
        }

        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

    public function update_customer($person_data, $customer_data, $customer_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        $this->db->where('person_id', $customer_id);
        $success = $this->db->update('customers', $customer_data);

        $this->db->where('id', $customer_id);
        $success &= $this->db->update('person', $person_data);
        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }

}
