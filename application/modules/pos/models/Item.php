<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Item extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
	Determines if a given item_id is an item
	*/
    public function exists($item_id, $ignore_deleted = FALSE, $deleted = FALSE)
    {
        if (ctype_digit($item_id))
        {
            $this->db->from('items');
            $this->db->where('id', (int) $item_id);
            if ($ignore_deleted == FALSE)
            {
                $this->db->where('deleted', $deleted);
            }

            return ($this->db->get()->num_rows() == 1);
        }

        return FALSE;
    }

    /*
	Determines if a given barcode exists
	*/
    public function barcode_exists($barcode, $item_id = '')
    {
        $this->db->from('items');
        $this->db->where('barcode', (string) $barcode);
        if(ctype_digit($item_id))
        {
            $this->db->where('id !=', (int) $item_id);
        }

        return ($this->db->get()->num_rows() == 1);
    }

    /*
	Gets total of rows
	*/
    public function get_total_rows()
    {
        $this->db->from('items');
        $this->db->where('deleted', 0);

        return $this->db->count_all_results();
    }

    /*
    Returns all the items
    */
    function get_all()
    {
        $this->db->from('items');
        $this->db->order_by("item_name", "asc");
        return $this->db->get()->result();
    }

    function get_all_print()
    {
        $this->db->select('i.id,i.item_name,i.brand,i.barcode,i.description,i.unit,i.unit_selling_price,i.ctn_selling_price,i.outer_selling_price,u.name AS unit,c.name AS category');
        $this->db->from('items i');
        $this->db->join('categories c','c.id=i.category_id');
        $this->db->join('units u','u.id=i.unit');
        $this->db->order_by("item_name", "asc");
        return $this->db->get()->result();
    }

    /*
	Get number of rows
	*/
    public function get_found_rows($search, $filters)
    {
        return $this->search($search, $filters)->num_rows();
    }


    function count_all()
    {
        $this->db->from('items');
        $this->db->where('deleted',0);
        return $this->db->count_all_results();
    }

    /*
     Inserts or updates a category
     */
    public function save_item(&$item_data, $item_units, $item_id = FALSE)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if(!$item_id || !$this->exists($item_id))
        {
            if($this->db->insert('items', $item_data)){
                $query =  $this->db->query("SELECT id FROM items WHERE item_name='".$item_data['item_name']."' AND description='".$item_data['description']."'");
                if ($query->num_rows() > 0)
                {
                    $row = $query->row();
                    $item_units['item_id'] = $row->id;
                    $success = $this->db->insert('item_units', $item_units);
                }
            };
        }
        $this->db->trans_complete();

        $success = $this->db->trans_status();

        return $success;
    }

    public function update_item($item_data, $item_units, $item_id)
    {
        $success = FALSE;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
        $this->db->where('id', $item_id);
        $success = $this->db->update('items', $item_data);

        if($success){
            $query =  $this->db->query("SELECT id FROM items WHERE item_name='".$item_data['item_name']."' AND description='".$item_data['description']."'");
            if ($query->num_rows() > 0)
            {
                $row = $query->row();
                $item_units['item_id'] = $row->id;
                $this->db->where('item_id', $item_units['item_id']);
                $success = $this->db->update('item_units', $item_units);
            }
        };
        $this->db->trans_complete();

        $success &= $this->db->trans_status();

        return $success;
    }
    /*
   Gets information about a particular item
   */
    function get_info($item_id)
    {
        $query = "SELECT i.id, i.item_name, i.category_id, i.supplier_id, i.barcode, 
                      i.description, i.unit_selling_price, i.ctn_selling_price, i.outer_selling_price, 
                      i.buying_price, i.quantity_in, i.brand, i.quantity_out, c.name, p.first_name, p.last_name,
                      u.outers_per_ctn, pcs_per_outer, un.id AS unit_id,un.name AS unit_name, un.description AS unit_desc 
                  FROM items i 
                  JOIN(categories c,person p,item_units u,units un) 
                  ON(i.category_id = c.id AND i.supplier_id = p.id AND i.id = u.item_id AND un.id = u.unit_id) 
                  WHERE i.id=".$item_id;
        $query = $this->db->query($query);

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $item_id is NOT an item
            $item_obj=new stdClass();

            //Get all the fields from items table
            $fields = $this->db->list_fields('items');

            foreach ($fields as $field)
            {
                $item_obj->$field='';
            }

            return $item_obj;
        }
    }

    public function items_taken_out(){
        $this->db->select('item_name,quantity_out');
        $this->db->from('items');
        return $this->db->get()->result();
    }
    public function items_taken_out_by_category(){

        $this->db->select('name, SUM(quantity_out) AS quantity_out');
        $this->db->from('items');
        $this->db->join('categories','items.category_id = categories.id');
        $this->db->group_by('category_id');
        $this->db->order_by('quantity_out', 'DESC');

        return $this->db->get()->result();
    }
    public function top_5_items(){
        $this->db->select('item_name, description, name AS category_name, quantity_out');
        $this->db->from('items');
        $this->db->join('categories','items.category_id = categories.id');
        $this->db->group_by('item_name');
        $this->db->order_by('quantity_out', 'DESC');
        $this->db->limit(5);

        return $this->db->get()->result();
    }

    public function items_in_store(){
        $this->db->select('i.item_name, s.quantity_in,l.name, l.id');
        $this->db->from('items i');
        $this->db->join('stock_store s','i.id = s.item_id');
        $this->db->join('stores l','s.location = l.id');

        return $this->db->get()->result();
    }

}
