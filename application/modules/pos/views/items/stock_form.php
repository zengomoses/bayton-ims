<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
    $user_level = ($this->session->userdata['logged_in']['level']);
}else {
    header("location:". base_url('auth')."");
}
?>
<style>
    .modal-dialog{
        width: 800px;
    }
    input[type="text"] {
        width: 100%;
        box-sizing: border-box;
        -webkit-box-sizing:border-box;
        -moz-box-sizing: border-box;
    }
</style>
<div class="modal fade" id="add_stock">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Stock</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'stock_form'));?>
                <input id="employee_id" name="employee_id" value="<?= $user_id?>" hidden>
                <div class="tab-content">
                    <div class="tab-pane active" id="stock_info">
                        <fieldset id="stock_info">
                            <div class="form-group form-group-sm col-xs-12">
                                <?php echo form_label('Stock ID', 'stock_id', array('class'=>'control-label col-xs-2')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'text',
                                            'name'=>'stock_id',
                                            'id'=>'stock_id',
                                            'class'=>'form-control input-md',
                                            'value'=> time() ,
                                            'disabled' => 'true')
                                    );?>
                                </div>
                            </div>
                            <div class="col-xs-12">
<!--                                --><?php //echo form_label('Items', 'item', array('class'=>'control-label col-xs-12')); ?>
                                <table class="table table-bordered table-responsive table-hover table-striped table-condensed" id="items_stock">
                                    <thead style="background-color: #34495E; color: #f6f6f6">
                                    <tr>
                                        <th style="width: 200px">ITEM NAME</th>
                                        <th>C/PRICE</th>
                                        <th>U/PRICE</th>
                                        <th style="width: 100px">QTY</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="stock_items">
                                    <button type="button" class="btn btn-default addRow"><i class="fa fa-plus"></i></button>
                                    <tr class="form-group">
                                        <td>
                                            <select class="form-control" name="stock_item[0].item_id">
                                                <option selected="true" disabled="true"></option>
                                                <?php foreach ($item_info as $item):?>
                                                    <option value="<?= $item->id ;?>"><?= $item->item_name.', '.$item->description ;?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </td>
                                        <td>
                                            <input name="stock_item[0].cost_price" class="cost_price form-control" type="text">
                                        </td>
                                        <td>
                                            <input name="stock_item[0].unit_price" class="unit_price form-control" type="text">
                                        </td>
                                        <td>
                                            <input name="stock_item[0].quantity_in" class="quantity_in form-control" type="text">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-info deleteRow" disabled><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    <!-- The template for adding new field -->
                                    <tr class="form-group hide" id="itemTemplate">
                                        <td>
                                            <select class="form-control" name="item_id">
                                                <option selected="true" disabled="true"></option>
                                                <?php foreach ($item_info as $item):?>
                                                    <option value="<?= $item->id ;?>"><?= $item->item_name.', '.$item->description ;?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </td>
                                        <td>
                                            <input name="cost_price" class="cost_price form-control" type="text">
                                        </td>
                                        <td>
                                            <input name="unit_price" class="unit_price form-control" type="text">
                                        </td>
                                        <td>
                                            <input name="quantity_in" class="quantity_in form-control" type="text">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-info deleteRow"><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Transport & Carriage Cost', 'transport_carriage', array('class'=>'control-label col-xs-2')); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_input(array(
                                                'name'=>'transport_carriage',
                                                'id'=>'transport_carriage',
                                                'class'=>'form-control input-md')
                                        );?>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Location', 'location', array('class'=>'control-label col-xs-2')); ?>
                                    <div class='col-xs-8'>
                                        <select class="form-control input-sm" id="ltn" name="ltn">
                                            <option disabled selected>Select Store</option>
                                            <?php foreach ($stores as $store):?>
                                                <option value="<?= $store->id ;?>"><?= $store->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Comment', 'comment', array('class'=>'control-label col-xs-2')); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_textarea(array(
                                                'rows'=>'1',
                                                'name'=>'comment',
                                                'id'=>'comment',
                                                'class'=>'form-control input-md')
                                        );?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div><!-- tab content -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="submitbutton">Save</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var itemValidators = {
                validators: {
                    notEmpty: {
                        //message: 'The item name is required'
                    }
                }
            },
            costValidators = {
                validators: {
                    notEmpty: {
                        //message: 'The price is required'
                    },
                    numeric: {
                        //message: 'The price must be a numeric number'
                    }
                }
            },
            unitValidators = {
                validators: {
                    notEmpty: {
                        // message: 'The price is required'
                    },
                    numeric: {
                        //message: 'The price must be a numeric number'
                    }
                }
            },

            quantityValidators = {
                validators: {
                    notEmpty: {
                        //message: 'The quantity is required'
                    },
                    numeric: {
                        //message: 'The quantity must be a numeric number'
                    }
                }
            },
            itemIndex = 0;

        $('#stock_form').bootstrapValidator({
            container : 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'stock_item[0].item_id': itemValidators,
                'stock_item[0].cost_price': costValidators,
                'stock_item[0].unit_price': unitValidators,
                'stock_item[0].quantity_in': quantityValidators,
                location: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the location in which to store the stock'
                        }
                    }
                },
                comment: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },
                transport_carriage: {
                    validators: {
                        numeric: {
                            message: 'This must contain only numeric figures'
                        }
                    }
                }
            }
        })

        // Add button click handler
        .on('click', '.addRow', function() {
            itemIndex++;
            var $template = $('#itemTemplate'),
                $clone    = $template
                    .clone()
                    .removeClass('hide')
                    .removeAttr('id')
                    .attr('item-index', itemIndex)
                    .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="item_id"]').attr('name', 'stock_item[' + itemIndex + '].item_id').end()
                .find('[name="cost_price"]').attr('name', 'stock_item[' + itemIndex + '].cost_price').end()
                .find('[name="unit_price"]').attr('name', 'stock_item[' + itemIndex + '].unit_price').end()
                .find('[name="quantity_in"]').attr('name', 'stock_item[' + itemIndex + '].quantity_in').end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#stock_form')
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].item_id', itemValidators)
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].cost_price', costValidators)
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].unit_price', unitValidators)
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].quantity_in', quantityValidators);

        })

        // Remove button click handler
        .on('click', '.deleteRow', function() {
            var $row  = $(this).parents('.form-group'),
                index = $row.attr('data-item-index');

            // Remove fields
            $('#stock_form')
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].item_id"]'))
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].cost_price"]'))
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].unit_price"]'))
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].quantity_in"]'));

            // Remove element containing the fields
            $row.remove();
        });

        $('#edit_stock_form').bootstrapValidator({
            container : 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'stock_item[0].item_id': itemValidators,
                'stock_item[0].cost_price': costValidators,
                'stock_item[0].unit_price': unitValidators,
                'stock_item[0].quantity_in': quantityValidators,
                ltn: {
                    validators: {
                        notEmpty: {
                            message: 'Please select the location in which to store the stock'
                        }
                    }
                },
                comment: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },
                transport_carriage: {
                    validators: {
                        numeric: {
                            message: 'This must contain only numeric figures'
                        }
                    }
                }
            }
        })

        // Add button click handler
        .on('click', '.addRow', function() {
            itemIndex++;
            var $template = $('#itemTemplate'),
                $clone    = $template
                    .clone()
                    .removeClass('hide')
                    .removeAttr('id')
                    .attr('item-index', itemIndex)
                    .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="item_id"]').attr('name', 'stock_item[' + itemIndex + '].item_id').end()
                .find('[name="cost_price"]').attr('name', 'stock_item[' + itemIndex + '].cost_price').end()
                .find('[name="unit_price"]').attr('name', 'stock_item[' + itemIndex + '].unit_price').end()
                .find('[name="quantity_in"]').attr('name', 'stock_item[' + itemIndex + '].quantity_in').end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#edit_stock_form')
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].item_id', itemValidators)
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].cost_price', costValidators)
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].unit_price', unitValidators)
                .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].quantity_in', quantityValidators);

        })

        // Remove button click handler
        .on('click', '.deleteRow', function() {
            var $row  = $(this).parents('.form-group'),
                index = $row.attr('data-item-index');

            // Remove fields
            $('#edit_stock_form')
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].item_id"]'))
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].cost_price"]'))
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].unit_price"]'))
                .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].quantity_in"]'));

            // Remove element containing the fields
            $row.remove();
        });
    });

    $(document).ready(function(){
        $("#submitbutton").on('click', function(e){
            var validator = $('#stock_form').data('bootstrapValidator');
            var item_id = $('input.item_id').val();
            validator.validate();
            if (validator.isValid()) {
                var employee_id = $('#employee_id').val();
                var stock_id = $('#stock_id').val();
                var stock_date = '<?= date('Y-m-d');?>';
                var transport_carriage = $('#transport_carriage').val();
                var ltn = $('#ltn').val();
                var comment = $('#comment').val();

                var stock_info = {
                    'employee_id': employee_id,
                    'stock_id': stock_id,
                    'stock_date': stock_date,
                    'transport_carriage': transport_carriage,
                    'location': ltn,
                    'comment': comment
                };

                var TableData = new Array();
                $('#stock_items tr').not('.hide').each(function (index, tr) {

                    TableData[index] = {
                        'stock_id': stock_id,
                        'item_id': $(tr).find("td:eq(0)").find("select").val(),
                        'cost_price': $(tr).find("td:eq(1)").find("input").val(),
                        'unit_price': $(tr).find("td:eq(2)").find("input").val(),
                        'quantity_in': $(tr).find("td:eq(3)").find("input").val(),
                        'location': ltn
                    }
                });
                var data = JSON.stringify([TableData]);
                data = data.substring(0, data.length - 1).substring(1, data.length);
                console.log(stock_info);
                console.log(data);
                swal({
                        title: "Confirm",
                        text: "You are about to submit the Stock!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        confirmButtonText: 'Continue',
                        cancelButtonText: "Cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                dataType: "JSON",
                                url: "<?php echo base_url(); ?>pos/stocks/save/",
                                data: {data: data, stock_info: stock_info},
                                success: function (response) {
                                    //response = $.parseJSON(response);
                                    if (response === "true") {
                                        swal('Submitted!', 'Stock has been submitted successfully', 'success');
                                        window.setTimeout(function () {
                                            $(this).closest('form').find("input[type=text], textarea").val("");
                                            location.reload();
                                        }, 3000);
                                    } else if (response === "false") {
                                        swal('Error!', 'Something went wrong. Please Try again', 'error');
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else if (response === "1") {
                                        swal('Error!', 'The Stock record already exists', 'error');
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    }
                                }
                            });
                        }
                    }
                );
                e.preventDefault;
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    })
</script>
<div class="modal fade" id="view_stock">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--           modal header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">View Stock</h4>
            </div>
            <!--                 modal body-->
            <div class="modal-body">
                <div class="row">
                    <div class="panel panel-default mypanel">
                        <div class="panel-heading">
                            <h3 class="panel-title">StockDetails</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-responsive table-bordered table-striped">
                                        <tbody>
                                        <?php foreach ($stock_info as $stock):?>
                                            <tr>
                                                <th>Stock ID</th>
                                                <td class="stock_id"><?= $stock->stock_id ?></td>
                                            </tr>
                                            <tr>
                                                <th>Stock Date</th>
                                                <td><?= $stock->stock_date ?></td>
                                            </tr>
                                            <tr>
                                                <th>Location</th>
                                                <td><?= $stock->name ?></td>
                                            </tr>
<!--                                            <tr>-->
<!--                                                <th>Transport & Carriage</th>-->
<!--                                                <td>--><?//= $stock->transport_carriage ?><!--</td>-->
<!--                                            </tr>-->
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                                        <thead style="background-color: #34495E; color: #f6f6f6">
                                        <tr>
                                            <th>ID</th>
                                            <th>ITEM NAME</th>
                                            <th>BRAND</th>
                                            <th>DESCRIPTION</th>
                                            <th>COST PRICE</th>
                                            <th>UNIT PRICE</th>
                                            <th>QUANTITY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($items_info as $stock):?>
                                            <tr>
                                                <td><?= $stock->item_id;?></td>
                                                <td><?= $stock->item_name?></td>
                                                <td><?= $stock->brand?></td>
                                                <td><?= $stock->description?></td>
                                                <td><?= $stock->cost_price?></td>
                                                <td><?= $stock->stock_unit_price?></td>
                                                <td><?= $stock->quantity_in?></td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-10">
                    <?php foreach ($stock_info as $stock):?>
                        <form action="<?= base_url('pos/stocks/stock/').$stock->stock_id?>" target="_blank">
                            <button class="btn btn-danger btn-md pull-right print-btn" style="font-size: small; font-weight: bold">
                                <i class="fa fa-lg fa-print"></i> Print
                            </button>
                        </form>
                    <?php endforeach;?>
                </div>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>