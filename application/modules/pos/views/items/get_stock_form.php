<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
    $user_level = ($this->session->userdata['logged_in']['level']);
}else {
    header("location:". base_url('auth')."");
}
?>
<style>
    .modal-dialog{
        width: 800px;
    }
    input[type="text"] {
        width: 100%;
        box-sizing: border-box;
        -webkit-box-sizing:border-box;
        -moz-box-sizing: border-box;
    }
</style>
<div class="modal fade" id="get_stock">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Stock Out Form</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'stock_form'));?>
                <input id="employee_id" name="employee_id" value="<?= $user_id?>" hidden>
                <div class="tab-content">
                    <div class="tab-pane active" id="stock_info">
                        <fieldset id="stock_info">
                            <div class="form-group form-group-sm col-xs-12" hidden>
                                <?php echo form_label('Stock ID', 'stock_id', array('class'=>'control-label col-xs-2')); ?>
                                <div class='col-xs-8'>
                                    <?php echo form_input(array(
                                            'type'=>'text',
                                            'name'=>'stock_id',
                                            'id'=>'stock_id',
                                            'class'=>'form-control input-md',
                                            'value'=> time() ,
                                            'disabled' => 'true')
                                    );?>
                                </div>
                            </div>
                            <div class="col-xs-12">
<!--                                --><?php //echo form_label('Items', 'item', array('class'=>'control-label col-xs-12')); ?>
                                <table class="table table-bordered table-responsive table-hover table-striped table-condensed" id="items_stock">
                                    <thead style="background-color: #34495E; color: #f6f6f6">
                                    <tr>
                                        <th style="width: 400px">ITEM NAME</th>
                                        <th>STORE</th>
                                        <th style="width: 100px">QUANTITY</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="stock_items">
                                    <button type="button" class="btn btn-default addRow"><i class="fa fa-plus"></i></button>
                                    <tr class="form-group">
                                        <td>
                                            <select class="form-control" name="stock_item[0].item_id">
                                                <option selected="true" disabled="true"></option>
                                                <?php foreach ($item_info as $item):?>
                                                    <option value="<?= $item->id ;?>"><?= $item->item_name.', '.$item->description ;?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="stock_item[0].store" class="store form-control">
                                                <option selected="true" disabled="true"></option>
                                                <?php foreach ($stores as $store):?>
                                                    <option value="<?= $store->id ;?>"><?= $store->name?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </td>
                                        <td>
                                            <input name="stock_item[0].quantity" class="quantity form-control" type="text">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-info deleteRow" disabled><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    <!-- The template for adding new field -->
                                    <tr class="form-group hide" id="itemTemplate">
                                        <td>
                                            <select class="form-control" name="item_id">
                                                <option selected="true" disabled="true"></option>
                                                <?php foreach ($item_info as $item):?>
                                                    <option value="<?= $item->id ;?>"><?= $item->item_name.', '.$item->description ;?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </td>
                                        <td>
                                            <input name="store" class="store form-control" type="text">
                                        </td>
                                        <td>
                                            <input name="quantity" class="quantity form-control" type="text">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-info deleteRow"><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                    </div>
                </div><!-- tab content -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="submitbutton">Save</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var itemValidators = {
                validators: {
                    notEmpty: {
                        //message: 'The item name is required'
                    }
                }
            },
            storeValidators = {
                validators: {
                    notEmpty: {
                        //message: 'The item name is required'
                    }
                }
            },

            quantityValidators = {
                validators: {
                    notEmpty: {
                        //message: 'The quantity is required'
                    },
                    numeric: {
                        //message: 'The quantity must be a numeric number'
                    }
                }
            },
            itemIndex = 0;

        $('#stock_form').bootstrapValidator({
            container : 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'stock_item[0].item_id': itemValidators,
                'stock_item[0].store': storeValidators,
                'stock_item[0].quantity': quantityValidators
            }
        })

        // Add button click handler
            .on('click', '.addRow', function() {
                itemIndex++;
                var $template = $('#itemTemplate'),
                    $clone    = $template
                        .clone()
                        .removeClass('hide')
                        .removeAttr('id')
                        .attr('item-index', itemIndex)
                        .insertBefore($template);

                // Update the name attributes
                $clone
                    .find('[name="item_id"]').attr('name', 'stock_item[' + itemIndex + '].item_id').end()
                    .find('[name="store"]').attr('name', 'stock_item[' + itemIndex + '].store').end()
                    .find('[name="quantity"]').attr('name', 'stock_item[' + itemIndex + '].quantity').end();

                // Add new fields
                // Note that we also pass the validator rules for new field as the third parameter
                $('#stock_form')
                    .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].item_id', itemValidators)
                    .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].store', storeValidators)
                    .bootstrapValidator('addField', 'stock_item[' + itemIndex + '].quantity', quantityValidators);

            })

            // Remove button click handler
            .on('click', '.deleteRow', function() {
                var $row  = $(this).parents('.form-group'),
                    index = $row.attr('data-item-index');

                // Remove fields
                $('#stock_form')
                    .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].item_id"]'))
                    .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].store"]'))
                    .bootstrapValidator('removeField', $row.find('[name="stock_item[' + index + '].quantity"]'));

                // Remove element containing the fields
                $row.remove();
            });
    });

    $(document).ready(function(){
        $("#submitbutton").on('click', function(e){
            var validator = $('#stock_form').data('bootstrapValidator');
            var item_id = $('input.item_id').val();
            validator.validate();
            if (validator.isValid()) {
                var employee_id = $('#employee_id').val();
                var stock_id = $('#stock_id').val();
                var stock_date = '<?= date('Y-m-d');?>';
                var stock_info = {
                    'employee_id': employee_id,
                    'stock_id': stock_id,
                    'stock_date': stock_date
                };

                var TableData = new Array();
                $('#stock_items tr').not('.hide').each(function (index, tr) {

                    TableData[index] = {
                        'stock_id': stock_id,
                        'item_id': $(tr).find("td:eq(0)").find("select").val(),
                        'store': $(tr).find("td:eq(1)").find("select").val(),
                        'quantity': $(tr).find("td:eq(2)").find("input").val(),
                    }
                });
                var data = JSON.stringify([TableData]);
                data = data.substring(0, data.length - 1).substring(1, data.length);
                console.log(stock_info);
                console.log(data);
                swal({
                        title: "Confirm",
                        text: "You are about to submit the Stock Out Info!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        confirmButtonText: 'Continue',
                        cancelButtonText: "Cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                dataType: "JSON",
                                url: "<?php echo base_url(); ?>pos/stocks/save_stock_out/",
                                data: {data: data, stock_info: stock_info},
                                success: function (response) {
                                    //response = $.parseJSON(response);
                                    if (response === "true") {
                                        swal('Submitted!', 'Stock has been submitted successfully', 'success');
                                        window.setTimeout(function () {
                                            $(this).closest('form').find("input[type=text], textarea").val("");
                                            location.reload();
                                        }, 3000);
                                    } else if (response === "false") {
                                        swal('Error!', 'Something went wrong. Please Try again', 'error');
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else if (response === "1") {
                                        swal('Error!', 'The Stock record already exists', 'error');
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    }
                                }
                            });
                        }
                    }
                );
                e.preventDefault;
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    })
</script>