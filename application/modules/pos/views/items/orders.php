<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
    $user_level = ($this->session->userdata['logged_in']['level']);
}else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-home">&nbsp;MANAGE ORDERS</i>
        </div>
        <div class="col-md-6">
            <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-refresh"></i> Refresh Page
            </button>
            <button class="btn btn-success btn-md pull-right new-order-btn" data-target="#add_order" data-toggle="modal" style="font-size: small; font-weight: bold">
                <i class="fa fa-lg fa-plus-circle"></i> New Order Request
            </button>
        </div>
    </div><hr/>

    <div class="panel panel-default">
        <div class="panel-body pannel-contents">
            <div class="row">
                <table id="myData" class="table table-bordered table-responsive table-hover table-striped table-condensed ">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                    <tr>
                        <th>ID</th>
                        <th>REF #</th>
                        <th>DATE CREATED</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($orders as $order):?>
                        <tr>
                            <td><?= $order->id;?></td>
                            <td class="ref_number"><?= $order->ref_number;?></td>
                            <td><?= $order->order_date;?></td>
                            <td><?= $order->status;?></td>
                            <td>
                                <?php if($order->status == "Approved"):?>
                                    <button class="btn btn-xs btn-primary btn-order-print">
                                        <i class="fa fa-eye">&nbsp;</i>Print Order
                                    </button>
                                <?php else:?>
                                    <button class="btn btn-xs btn-primary btn-order-print" disabled>
                                        <i class="fa fa-eye">&nbsp;</i>Print Order
                                    </button>
                                <?php endif;?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(".new-order-btn").click(function () {
        // get needed html
        $.get("<?= base_url('pos/orders/view/')?>", function (result) {
            // append response to body
            $('body').append(result);
            // open modal
            $('#add_order').modal(/* options object here*/);
            $('#add_order').on('hidden.bs.modal', function(){
                window.location.href = "<?= base_url('pos/orders/')?>";
            });
        });
    });

    //
    //$(".get-order-btn").click(function () {
    //    // get needed html
    //    $.get("<?//= base_url('pos/orders/view/')?>//", function (result) {
    //        // append response to body
    //        $('body').append(result);
    //        // open modal
    //        $('#get_order').modal(/* options object here*/);
    //        $('#get_order').on('hidden.bs.modal', function(){
    //            window.location.href = "<?//= base_url('pos/orders')?>//";
    //        });
    //    });
    //});
    //$(".edit-order-btn").click(function () {
    //    var order_id = $(this).closest("tr").find(".order_id").text();
    //    // get needed html
    //    $.get("<?//= base_url('pos/orders/view_edit/')?>//" + order_id, function (result) {
    //        // append response to body
    //        $('body').append(result);
    //        // open modal
    //        $('#edit_order').modal(/* options object here*/);
    //        $('#edit_order').on('hidden.bs.modal', function(){
    //            window.location.href = "<?//= base_url('pos/orders')?>//";
    //        });
    //    });
    //});
    //
    //$(".view-order-btn").click(function () {
    //    var order_id = $(this).closest("tr").find(".order_id").text();
    //    // get needed html
    //    $.get("<?//= base_url('pos/orders/view/')?>//" + order_id, function (result) {
    //        // append response to body
    //        $('body').append(result);
    //        // open modal
    //        $('#view_order').modal(/* options object here*/);
    //        $('#view_order').on('hidden.bs.modal', function(){
    //            window.location.href = "<?//= base_url('pos/orders')?>//";
    //        });
    //    });
    //});

    $('.btn-order-print').on('click', function(){
        var ref_number = $(this).closest("tr").find(".ref_number").text();
        window.open("<?= base_url('pos/orders/print_order/')?>"+ ref_number, 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=0,left=0,top=0');
        window.location.assign('<?=base_url('pos/orders')?>');
    });
</script>


