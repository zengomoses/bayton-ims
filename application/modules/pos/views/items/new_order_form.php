<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
    $user_level = ($this->session->userdata['logged_in']['level']);
}else {
    header("location:". base_url('auth')."");
}
?>
<style>
    .modal-dialog{
        width: 800px;
    }
    input[type="text"] {
        width: 100%;
        box-sizing: border-box;
        -webkit-box-sizing:border-box;
        -moz-box-sizing: border-box;
    }
</style>
<div class="modal fade" id="add_order">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Order Form</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class='col-md-8' id="item_select">
                            <label>SELECT ITEM</label>
                            <select class=" form-control input-md item selectpicker" data-live-search="true" id="item" name="item">
                                <option selected disabled></option>
                                <?php foreach ($item_info as $item):?>
                                    <option value="<?= $item->id ;?>"><?= $item->item_name.', '.$item->description ;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
<!--                        <div class='col-md-2' style="margin-top:25px">-->
<!--                            <button class="btn btn-primary btn-sm" id="addToList"><i class="fa fa-plus"></i></button>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="row">
                    <?php echo form_open('',array('id'=>'order_form'));?>
                    <input id="employee_id" name="employee_id" value="<?= $user_id?>" hidden>
                    <div class="tab-content">
                        <div class="tab-pane active" id="order_info">
                            <fieldset id="order_info">
                                <div class="form-group form-group-sm col-xs-12" hidden>
                                    <?php echo form_label('Order ID', 'order_id', array('class'=>'control-label col-xs-2')); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'order_id',
                                                'id'=>'order_id',
                                                'class'=>'form-control input-md',
                                                'value'=> time() ,
                                                'disabled' => 'true')
                                        );?>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <table class="table table-bordered table-responsive table-hover table-striped table-condensed" id="items_order">
                                        <thead style="background-color: #34495E; color: #f6f6f6">
                                        <tr>
                                            <th>ID</th>
                                            <th style="width: 400px">ITEM NAME</th>
                                            <th style="width: 100px">QUANTITY</th>
                                            <th>STORE</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="order_items">
                                        <!--                                        <td>-->
                                        <!--                                            <input name="quantity" class="quantity form-control" type="text">-->
                                        <!--                                        </td>-->
                                        <!--                                        <td>-->
                                        <!--                                            <button type="button" class="btn btn-info deleteRow"><i class="fa fa-minus"></i></button>-->
                                        <!--                                        </td>-->

                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </div><!-- tab content -->

                </div>
                <?php echo form_close();?>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="submitbutton">Save</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.selectpicker').selectpicker({
            style: 'btn-default',
            size: 4
        });

        $('#item_select').bootstrapValidator({

            feedbackIcons: {
                // valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                item: {
                    validators: {
                        notEmpty: {
                            message: 'Please select item to add to order list'
                        }
                    }
                }
            }
        });

        $("#item").on("change", function () {
            var validator = $('#item_select').data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var id = $(this).val();
                $.getJSON("<?= base_url('pos/orders/get_item/')?>" + id, function (data) {
                    $("#order_items").append(
                        "<tr>" +
                        "<td>" + data.id + "</td>" +
                        "<td>" + data.item_name + ", " + data.description + "</td>" +
                        "<td><input type=\"number\" class=\"form-control\" min=\"1\" value=\"1\"></td>" +
                        "<td><select name=\"store\" id=\"store\" class=\"store form-control\">\n" +
                        "       <option selected=\"true\" disabled=\"true\"></option>\n" +
                        "       <?php foreach ($stores as $store):?>\n" +
                        "       <option value=\"<?= $store->id;?>\"><?= $store->name?></option>\n" +
                        "       <?php endforeach;?>\n" +
                        "    </select>" +
                        "</td>" +
                        "<td> <button type=\"button\" class=\"btn btn-info deleteRow\"><i class=\"fa fa-minus\"></i></button></td>" +
                        "</tr>"
                    )
                })
            }
        });

        $('#order_items').on('click', 'button.deleteRow', function() {
            $(this).parents('tr').remove();
        });

        $("#submitbutton").on('click', function(e){
            var employee_id = $('#employee_id').val();
            var ref_number = $('#order_id').val();
            //var order_date = '<?//= date('Y-m-d');?>//';
            var order_info = {
                'ref_number': ref_number,
                'ordered_by': employee_id
                // 'order_date': order_date
            };

            var TableData = new Array();
            $('#order_items tr').not('.hide').each(function (index, tr) {

                TableData[index] = {
                    'order_id': ref_number,
                    'item': $(tr).find("td:eq(0)").text(),
                    'quantity': $(tr).find("td:eq(2)").find("input").val(),
                    'store': $(tr).find("td:eq(3)").find("select").val()
                }
            });
            var data = JSON.stringify([TableData]);
            data = data.substring(0, data.length - 1).substring(1, data.length);
            console.log(order_info);
            console.log(data);
            swal({
                    title: "Confirm",
                    text: "You are about to submit the Order Out Info!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    confirmButtonText: 'Continue',
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "<?php echo base_url(); ?>pos/orders/save/",
                            data: {order_items: data, order_info: order_info},
                            success: function (response) {
                                //response = $.parseJSON(response);
                                if (response === "true") {
                                    swal('Submitted!', 'Order has been submitted successfully', 'success');
                                    window.setTimeout(function () {
                                        $(this).closest('form').find("input[type=text], textarea").val("");
                                        location.reload();
                                    }, 3000);
                                } else if (response === "false") {
                                    swal('Error!', 'Something went wrong. Please Try again', 'error');
                                    window.setTimeout(function () {
                                        location.reload();
                                    }, 3000);
                                } else if (response === "1") {
                                    swal('Error!', 'The Order record already exists', 'error');
                                    window.setTimeout(function () {
                                        location.reload();
                                    }, 3000);
                                }
                            }
                        });
                    }
                }
            );
            e.preventDefault;
        });
    })
</script>