<?php
    if(isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
        $name = ($this->session->userdata['logged_in']['name']);
        $gender = ($this->session->userdata['logged_in']['gender']);
        $username = ($this->session->userdata['logged_in']['username']);
        $user_id = ($this->session->userdata['logged_in']['id']);
    }else {
        header("location:". base_url('auth')."");
    }
?>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('First Name', 'first_name', array('class'=>'required control-label col-xs-3')); ?>
	<div class='col-xs-8'>
		<?php echo form_input(array(
				'name'=>'first_name',
				'id'=>'first_name',
				'class'=>'required form-control input-sm',)
        );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('Last Name', 'last_name', array('class'=>'required control-label col-xs-3')); ?>
	<div class='col-xs-8'>
		<?php echo form_input(array(
				'name'=>'last_name',
				'id'=>'last_name',
				'class'=>'form-control input-sm',)
        );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('Gender', 'gender', !empty($basic_version) ? array('class'=>'required control-label col-xs-3') : array('class'=>'control-label col-xs-3')); ?>
	<div class="col-xs-4">
            <label class="radio-inline">
                <?= form_radio(array(
                    'name'=>'gender',
                    'type'=>'radio',
                    'id'=>'gender',
                    'value'=>1,)
                ); ?><?= 'Male';?>
            </label>
            <label class="radio-inline">
                <?= form_radio(array(
                    'name'=>'gender',
                    'type'=>'radio',
                    'id'=>'gender',
                    'value'=>0,)
                ); ?><?= 'Female';?>
            </label>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('E-mail', 'email', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
        <?php echo form_input(array(
                'type'=>'email',
                'name'=>'email',
                'id'=>'email',
                'placeholder' => 'Optional',
                'class'=>'required form-control input-sm',)
        );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('Phone Number 1', 'phone_number', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
			<?php echo form_input(array(
					'name'=>'phone_number_1',
					'id'=>'phone_number_1',
					'class'=>'form-control input-sm',)
            );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('Phone Number 2', 'phone_number', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
			<?php echo form_input(array(
					'name'=>'phone_number_2',
					'id'=>'phone_number_2',
                    'placeholder' => 'Optional',
					'class'=>'form-control input-sm',)
            );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('Phone Number 3', 'phone_number', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
			<?php echo form_input(array(
					'name'=>'phone_number_3',
					'id'=>'phone_number_3',
                    'placeholder' => 'Optional',
					'class'=>'form-control input-sm',)
            );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('Address', 'address', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
		<?php echo form_input(array(
				'name'=>'address',
				'id'=>'address',
				'class'=>'form-control input-sm')
        );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('Region', 'region', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
		<?php echo form_input(array(
				'name'=>'region',
				'id'=>'region',
				'class'=>'form-control input-sm',)
        );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('City', 'city', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
		<?php echo form_input(array(
				'name'=>'city',
				'id'=>'city',
                'placeholder' => 'Optional',
				'class'=>'form-control input-sm',)
        );?>
	</div>
</div>
<div class="form-group form-group-sm col-xs-12">
	<?php echo form_label('District', 'district', array('class'=>'control-label col-xs-3')); ?>
	<div class='col-xs-8'>
		<?php echo form_input(array(
				'name'=>'district',
				'id'=>'district',
				'class'=>'form-control input-sm')
        );?>
	</div>
</div>

