<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    .modal-dialog{
        width: 1100px;
    }
    input[type="text"] {
        width: 100%;
        box-sizing: border-box;
        -webkit-box-sizing:border-box;
        -moz-box-sizing: border-box;
    }
</style>
<div class="modal fade" id="add_proforma_invoice">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Proformer Invoice</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<i class="fa fa-2x fa--dot" aria-hidden="true">PROFORMER INVOICE</i>
					</div>
				</div>
				<hr>
				<div class="row">
                    <div id="message"></div>
					<div class="col-md-12">
                        <div style="margin-top: -20px">
                            <form class="form-inline" method="post">
                                <div class="form-group form-group-sm">
                                    <input type="text" class="form-control" id="barcode" name="barcode" placeholder="Enter Barcode" style="font-size: medium">
                                </div>&nbsp;&nbsp;
                                <div class="form-group form-group-sm">
                                    <input type="text" list="item_suggestion_list" class="form-control" id="item_search" name="item_search" placeholder="Search by Name or ID " style="font-size: medium">
                                    <datalist id="item_suggestion_list">
                                    </datalist>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <div id="message"></div>
                        <form id="invoice_data" method="post">
                            <input id="employee_id" name="employee_id" value="<?= $user_id?>" type="hidden">                        
                            <?php echo form_input(array(
                                    'type'=>'hidden',
                                    'name'=>'invoice_num',
                                    'id'=>'invoice_num',
                                    'class'=>'form-control input-md',
                                    'value'=> time() ,
                                )
                            );?>
                                    
                            <div id="register_wrapper" class="col col-md-8">
                                <table id="register">
                                    <thead style="background-color: #34495E; color: #f6f6f6">
                                    <tr>
                                        <th style="width:3%;">#</th>
                                        <th style="width:18%;">PARTICULARS</th>
                                        <th style="width:12.5%;">TAKEN IN (PRICE)</th>
                                        <th style="width:2%;">QTY</th>
                                        <th style="width:2%;">DISC</th>
                                        <th style="width:6%;">AMOUNT</th>
                                        <th style="width:2%;"></th>
                                    </tr>
                                    </thead>
                                    <tbody id="cart_contents">
                                    </tbody>
                                </table>
                            </div>
                            <div id="overall_sale" class="col col-md-4">
                                <div class="" style="text-align:center; margin-top: -10px">
                                    <h5>Search Customer</h5>
                                    <select class=" form-control input-sm customer selectpicker" name="customer" data-live-search="true" id="customer" style="width:280px;">
                                        <option selected disabled></option>
                                        <?php foreach ($customers as $customer ):?>
                                            <?php if($customer->person_id == 1):?>
                                                <option value="<?= $customer->person_id?>" selected><?= $customer->first_name;?></option>
                                            <?php else:?>
                                                <option value="<?= $customer->person_id?>"><?= $customer->first_name." ".$customer->last_name?></option>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    </select>
                                    <h5>Or Enter Name & Phone Number</h5>
                                    <div class="row">
                                        <div class="col col-md-6" style="margin-right: -10px">
                                            <input type="text" id="customer_2" class="customer_2 form-control input-sm" name="customer_2" placeholder="First & Last Name" style="text-align:left; width: 100%; font-size: small; margin-top: 4px;">
                                        </div>
                                        <div class="col col-md-5" style="margin-right: -18px">
                                            <input type="text" id="phone_num" class="customer2 form-control input-sm" name="phone_num" placeholder="Phone Number" style="text-align:left; width: 100%; font-size: small; margin-top: 4px;">
                                        </div>
                                        <div class="col col-md-2">
                                            <button type="button" id="sv-contact" class="btn btn-xs" style="width: 100%; font-size: small; margin-top: 7px;"> <i class="fa fa-md fa-save"></i>
                                        </div>
                                        <input id="customer_id" hidden>
                                    </div>
                                </div>
                                <div id='sale_details'>
                                    <table width="100%">
                                        <tr>
                                            <td style="width:55%; ">
                                                <div class="float_left">Sub Total (TZS)</div>
                                            </td>
                                            <td style="width:45%; text-align:right;">
                                                <div class="float_left">
                                                    <input class="subtotal" style="text-align:right;font-weight:bold; width: 100%" name="subtotal">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:55%; "><div class="float_left">Discount</div></td>
                                            <td style="width:45%; text-align:right;"><div class="float_left discount" style="text-align:right;font-weight:bold;"><input class="discount" id="discount" style="text-align:right;font-weight:bold; width: 100%" name="discount"></div></td>
                                        </tr>
                                        <tr>
                                            <td style="width:55%; "><div class="float_left" >Total (TZS)</div></td>
                                            <td style="width:45%; text-align:right; "><div class="float_left" style="text-align:right;font-weight:bold;"><input class="total" id="total" style="text-align:right;font-weight:bold; width: 100%" name="total"></div></td>
                                        </tr>
                                        <tr>
                                            <td style="width:55%; "><div class="float_left" ></div></td>
                                            <td style="width:45%; text-align:left; ">
                                                <div class="float_left" style="text-align:left;font-weight:bold;">
                                                    <input type="checkbox" id="include_vat" checked> Include VAT
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:55%; "><div class="float_left" >VAT (18%)</div></td>
                                            <td style="width:45%; text-align:right; "><div class="float_left" style="text-align:right;font-weight:bold;"><input class="vat" id="vat" style="text-align:right;font-weight:bold; width: 100%" name="vat"></div></td>
                                        </tr>                                   
                                        <tr>
                                            <td style="width:55%; "><div class="float_left" >Total (TZS Incl. VAT)</div></td>
                                            <td style="width:45%; text-align:right; "><div class="float_left " style="text-align:right;font-weight:bold;"><input class="total_incl_vat" id="total_incl_vat" style="text-align:right;font-weight:bold; width: 100%" name="total_incl_vat"></div></td>
                                        </tr>
                                    </table>
                                </div>
                            
                                <div id='sale_details'>
                                    <div type="" class='btn btn-sm btn-primary' id='saveBtn' style='margin-top:10px; margin-left:40px;'>
                                        <i class="fa fa-md fa-save"></i>  Create Invoice
                                    </div>
                                    <div class='btn btn-sm btn-danger' id='cancelBtn' style='margin-top:10px;'>
                                        <i class="fa fa- fa-close"></i>  Cancel
                                    </div>
                                    <div id="Payment_Types">
                                        <div>
                                            <label>Discount % Calculator</label>
                                        </div>
                                        <table id="calculator" style="margin-bottom:5px;">
                                            <tr>
                                                <td>
                                                    <input class="amount" style="text-align:left; border-radius: 0; width:80%;" placeholder="Amount">
                                                    <span style="font-size: 25px">/</span>
                                                </td>
                                                <td>
                                                    <input class="total" style="text-align:left; border-radius: 0; width:80%;" placeholder="Out of">
                                                    <span style="font-size: 25px">=</span>
                                                </td>
                                                <td>
                                                    <input class="perc" style="text-align:left; border-radius: 0; width:80%;" placeholder="Percent">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function(){
        $('.selectpicker').selectpicker({
            style: 'btn-default',
            size: 4
        });
    });
    $('.total').on('keyup', function(){
        var amount = $('#calculator').find("[class=amount]").val();
        var total = $('#calculator').find("[class=total]").val();
        var perc = (parseFloat(amount)/parseFloat(total)) * 100;
        var percentage = isNaN(perc) ? "" : perc;
        sales_calculations();

        $('.perc').val(percentage);
    });
    $("#barcode").on("keyup",function search(e) {
        var table = $("#register");
        var item_id = $("td:first", table);
        if(e.keyCode === 32) {
            if($.trim($( this ).val()) !== ''){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('pos/pos/view_by_barcode/')?>' + $(this).val(),
                    data: $("#barcode").serialize(),
                    success: function (resp) {
                        response = $.parseJSON(resp);
                        if(response.item_info.quantity == 0){
                            swal('Oops!', 'This item is out of stock', 'error');
                        }else{
                            if (item_id.text() === response.item_info.id) {
                                swal('Warning!', 'This item already exists on the list', 'warning');
                            }else {
                                addRow("cart_contents");
                                sales_calculations();
                            }
                        }
                    }
                });
            }
            $('#barcode').val("");
            $('#barcode').focus();
        }
    });

    $("#item_search").on("keyup", function search(e){
        var table = $("#register");
        var item_id = $("td:first", table);
        if(e.keyCode === 32) {
            if($.trim($( this ).val()) !== ''){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('pos/pos/view_by_id/')?>' + $(this).val(),
                    data: $("#barcode").serialize(),
                    success: function (resp) {
                        response = $.parseJSON(resp);
                        if(response.item_info.quantity == 0){
                            swal('Oops!', 'This item is out of stock', 'error');
                        }else{
                            if (item_id.text() === response.item_info.id) {
                                swal('Warning!', 'This item already exists on the list', 'warning');
                            }else {
                                $.ajax({
                                    type: 'GET',
                                    url: '<?= base_url('pos/pos/get_units')?>',
                                    success: function (d) {

                                        data = $.parseJSON(d);
                                        console.log(data);
                                        addRow("cart_contents");
                                        sales_calculations();
                                    }
                                });
                            }
                        }
                    }
                });
            }
            $('#item_search').val("");
            $('#barcode').focus();
        }
    });

    $("#item_search").on('keyup', function(){
        var item_name = $('#item_search').val();
        var post_data = {
            'search_data': item_name,
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
        };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>pos/search/",
            data: post_data,
            success: function (data) {
                // return success
                if (data.length > 0) {
                    $('#item_suggestion_list').addClass('auto_list');
                    $('#item_suggestion_list').html(data);
                }
            }
        });
    });
    $('#customer_2').on('click', function(){
        $('#customer').val('').selectpicker('refresh');
    });

    $('#customer.selectpicker').on('change', function(){
        $('#customer_2').val('');
        $('#phone_num').val('');
    });
    $('#sv-contact').on('click', function(){
        var name = $('#customer_2').val();
        var arr = name.split(" ");
        var first_name = arr[0];
        var last_name = arr[1];
        var phone_number = $('#phone_num').val();
        $.post("<?= base_url('pos/customers/save_custom') ?>", {first_name:first_name, last_name:last_name, phone_number_1: phone_number},function(data){
            var cust = $.parseJSON(data);
            $('#customer_id').val(cust['id']);
            alert("Contact saved");
        });
    });

    $('#invoice_data').bootstrapValidator({

        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            phone_num: {
                validators: {
                    numeric:{
                        message: 'The phone number must contain digits only'
                    }
                }
            }
        }
    });
    
    $('#saveBtn').on("click", function(e){
        var validator = $('#invoice_data').data('bootstrapValidator');
        var item_id = $('input.item_id').val();
        
        validator.validate();

        if (validator.isValid()) {
            var  employee_id = $('#employee_id').val();
            var  invoice_num = $('#invoice_num').val();
            var  date_created= '<?= date('Y-m-d H:i:s');?>';
            var  customer_id = "";
            localStorage['invoice_id'] = invoice_num;

            if($('#customer').val() === null){
                customer_id = $('#customer_id').val();
            }else{
                customer_id = $('#customer').val();
            }
            var  total = $('#total').val();
            var VAT = $('#vat').val();
            var  total_VAT = $('#total_incl_vat').val();
            if(VAT == 0 ) {
                var comment = "VAT Exempted";
            }

            var invoice_data = {
                'employee_id': employee_id,
                'invoice_number': invoice_num,
                'date_created': date_created,
                'customer_id': customer_id,
                'sub_total': total,
                'discount': parseFloat(discount/100),
                'VAT': VAT,
                'total_VAT': total_VAT,
                'comment': comment
            };

            var invoiceItems = new Array();
            $('#cart_contents tr').each(function(row, tr){
                invoiceItems[row] = {
                    "invoice_number": invoice_num,
                    "item" : $(tr).find('td:eq(0)').text(),
                    "quantity" : $(tr).find("[name=qty]").val(),
                    "unit": $(tr).find("[name=price]  option:selected").text().substr(0, $(tr).find("[name=price]  option:selected").text().indexOf(' ')),
                    "unit_price": $(tr).find("[name=price]").val(),
                }
            });

            console.log(invoiceItems)
          
            swal({
                    title: "Confirm",
                    text: "You are about to create the Invoice!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    confirmButtonText: 'Continue',
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "<?php echo base_url(); ?>pos/invoices/save/",
                            data: {invoice_items: invoiceItems, invoice_data: invoice_data},
                            success: function (response) {
                                if (response === "true") {
                                    swal('Submitted!', 'Invoice "'+ invoice_num +'" has been created successfully', 'success');
                                    $('#btn-print').css('pointer-events','auto');
                                }else if(response === "false"){
                                    swal('Error!', 'Something went wrong while creating invoice. Please Try again', 'error');
                                }else if(response === "1"){
                                    swal('Error!', 'The Invoice "'+ invoice_num +'" record already exists', 'error');
                                    window.setTimeout(function(){
                                        window.location.assign('<?=base_url('pos/invoices')?>')
                                    } ,2000);
                                }
                            }
                        });
                    }
                }
            );
            e.preventDefault();
        }else{
            swal('Error!', 'Some fields have invalid entries', 'error');
        }
    });

    $(document).ready(function(){
        $('body').on('click', 'button.deleteRow', function() {
            $(this).parents('tr').remove();
            sales_calculations();
        });

        $('#btn-print').css('pointer-events','none');

        $('#btn-print').on('click', function(){
            var  invoice_num = $('#invoice_num').val();
            window.open("<?= base_url('pos/print_invoice/')?>" + invoice_num, 'Invoice', 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=0,left=0,top=0');
            window.location.assign('<?=base_url('pos')?>');
        });
 
        $("input[type=checkbox]").on("click", function(){
            if($(this).is(":not(:checked)")){
                var vat = parseFloat(0.00);
                $('.vat').val(vat);
                $('.total_incl_vat').val(vat)
            }
            if($(this).is(":checked")){
               sales_calculations();
            }
        });
    });

</script>