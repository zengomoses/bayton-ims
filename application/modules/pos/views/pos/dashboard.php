<?php
/**
 * Created by PhpStorm.
 * User: kilenga
 * Date: 8/8/17
 * Time: 2:27 PM
 */
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-2x fa-shopping-bag" aria-hidden="true">&nbsp SALES REGISTER</i>
        </div>
        <button class="btn btn-default btn-md pull-right" id="refresh-btn" onClick="window.location.assign('<?=base_url('pos')?>'); " style="font-size: medium"><i class="fa fa-lg fa-refresh"></i>  Clear Register</button>
    </div>
    <hr>
    <div style="margin-top: -20px">
        <form class="form-inline" method="post">
            <div class="form-group form-group-sm">
                <input type="text" class="form-control" id="barcode" name="barcode" placeholder="Enter Barcode" style="font-size: medium">
            </div>&nbsp;&nbsp;
            <div class="form-group form-group-sm">
                <input type="text" list="item_suggestion_list" class="form-control" id="item_search" name="item_search" placeholder="Search by Name or ID " style="font-size: medium">
                <datalist id="item_suggestion_list">
                </datalist>
            </div>
        </form>
    </div>
    <div class="row">
        <div id="message"></div>
        <form id="sales_data" method="post">
            <input id="employee_id" name="employee_id" value="<?= $user_id?>" type="hidden">
            <input id="invoice_num" name="invoice_num" value="" hidden/>
            <div id="register_wrapper" class="col col-md-8">
                <table id="register">
                    <thead style="background-color: #34495E; color: #f6f6f6">
                    <tr>
                        <th style="width:3%;">#</th>
                        <th style="width:18%;">PARTICULARS</th>
                        <th style="width:12.5%;">TAKEN IN (PRICE)</th>
                        <th style="width:2%;">QTY</th>
                        <th style="width:2%;">DISC</th>
                        <th style="width:6%;">AMOUNT</th>
                        <th style="width:2%;"></th>
                    </tr>
                    </thead>
                    <tbody id="cart_contents">
                    </tbody>
                </table>
            </div>
            <div id="overall_sale" class="col col-md-4">
                <div class="" style="text-align:center; margin-top: -10px">
                    <h5>Search Customer</h5>
                    <select class=" form-control input-sm customer selectpicker" name="customer" data-live-search="true" id="customer" style="width:280px;">
                        <option selected disabled></option>
                        <?php foreach ($customers as $customer ):?>
                            <?php if($customer->person_id == 1):?>
                                <option value="<?= $customer->person_id?>" selected><?= $customer->first_name;?></option>
                            <?php else:?>
                                <option value="<?= $customer->person_id?>"><?= $customer->first_name." ".$customer->last_name?></option>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                    <h5>Or Enter Name & Phone Number</h5>
                    <div class="row">
                        <div class="col col-md-6" style="margin-right: -10px">
                            <input type="text" id="customer_2" class="customer_2 form-control input-sm" name="customer_2" placeholder="First & Last Name" style="text-align:left; width: 100%; font-size: small; margin-top: 4px;">
                        </div>
                        <div class="col col-md-4" style="margin-right: -18px">
                            <input type="text" id="phone_num" class="customer2 form-control input-sm" name="phone_num" placeholder="Phone Number" style="text-align:left; width: 100%; font-size: small; margin-top: 4px;">
                        </div>
                        <div class="col col-md-2"  style="margin-right: 18px">
                            <button type="button" id="sv-contact" class="btn btn-xs" style="width: 100%; font-size: small; margin-top: 7px;"> <i class="fa fa-md fa-save"></i>
                        </div>
                        <input id="customer_id" hidden>
                    </div>
                </div>
                <div id='sale_details'>
                    <table width="100%">
                        <tr>
                            <td style="width:55%; ">
                                <div class="float_left">Sub Total (TZS)</div>
                            </td>
                            <td style="width:45%; text-align:right;">
                                <div class="float_left">
                                    <input class="grdtot" style="text-align:right;font-weight:bold; width: 100%" name="total">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <table width="100%">
                    <tr>
                        <td style="width:55%; "><div class="float_left">Discount</div></td>
                        <td style="width:45%; text-align:right;"><div class="float_left discount" style="text-align:right;font-weight:bold;"><input class="discount" id="discount" style="text-align:right;font-weight:bold; width: 100%" name="discount"></div></td>
                    </tr>
                    <tr>
                        <td style="width:55%; "><div class="float_left" ></div></td>
                        <td style="width:45%; text-align:left; ">
                            <div class="float_left" style="text-align:left;font-weight:bold;">
                                <input type="checkbox" id="include_vat" checked> Include VAT
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%; "><div class="float_left" >VAT (18%)</div></td>
                        <td style="width:45%; text-align:right; "><div class="float_left" style="text-align:right;font-weight:bold;"><input class="vat" id="vat" style="text-align:right;font-weight:bold; width: 100%" name="vat"></div></td>
                    </tr>                                   
                    <tr>
                        <td style="width:55%; "><div class="float_left" >Total (TZS Incl. VAT)</div></td>
                        <td style="width:45%; text-align:right; "><div class="float_left " style="text-align:right;font-weight:bold;"><input class="payout_incl_vat" id="payout_incl_vat" style="text-align:right;font-weight:bold; width: 100%" name="payout_incl_vat"></div></td>
                    </tr>
                    <tr>
                        <td style="width:55%; "><div class="float_left" >Total Payout (TZS)</div></td>
                        <td style="width:45%; text-align:right; "><div class="float_left" style="text-align:right;font-weight:bold;"><input class="payout" style="text-align:right;font-weight:bold; width: 100%;" name="payout"></div></td>
                    </tr>
                    <tr>
                        <td style="width:55%; "><div class="float_left" >Amount Paid(TZS)</div></td>
                    </tr>
                    <tr>
                        <td colspan='2' style="width:100%; text-align:right; "><div class="float_left" style="text-align:right;font-weight:bold; background-color:#EEEEEE;"><input id="paid" class="paid" style="text-align:right;font-weight:bold; width: 100%;  border-radius: 0px; font-size:40px" name="paid"></div></td>
                    </tr>
                    <tr>
                        <td style="width:55%; "><div class="float_left" >Amount Due(TZS)</div></td>
                        <td style="width:45%; text-align:right; "><div class="float_left" style="text-align:right;font-weight:bold; background-color:#EEEEEE;"><input id="bal" class="bal" style="text-align:right;font-weight:bold; width: 100%;  border-radius: 0px" name="bal"></div></td>
                    </tr>
                   
                </table>
                <!-- <div>
                    <div>Comment</div>
                    <textarea cols="1" rows="1" style="height: auto; width: 283px;" id="comment" name="comment"></textarea>
                </div> -->
                <div id='sale_details'>
                    <div type="" class='btn btn-sm btn-primary' id='saleBtn' style='margin-top:5px;'>
                        <i class="fa fa-md fa-save"></i>  Submit
                    </div>
                    <div class='btn btn-sm btn-danger' id='cancelBtn' style='margin-top:5px;'>
                        <i class="fa fa- fa-close"></i>  Cancel
                    </div>
                    <div div class='btn btn-sm btn-success' id="btn-print" style="margin-top:5px; color: #eeeeee"><i class="fa fa- fa-print"></i>  Print Invoice</div>
                    <div id="Payment_Types">
                        <div>
                            <label>Discount % Calculator</label>
                        </div>
                        <table id="calculator" style="margin-bottom:5px;">
                            <tr>
                                <td>
                                    <input class="amount" style="text-align:left; border-radius: 0; width:80%;" placeholder="Amount">
                                    <span style="font-size: 25px">/</span>
                                </td>
                                <td>
                                    <input class="total" style="text-align:left; border-radius: 0; width:80%;" placeholder="Out of">
                                    <span style="font-size: 25px">=</span>
                                </td>
                                <td>
                                    <input class="perc" style="text-align:left; border-radius: 0; width:80%;" placeholder="Percent">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="message">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Success</h3>
            </div>
            <!-- body -->
            <div class="modal-body">
                <div id="message"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.selectpicker').selectpicker({
            style: 'btn-default',
            size: 4
        });
    });
    $('.paid').on('keyup', function () {
        var paid = parseFloat($(this).val());
        var payout = parseFloat($(".payout").val());

        var bal = parseFloat(payout) - parseFloat(paid);

        bal = isNaN(bal) ? 0 : bal;

        $('.bal').val(bal.toFixed(2));
    });
    $('.total').on('keyup', function(){
        var amount = $('#calculator').find("[class=amount]").val();
        var total = $('#calculator').find("[class=total]").val();
        var perc = (parseFloat(amount)/parseFloat(total)) * 100;
        var percentage = isNaN(perc) ? "" : perc;
        sales_calculations();

        $('.perc').val(percentage);
    });
    $("#barcode").on("keyup",function search(e) {
        var table = $("#register");
        var item_id = $("td:first", table);
        if(e.keyCode === 32) {
            if($.trim($( this ).val()) !== ''){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('pos/pos/view_by_barcode/')?>' + $(this).val(),
                    data: $("#barcode").serialize(),
                    success: function (resp) {
                        response = $.parseJSON(resp);
                        if(response.item_info.quantity == 0){
                            swal('Oops!', 'This item is out of stock', 'error');
                        }else{
                            if (item_id.text() === response.item_info.id) {
                                swal('Warning!', 'This item already exists on the list', 'warning');
                            }else {
                                addRow("cart_contents");
                                sales_calculations();
                            }
                        }
                    }
                });
            }
            $('#barcode').val("");
            $('#barcode').focus();
        }
    });

    $("#item_search").on("keyup", function search(e){
        var table = $("#register");
        var item_id = $("td:first", table);
        if(e.keyCode === 32) {
            if($.trim($( this ).val()) !== ''){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('pos/pos/view_by_id/')?>' + $(this).val(),
                    data: $("#barcode").serialize(),
                    success: function (resp) {
                        response = $.parseJSON(resp);
                        if(response.item_info.quantity == 0){
                            swal('Oops!', 'This item is out of stock', 'error');
                        }else{
                            if (item_id.text() === response.item_info.id) {
                                swal('Warning!', 'This item already exists on the list', 'warning');
                            }else {
                                $.ajax({
                                    type: 'GET',
                                    url: '<?= base_url('pos/pos/get_units')?>',
                                    success: function (d) {

                                        data = $.parseJSON(d);
                                        console.log(data);
                                        addRow("cart_contents");
                                        sales_calculations();
                                    }
                                });
                            }
                        }
                    }
                });
            }
            $('#item_search').val("");
            $('#barcode').focus();
        }
    });

    $("#item_search").on('keyup', function(){
        var item_name = $('#item_search').val();
        var post_data = {
            'search_data': item_name,
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
        };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>pos/search/",
            data: post_data,
            success: function (data) {
                // return success
                if (data.length > 0) {
                    $('#item_suggestion_list').addClass('auto_list');
                    $('#item_suggestion_list').html(data);
                }
            }
        });
    });
    $('#customer_2').on('click', function(){
        $('#customer').val('').selectpicker('refresh');
    });

    $('#customer.selectpicker').on('change', function(){
        $('#customer_2').val('');
        $('#phone_num').val('');
    });
    $('#sv-contact').on('click', function(){
        var name = $('#customer_2').val();
        var arr = name.split(" ");
        var first_name = arr[0];
        var last_name = arr[1];
        var phone_number = $('#phone_num').val();
        $.post("<?= base_url('admin/customers/save_custom') ?>", {first_name:first_name, last_name:last_name, phone_number_1: phone_number},function(data){
            var cust = $.parseJSON(data);
            $('#customer_id').val(cust['id']);
            alert("Contact saved");
        });
    });
    $('#sales_data').bootstrapValidator({

        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            phone_num: {
                validators: {
                    numeric:{
                        message: 'The phone number must contain digits only'
                    }
                }
            }
        }
    });

    // $('#register tr').find("[name=price]").val(),

    $('#saleBtn').on("click", function(e){
        var validator = $('#sales_data').data('bootstrapValidator');
        var item_id = $('input.item_id').val();
        validator.validate();
        if (validator.isValid()) {
            var  employee_id = $('#employee_id').val();
            var  invoice_num = $('#invoice_num').val();
            var  sale_time = '<?= date('Y-m-d H:i:s');?>';
            var  customer_id = "";
            localStorage['invoice_id'] = invoice_num;

            if($('#customer').val() === null){
                customer_id = $('#customer_id').val();
            }else{
                customer_id = $('#customer').val();
            }
            var  sale_status = "";
            var  paid = $('#paid').val();
            var  balance = $('#bal').val();
            var  comment = $('#comment').val();
            var  discount = $('#discount').val();
            if(balance <= 0.00){
                sale_status = "0";
            }else{
                sale_status = "1";
            }
            var sales_data = {
                'employee_id': employee_id,
                'invoice_number': invoice_num,
                'sale_time': sale_time,
                'customer_id': customer_id,
                'amount_paid': paid,
                'amount_due': balance,
                'sale_status' : sale_status,
                'comment' : comment,
                'discount' : discount
            };

            var TableData = new Array();
            $('#register tr').each(function(row, tr){
                TableData[row] = {
                    "invoice": invoice_num,
                    "item_id" : $(tr).find('td:eq(0)').text(),
                    "disc" : $(tr).find("[name=disc]").val(),
                    "quantity_purchased" : $(tr).find("[name=qty]").val(),
                    "amount" : $(tr).find("[name=subtot]").val()
                }
            });
            TableData.shift();
            var TableData_2 = new Array();
            $('#register tr').each(function(row, tr){
                TableData_2[row] = {
                    "invoice": invoice_num,
                    "item_id" : $(tr).find('td:eq(0)').text(),
                    "item_name" : $(tr).find('td:eq(1)').text(),
                    "price" : $(tr).find("[name=price]").val(),
                    "disc" : $(tr).find("[name=disc]").val(),
                    "quantity_purchased" : $(tr).find("[name=qty]").val(),
                    "total" : $(tr).find("[name=subtot]").val()
                }
            });
            TableData_2.shift();
            var data = JSON.stringify([TableData]);
            data = data.substring(0, data.length - 1).substring(1, data.length);

            console.log("Data 1 ==>" + data);

            var data_2 = JSON.stringify([TableData_2]);
            data_2 = data_2.substring(0, data_2.length - 1).substring(1, data_2.length);
            console.log("Data 2 ==>" + data_2);
            swal({
                    title: "Confirm",
                    text: "You are about to submit the sales!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    confirmButtonText: 'Continue',
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "<?php echo base_url(); ?>pos/save/",
                            data: {data: data, data_2: data_2, sales_data: sales_data},
                            success: function (response) {
                                //response = $.parseJSON(response);
                                if (response === "true") {
                                    swal('Submitted!', 'Sale have been submitted successfully', 'success');
                                    $('#btn-print').css('pointer-events','auto');
                                }else if(response === "false"){
                                    swal('Error!', 'Something went wrong. Please Try again', 'error');
                                }else if(response === "1"){
                                    swal('Error!', 'The sales record already exists', 'error');
                                    window.setTimeout(function(){
                                        window.location.assign('<?=base_url('pos')?>')
                                    } ,2000);
                                }
                            }
                        });
                    }
                }
            );
            e.preventDefault();
        }else{
            swal('Error!', 'Some fields have invalid entries', 'error');
        }
    });

    $(document).ready(function(){
        $('#invoice_num').val(generateInvoiceNum());

        $('body').on('click', 'button.deleteRow', function() {
            $(this).parents('tr').remove();
            sales_calculations();
        });
    });
    $(document).ready(function(){
        $('#btn-print').css('pointer-events','none');
    });
    $('#btn-print').on('click', function(){
        var  invoice_num = $('#invoice_num').val();
        window.open("<?= base_url('pos/print_invoice/')?>" + invoice_num, 'Invoice', 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=0,left=0,top=0');
        window.location.assign('<?=base_url('pos')?>');
    });

</script>