<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 11/30/17
 * Time: 6:21 AM
**/
?>
<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
    $user_level = ($this->session->userdata['logged_in']['level']);
}else {
    header("location:". base_url('auth')."");
}
?>
<?php if($user_level == 'chief'):?>
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-6">
                <i class="fa fa-2x fa-home">&nbsp;MESSAGING</i>
            </div>
            <div class="col-md-6">
                <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                    <i class="fa fa-lg fa-refresh"></i> Refresh Page
                </button>
            </div>
        </div><hr/>

        <div class="panel panel-default">
            <div class="panel-body pannel-contents">
                <div class="row">
                    <div class="col col-md-12">
                        <ul class="nav nav-tabs" style="font-size: 16px">
                            <li class="active"><a href="#customer_sms" data-toggle="tab">Customer SMS</a></li>
                        </ul>
                        <form id="sms_form" method="post">
                            <div class="tab-content">
                                <div class="tab-pane active" id="customer_sms">
                                    <fieldset>
                                        <hr>
                                        <div class="form-group form-group-sm col-xs-12">
                                            <?php echo form_label('Recipient', 'receiver', array('class'=>'control-label col-xs-1')); ?>
                                            <div id="customer" class='col-xs-8'>
                                                <select class= 'form-control input-sm selectpicker' data-live-search="true" id='customer' name='customer[]' multiple data-size="5" data-selected-text-format="count>2">
                                                    <option value="All">All Contacts</option>
                                                    <?php foreach ($customers as $customer):?>
                                                        <option value="<?= $customer->phone_number_1?>"><?= $customer->customer_name?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-sm col-xs-12">
                                            <?php echo form_label('Message', 'message', array('class'=>'control-label col-xs-1')); ?>
                                            <div class='col-xs-8'>
                                                <textarea name="customer_message" rows="5" class="form-control input-sm"></textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" id="sendBtn">Send</button>
                                </div>
                            </div><!-- tab content -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else:?>
    <div class="right_col" role="main">

        <div class="row">
            <div class="col-md-6">
                <i class="fa fa-2x fa-home">&nbsp;MESSAGING</i>
            </div>
            <div class="col-md-6">
                <button class="btn btn-default btn-md pull-right" onClick="window.location.reload()" style="font-size: small; font-weight: bold">
                    <i class="fa fa-lg fa-refresh"></i> Refresh Page
                </button>
            </div>
        </div><hr/>

        <div class="panel panel-default">
            <div class="panel-body pannel-contents">
                <div class="row">
                    <div class="col col-md-12">
                       <div class="alert alert-info">You have no permission to access Messaging</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif?>
<script>
    function toggleSelectAll(control) {
        var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
        function valuesOf(elements) {
            return $.map(elements, function(element) {
                return element.value;
            });
        }

        if (control.data('allOptionIsSelected') != allOptionIsSelected) {
            // User clicked 'All' option
            if (allOptionIsSelected) {
                // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
                control.selectpicker('val', valuesOf(control.find('option')));
            } else {
                control.selectpicker('val', []);
            }
        } else {
            // User clicked other option
            if (allOptionIsSelected && control.val().length != control.find('option').length) {
                // All options were selected, user deselected one option
                // => unselect 'All' option
                control.selectpicker('val', valuesOf(control.find('option:selected[value!=All]')));
                allOptionIsSelected = false;
            } else if (!allOptionIsSelected && control.val().length == control.find('option').length - 1) {
                // Not all options were selected, user selected all options except 'All' option
                // => select 'All' option too
                control.selectpicker('val', valuesOf(control.find('option')));
                allOptionIsSelected = true;
            }
        }
        control.data('allOptionIsSelected', allOptionIsSelected);
    }
    $('#receiver').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');

    $("#sendBtn").click(function(e){  // passing down the event
        $.ajax({
            url:'<?= base_url('sms/send_sms')?>',
            type: 'POST',
            data: $("#sms_form").serialize(),
            success: function(response){
                //response = $.parseJSON(resp);
                if (response.success === true) {
                    swal('Success!', 'Message sent successfully', 'success');
                }else{
                    swal('Failed!', response.message , 'error');
                }
            }
        });
        e.preventDefault(); // could also use: return false;
    });
    $('a[data-toggle="tab"]').on('click', function (e) {
        $('.form-group input[type="text"]').val('');
        $('.form-group textarea').val('');
        $('.form-group select').val('').selectpicker('refresh')
    });
</script>