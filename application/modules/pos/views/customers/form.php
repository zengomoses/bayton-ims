<?php
if (isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in']['is_member'] == true) {
    $name = ($this->session->userdata['logged_in']['name']);
    $gender = ($this->session->userdata['logged_in']['gender']);
    $username = ($this->session->userdata['logged_in']['username']);
    $user_id = ($this->session->userdata['logged_in']['id']);
} else {
    header("location:". base_url('auth')."");
}
?>
<style>
    th{
        max-width: 100px;
        font-style: italic;
    }
    .mypanel{
        color: #0f0f0f;
    }
</style>
<div class="modal fade" id="add_customer">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Customer</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'customer_form'));?>
                    <div class="tab-content">
                        <div class="tab-pane active" id="customer_basic_info" style="text-align: center; background-color: #e2e2e2">
                            <hr>
                            <fieldset id="customer_basic_info">
                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Company Name', 'company_name', array('class'=>'control-label col-xs-3')); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'company_name',
                                                'id'=>'company_name',
                                                'class'=>'form-control input-sm',)
                                        );?>
                                    </div>
                                </div>
                                <?php $this->load->view("people/form_basic_info"); ?>
                            </fieldset>
                        </div>
                    </div><!-- tab content -->
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="submitbutton">Save</button>
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_customer">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Customer</h4>
            </div>
            <!-- body -->
            <div class="modal-body">
                <?php echo form_open('',array('id'=>'edit_customer_form'));?>
                    <div class="tab-content">
                        <div class="tab-pane active" id="customer_basic_info" style="text-align: center; background-color: #e2e2e2">
                            <hr>
                            <fieldset id="customer_basic_info">
                                <div class="form-group form-group-sm col-xs-12">
                                    <?php echo form_label('Company Name', 'company_name', array('class'=>'control-label col-xs-3')); ?>
                                    <div class='col-xs-8'>
                                        <?php echo form_input(array(
                                                'type'=>'text',
                                                'name'=>'company_name',
                                                'id'=>'company_name',
                                                'class'=>'form-control input-sm',
                                                'value' => $person_info->company_name)
                                        );?>
                                    </div>
                                </div>
                                <?php $this->load->view("people/form_edit_basic_info"); ?>
                            </fieldset>
                        </div>
                    </div><!-- tab content -->
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="updatebutton">Update</button>
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#customer_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                first_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter first name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                last_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter last name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: 'The gender is required'
                        }
                    }
                },
                email: {
                    validators: {
                        emailAddress: {
                            message: 'Please enter valid email address'
                        }
                    }
                },
                phone_number_1: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter phone number'
                        },
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                phone_number_2: {
                    validators: {
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                phone_number_3: {
                    validators: {
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                address: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },
                city: {
                    validators: {
                        stringLength: {
                            min: 4
                        },
                    }
                },
                region: {
                    validators: {
                        notEmpty: {
                            message: 'Please select Region'
                        }
                    }
                },
                district: {
                    validators: {
                        notEmpty: {
                            message: 'Please select District'
                        }
                    }
                },

                comments: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 200,
                            message:'Please enter at least 2 characters and no more than 200'
                        }
                    }
                },

                company_name: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },

                discount_percent: {
                    validators: {
                        regexp: {
                            regexp: /^((([0-9]|[1-9]\d)\.\d{2})|100\.00)$/,
                            message: 'The entry must be 0.00-100.00 only'
                        }
                    }
                }
            }
        });

        $("#submitbutton").click(function(e){  // passing down the event
            var validator = $('#customer_form').data('bootstrapValidator');
            validator.validate();
            var  data = $("#customer_form").find(":input").filter(
                function(index, element){
                    return $(element).val() !== '';
                }
            ).serialize();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/customers/save/')?>',
                    type: 'POST',
                    data: data,
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Customer has been added successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }
                    }
                });
                e.preventDefault();
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    });


    $(document).ready(function() {
        $('#edit_customer_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                first_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter first name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                last_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 25
                        },
                        notEmpty: {
                            message: 'Please enter last name'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The name can only consist of alphabetal letters',
                        }
                    }
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: 'The gender is required'
                        }
                    }
                },
                email: {
                    validators: {
                        emailAddress: {
                            message: 'Please enter valid email address'
                        }
                    }
                },

                phone_number_1: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter phone number'
                        },
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                phone_number_2: {
                    validators: {
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                phone_number_3: {
                    validators: {
                        phone: {
                            country: 'US',
                            message: 'Please enter a vaild phone number'
                        },
                        regexp: {
                            regexp: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                            message: 'The phone number can only consist of digits',
                        }
                    }
                },
                address: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },
                city: {
                    validators: {
                        stringLength: {
                            min: 4
                        },
                    }
                },
                region: {
                    validators: {
                        notEmpty: {
                            message: 'Please select Region'
                        }
                    }
                },
                district: {
                    validators: {
                        notEmpty: {
                            message: 'Please select District'
                        }
                    }
                },
                comments: {
                    validators: {
                        stringLength: {
                            min: 2,
                            max: 200,
                            message:'Please enter at least 2 characters and no more than 200'
                        }
                    }
                },

                company_name: {
                    validators: {
                        stringLength: {
                            min: 2
                        }
                    }
                },
            }
        });

        $("#updatebutton").click(function(e){  // passing down the event
            var validator = $('#edit_customer_form').data('bootstrapValidator');
            validator.validate();
            var  data = $("#edit_customer_form").find(":input").filter(
                function(index, element){
                    return $(element).val() !== '';
                }
            ).serialize();
            if (validator.isValid()) {
                $.ajax({
                    url:'<?= base_url('admin/customers/update/').$person_info->person_id?>',
                    type: 'POST',
                    data: data,
                    success: function(resp){
                        response = $.parseJSON(resp);
                        if (response.success) {
                            swal('Success!', 'Customer has been updated successfully', 'success');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }else{
                            swal('Failed!', 'Something went wrong. Please Try again', 'error');
                            window.setTimeout(function(){
                                location.reload();
                            } ,3000);
                        }

                    },
                });
                e.preventDefault(); // could also use: return false;
            }else{
                swal('Error!', 'Some fields have invalid entries', 'error');
                window.setTimeout(function(){} ,3000);
            }
        });
    });
</script>

<div class="modal fade" id="view_customer">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--           modal header-->
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">View customer</h4>
            </div>
            <!--                 modal body-->
            <div class="modal-body">

                <div class="row">
                    <div class="panel panel-default mypanel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Customer Details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table table-responsive table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th>First Name</th>
                                                <td><?= $person_info->first_name; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Last Name</th>
                                                <td><?= $person_info->last_name; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Gender</th>
                                                <?php if($person_info->gender == 1):?>
                                                    <td>Male</td>
                                                <?php endif;?>
                                                <?php if($person_info->gender == 0):?>
                                                    <td>Female</td>
                                                <?php endif;?>
                                            </tr>
                                            <tr>
                                                <th>Contact Details</th>
                                                <td>
                                                    Email:<?= $person_info->email; ?><br>
                                                    Phone Number:<?= $person_info->phone_number_1?>
                                                    <?php if($person_info->phone_number_2 != NULL):?>
                                                        <?= ', '.$person_info->phone_number_2?><br>
                                                    <?php endif;?>
                                                    <?php if($person_info->phone_number_3 != NULL):?>
                                                        <?= ', '.$person_info->phone_number_3?><br>
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Company Name</th>
                                                <td><?= $person_info->company_name; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Location</th>
                                                <td><?= $person_info->address." ".$person_info->district." ".$person_info->city." ".$person_info->region; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-4">

                                    <?php if($person_info->gender == 1):?>
                                        <img src="<?= base_url() ?>assets/images/default/male.jpeg" class="img-responsive img-thumbnail" alt="">
                                    <?php endif;?>
                                    <?php if($person_info->gender == 0):?>
                                        <img src="<?= base_url() ?>assets/images/default/female.jpeg" class="img-responsive img-thumbnail" alt="">
                                    <?php endif;?>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" type="submit" id="submitButton">Save</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
