<?php
    if (isset($this->session->userdata['logged_in'])) {
        header("location:". base_url() ."auth/user_login_process");
    }
?>
<div class="row brand-contents">
    <div>
        <span><img src="assets/images/byton_logo.jpg" class="img-responsive img-thumbnail logo"></span>
    </div>
</div>

<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form action="auth/user_login_process" method="post" >
                    <h1>Please Login</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" name="username" required="" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" name="password" required="" />
                    </div>
                    <div>
                        <button type="submit" class="btn btn-default submit">Log in</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </section>
        </div>
    </div>
</div>
