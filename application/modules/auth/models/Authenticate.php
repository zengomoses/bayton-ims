<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 10/19/17
 * Time: 10:22 PM
 */

class Authenticate extends MY_Model
{
    public function login($data)
    {
        $condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . md5($data['password']). "'";
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function get_user_info($username) {

        $condition = "username =" . "'" . $username . "'";
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->join('person', 'person.id = employees.person_id');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function is_admin($username){
        $condition = "username =" . "'" . $username . "' AND " . "level =" . "'admin'";
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where($condition);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function is_member($username){
        $condition = "username =" . "'" . $username . "' AND " . "level =" . "'sales' OR ". "level ="."'chief'";
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where($condition);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function is_active($username){
        $condition = "username =" . "'" . $username . "' AND " . "status =" . "'1'";
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where($condition);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
}