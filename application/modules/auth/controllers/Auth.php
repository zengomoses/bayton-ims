
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 12:54 PM
 */

class Auth extends Auth_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation'));
        $this->load->model(array('auth/authenticate'));
    }

    public function index() {
        $data['page'] = $this->config->item('byton_ims_template_dir_public') . "login";
        $data['title'] = "LOGIN";
        $this->load->view($this->_container, $data);
    }

    public function user_login_process(){

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if($this->form_validation->run() == FALSE){
            if(isset($this->session->userdata['logged_in'])){
                redirect('admin');
            }else{
                redirect('auth');
            }
        }else{
           $data = array(
               'username' => $this->input->post('username'),
               'password' => $this->input->post('password')
           );

            $result = $this->authenticate->login($data);
            if ($result == TRUE) {

                $username = $this->input->post('username');
                $result = $this->authenticate->get_user_info($username);
                if ( $result !=false && $this->authenticate->is_admin($username) && $this->authenticate->is_active($username)) {
                    $session_data = array(
                        'name' => $result->first_name." ".$result->last_name,
                        'is_admin' => true,
                        'gender' => $result->gender,
                        'id' => $result->person_id,
                        'username' => $username,
                        'level' => $result->level
                    );
                    $this->session->set_userdata('logged_in', $session_data);
                    redirect('admin');
                }else if($result !=false && $this->authenticate->is_member($username) && $this->authenticate->is_active($username)) {
                    $session_data = array(
                        'name' => $result->first_name." ".$result->last_name,
                        'is_member' => true,
                        'gender' => $result->gender,
                        'id' => $result->person_id,
                        'username' => $username,
                        'level' => $result->level
                    );
                    $this->session->set_userdata('logged_in', $session_data);
                    redirect('pos');
                }
            }else {
                $data['page'] = $this->config->item('byton_ims_template_dir_public') . "login_fail";
                $data['title'] = "LOGIN FAILED";
                $this->load->view($this->_container, $data);
            }
        }
    }

    public function logout() {
        $sess_array = array(
            'username' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        $data = array(
            'msg' => 'You have loggedout'
        );
        redirect('auth', $data);
    }

}
