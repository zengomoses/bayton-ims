<?php  (! defined('BASEPATH')) and exit('No direct script access allowed');

class Sms extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('nexmo'));
        $this->load->model('sms_model');
        $this->load->helper(array('form', 'url'));
        $this->nexmo->set_format('json');
    }

    public function index()
    {

        $data['page'] = $this->config->item('byton_ims_template_dir_admin') . "sms/send_sms";
        $data['customers'] = $this->sms_model->get_customer_contacts();
        $data['employees'] = $this->sms_model->get_employee_contacts();
        $data['message'] = $this->sms_model->get_custom_sms();
        $data['suppliers'] = $this->sms_model->get_supplier_contacts();
        $data['title'] = "SMS";
        $this->load->view($this->_container, $data);
    }

    public function send_sms(){
        $from = 'BYTON GROUP';
        if($this->input->post('custom')){
            $to = (object)$this->input->post('custom');
            $msg = $this->input->post('custom_message');
            $message = array(
                'text' => $msg,
            );
            foreach ($to as $to){
                $response = $this->nexmo->send_message($from, $to, $message);
                if($response['messages']['0']['status'] == 0){
                    echo json_encode(array('success'=>true));

                }else{
                    echo json_encode(array('success'=>false, 'message'=>$response['messages']['0']['error-text']));
                }
            }
        }elseif($this->input->post('customer')){
            $to = (object)$this->input->post('customer');
            $msg = $this->input->post('customer_message');
            $message = array(
                'text' => $msg,
            );
            foreach ($to as $to){
                $response = $this->nexmo->send_message($from, $to, $message);
                if($response['messages']['0']['status'] == 0){
                    echo json_encode(array('success'=>true));

                }else{
                    echo json_encode(array('success'=>false, 'message'=>$response['messages']['0']['error-text']));
                }
            }
        }elseif($this->input->post('supplier')){
            $to = (object)$this->input->post('supplier');
            $msg = $this->input->post('supplier_message');
            $message = array(
                'text' => $msg,
            );
            foreach ($to as $to){
                $response = $this->nexmo->send_message($from, $to, $message);
                if($response['messages']['0']['status'] == 0){
                    echo json_encode(array('success'=>true));

                }else{
                    echo json_encode(array('success'=>false, 'message'=>$response['messages']['0']['error-text']));
                }
            }
        }elseif($this->input->post('employee')){
            $to = (object)$this->input->post('employee');
            $msg = $this->input->post('employee_message');
            $message = array(
                'text' => $msg,
            );
            foreach ($to as $to){
                $response = $this->nexmo->send_message($from, $to, $message);
                if($response['messages']['0']['status'] == 0){
                    echo json_encode(array('success'=>true));

                }else{
                    echo json_encode(array('success'=>false, 'message'=>$response['messages']['0']['error-text']));
                }
            }
        }else{
            echo json_encode(array('success'=>false));
        }
    }

    public function update_sms(){
        $message = $this->input->post('message');
        if($this->sms_model->update_custom_sms($message)){
            echo json_encode("true");
        }else{
            echo json_encode("false");
        }
    }
}
/* End of file example.php */
/* Location: ./application/controllers/example.php */