<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 12/2/17
 * Time: 8:09 AM
 */

class Sms_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_customer_contacts(){
        $this->db->select('concat_ws(" ",first_name, last_name) as customer_name, phone_number_1', FALSE);
        $this->db->from('person');
        $this->db->join('customers', 'customers.person_id = person.id');
        return $this->db->get()->result();
    }

    public function get_employee_contacts(){
        $this->db->select('concat_ws(" ",first_name, last_name) as customer_name, phone_number_1', FALSE);
        $this->db->from('person');
        $this->db->join('employees', 'employees.person_id = person.id');
        return $this->db->get()->result();
    }

    public function get_supplier_contacts(){
        $this->db->select('concat_ws(" ",first_name, last_name) as customer_name, phone_number_1', FALSE);
        $this->db->from('person');
        $this->db->join('suppliers', 'suppliers.person_id = person.id');
        return $this->db->get()->result();
    }

    public function get_custom_sms(){
        $this->db->from('messages');
        $result = $this->db->get();
        if($result->num_rows()==1)
        {
            return $result->row();

        }
    }

    public function update_custom_sms($message)
    {

        $this->db->set('message', $message);
        $this->db->where('id','1');
        $success = $this->db->update('messages');

        return $success;
    }
}