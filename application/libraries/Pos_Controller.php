<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 9/27/17
 * Time: 1:01 PM
 */

class Pos_Controller extends MY_Controller
{
    function __construct() {
        parent::__construct();

        // Set container variable
        $this->_container = $this->config->item('byton_ims_template_dir_pos') . "layout.php";
        $this->_modules = $this->config->item('modules_locations');
    }
}