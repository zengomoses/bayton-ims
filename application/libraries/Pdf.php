<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Pdf extends FPDF
{

    public function __construct()
    {
        parent::__construct();
        $pdf = new FPDF();
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true, 4);


        $CI =& get_instance();
        $CI->fpdf = $pdf;
    }
}