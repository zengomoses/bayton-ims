<?php
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 10/19/17
 * Time: 9:56 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        // Set container variable
        $this->_container = $this->config->item('byton_ims_template_dir_public') . "layout.php";
        $this->_modules = $this->config->item('modules_locations');
    }
}