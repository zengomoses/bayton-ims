<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*--------------------------------------------
 * AUTHENTICATION ROUTES
 * -------------------------------------------*/
$route['auth/logout'] = 'auth/logout';

/*--------------------------------------------
 * ADMIN ROUTES
 * -------------------------------------------*/
$route['admin/'] = 'admin';

    /*---------Sales------*/
$route['admin/sales/'] = 'sales';
$route['admin/invoices/'] = 'invoices/invoices';


    /*---------Employees---*/
$route['admin/employees/'] = 'employees';

    /*---------Invoices---*/
$route['admin/statistics/'] = 'statistics';

    /*---------Items---*/
$route['admin/items/'] = 'items';
$route['admin/items/stored_items/'] = 'items/stored_items';
$route['admin/items/inventory/'] = 'items/inventory';
$route['admin/items/categories/'] = 'items/categories';
$route['admin/items/barcode/'] = 'items/barcode';
$route['admin/orders/'] = 'orders';


 /*---------Customers---*/
$route['admin/customers/'] = 'customers';

 /*---------Suppliers---*/
$route['admin/suppliers/'] = 'suppliers';

 /*---------Reports---*/
//$route['admin/reports/'] = 'Report';

/*registration*/
$route['admin/register/'] = 'register';

/*--------------------------------------------
 * POS ROUTES
 *--------------------------------------------*/
$route['pos/'] = 'Pos';


