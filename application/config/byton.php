<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: moses
 * Date: 8/7/17
 * Time: 10:40 AM
 */

    $config['byton_ims_template_dir'] = "";
    $config['byton_ims_template_dir_public'] = $config['byton_ims_template_dir']."public/";
    $config['byton_ims_template_dir_admin'] = $config['byton_ims_template_dir']."admin/";
    $config['byton_ims_template_dir_pos'] = $config['byton_ims_template_dir']."pos/";

