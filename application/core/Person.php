<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Person extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*Determines whether the given person exists*/
    function exists($person_id)
    {
        $this->db->from('person');
        $this->db->where('person.id',$person_id);
        $query = $this->db->get();

        return ($query->num_rows()==1);
    }

    /*Gets all people*/
    function get_all()
    {
        $this->db->from('person');
        $this->db->order_by("last_name", "asc");
        return $this->db->get();
    }

    /*
    Gets information about a person as an array.
    */
    function get_info($person_id)
    {
        $query = $this->db->get_where('person', array('id' => $person_id), 1);

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //create object with empty properties.
            $fields = $this->db->list_fields('person');
            $person_obj = new stdClass;

            foreach ($fields as $field)
            {
                $person_obj->$field='';
            }

            return $person_obj;
        }
    }
    //Gets total of rows of all people in the database
    public function get_total_rows()
    {
        $this->db->from('person');
        $this->db->where('deleted', 0);

        return $this->db->count_all_results();
    }

    /*
    Get person with specific ids
    */
    function get_multiple_info($person_ids)
    {
        $this->db->from('person');
        $this->db->where_in('id',$person_ids);
        $this->db->order_by("last_name", "asc");
        return $this->db->get();
    }

    /*
    Inserts or updates a person
    */
    function save(&$person_data,$person_id=false)
    {
        if (!$person_id or !$this->exists($person_id))
        {
            if ($this->db->insert('person',$person_data))
            {
                $person_data['person_id']=$this->db->insert_id();
                return true;
            }

            return false;
        }

        $this->db->where('id', $person_id);
        return $this->db->update('person',$person_data);
    }

    /*
    Deletes one Person
    */
    function delete($person_id)
    {
        return $this->db->delete('person', array('id' => $person_id));
    }

    /*
    Deletes a list of person
    */
    function delete_list($person_ids)
    {
        $this->db->where_in('id',$person_ids);
        return $this->db->delete('person');
    }
}

